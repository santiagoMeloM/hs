-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-06-17 04:21:39.916

-- tables
-- Table: folder
CREATE TABLE folder (
    id int NOT NULL AUTO_INCREMENT,
    CONSTRAINT folder_pk PRIMARY KEY (id)
);

-- Table: image
CREATE TABLE image (
    id int NOT NULL AUTO_INCREMENT,
    content longblob NOT NULL,
    created date NOT NULL,
    CONSTRAINT image_pk PRIMARY KEY (id)
);

-- Table: image_folder
CREATE TABLE image_folder (
    id int NOT NULL AUTO_INCREMENT,
    folder_id int NOT NULL,
    image_id int NOT NULL,
    CONSTRAINT image_folder_pk PRIMARY KEY (id)
);

-- Table: product_folder
CREATE TABLE product_folder (
    id int NOT NULL AUTO_INCREMENT,
    product_id int NOT NULL,
    folder_id int NOT NULL,
    CONSTRAINT product_folder_pk PRIMARY KEY (id)
);

-- Table: video
CREATE TABLE video (
    id int NOT NULL AUTO_INCREMENT,
    content longblob NOT NULL,
    created date NOT NULL,
    CONSTRAINT video_pk PRIMARY KEY (id)
);

-- Table: video_folder
CREATE TABLE video_folder (
    id int NOT NULL AUTO_INCREMENT,
    video_id int NOT NULL,
    folder_id int NOT NULL,
    CONSTRAINT video_folder_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: folder_image_folder (table: image_folder)
ALTER TABLE image_folder ADD CONSTRAINT folder_image_folder FOREIGN KEY folder_image_folder (folder_id)
    REFERENCES folder (id);

-- Reference: folder_image_image (table: image_folder)
ALTER TABLE image_folder ADD CONSTRAINT folder_image_image FOREIGN KEY folder_image_image (image_id)
    REFERENCES image (id);

-- Reference: product_folder_folder (table: product_folder)
ALTER TABLE product_folder ADD CONSTRAINT product_folder_folder FOREIGN KEY product_folder_folder (folder_id)
    REFERENCES folder (id);

-- Reference: video_folder_folder (table: video_folder)
ALTER TABLE video_folder ADD CONSTRAINT video_folder_folder FOREIGN KEY video_folder_folder (folder_id)
    REFERENCES folder (id);

-- Reference: video_folder_video (table: video_folder)
ALTER TABLE video_folder ADD CONSTRAINT video_folder_video FOREIGN KEY video_folder_video (video_id)
    REFERENCES video (id);

-- End of file.

