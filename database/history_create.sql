-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-06-17 04:22:22.923

-- tables
-- Table: delivered
CREATE TABLE delivered (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    product_id int NOT NULL,
    created date NOT NULL,
    CONSTRAINT delivered_pk PRIMARY KEY (id)
);

-- Table: like_product
CREATE TABLE like_product (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    product_id int NOT NULL,
    created date NOT NULL,
    CONSTRAINT like_product_pk PRIMARY KEY (id)
);

-- End of file.

