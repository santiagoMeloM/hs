
DELIMITER $$
DROP PROCEDURE IF EXISTS addImage;
CREATE PROCEDURE addImage(IN cntnt longblob)
BEGIN
    INSERT INTO image (content, created) VALUES (cntnt, NOW());
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addVideo;
CREATE PROCEDURE addVideo(IN cntnt longblob)
BEGIN
    INSERT INTO video (content, created) VALUES (cntnt, NOW());
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addFolder;
CREATE PROCEDURE addFolder(IN pid int)
BEGIN
    INSERT INTO folder (product_id) VALUES (pid);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS assignImage;
CREATE PROCEDURE assignImage(IN iid int, IN fid int)
BEGIN
    INSERT INTO image_folder (image_id, folder_id) VALUES (iid, fid);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS assignVideo;
CREATE PROCEDURE assignVideo(IN vid int, IN fid int)
BEGIN
    INSERT INTO video_folder (video_id, folder_id) VALUES (vid, fid);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS getProductFolder;
CREATE PROCEDURE getProductFolder(IN pid int)
BEGIN
    DECLARE fid int;
    SELECT id INTO fid FROM folder WHERE product_id=pid;
    SELECT i.*, v.* FROM image_folder fi INNER JOIN video_folder fv 
        ON (fi.)
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateImage;
CREATE PROCEDURE updateImage(IN iid int, IN cntnt longblob, IN crtd date)
BEGIN
    UPDATE image SET content=cntnt, created=crtd WHERE id=iid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateVideo;
CREATE PROCEDURE updateVideo(IN vid int, IN cntnt longblob, IN crtd date)
BEGIN
    UPDATE video SET content=cntnt, created=crtd WHERE id=vid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteImage;
CREATE PROCEDURE deleteImage(IN iid int)
BEGIN
    DELETE FROM image WHERE id=iid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteVideo;
CREATE PROCEDURE deleteVideo(IN vid int)
BEGIN
    DELETE FROM video WHERE id=vid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteFolder;
CREATE PROCEDURE deleteFolder(IN fid int)
BEGIN
    DELETE FROM folder WHERE id=fid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS unassignImage;
CREATE PROCEDURE unassignImage(IN ifid int)
BEGIN
    DELETE FROM image_folder WHERE id=ifid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS unassignVideo;
CREATE PROCEDURE unassignVideo(IN vfid int)
BEGIN
    DELETE FROM video_folder WHERE id=vfid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;