
ADD = {
    "accepted": {
        "body": {
            "name": "product_0",
            "description": "0",
            "price": 0
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "name": "product_0",
            "description": "0",
            "price": "a"
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

ADD_DISCOUNT = {
    "accepted": {
        "body": {
            "product_id": 1,
            "state": 1,
            "deadline": "2020-12-12",
            "porcentage": 0
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "product_id": 1,
            "state": 1,
            "deadline": "2020-15-12",
            "porcentage": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET = {
    "accepted": {
        "body": {
            "product_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "product_id": "a"
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_PRODUCT_SIZE = {
    "accepted": {
        "response": {
            "response": 200, 
            "content": 1
        }
    }
}

GET_DISCOUNT = {
    "accepted": {
        "body": {
            "discount_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "discount_id": "a"
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_DISCOUNT_SIZE = {
    "accepted": {
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_DISCOUNT_RANGE = {
    "accepted": {
        "params": {
            "start": 0,
            "qnt": 10
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "params": {
            "start": 0,
            "qnt": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_PRODUCT_DISCOUNT = {
    "accepted": {
        "body": {
            "product_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "product_id": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_PRODUCT_ARRAY_DISCOUNT = {
    "accepted": {
        "body": {
            "products": [1,2]
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "products": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_PRODUCT_ARRAY = {
    "accepted": {
        "body": {
            "products": [1,2]
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "products": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_RANGE = {
    "accepted": {
        "params": {
            "start": 0,
            "qnt": 10
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "params": {
            "start": 0,
            "qnt": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_PRODUCT_ALL = {
    "accepted": {
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_DATE_RANGE = {
    "accepted": {
        "body": {
            "start": 0,
            "qnt": 10,
            "date": "2020-12-12"
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "start": 0,
            "qnt": 0,
            "date": ""
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_DISCOUNT_RANGE = {
    "accepted": {
        "params": {
            "start": 0,
            "qnt": 10
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "params": {
            "start": 0,
            "qnt": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

GET_NAME_RANGE = {
    "accepted": {
        "params": {
            "start": 0,
            "qnt": 10,
            "name": "p"
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "params": {
            "start": 0,
            "qnt": 0,
            "name": "p"
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

UPDATE_STATE = {
    "accepted": {
        "body": {
            "product_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "product_id": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

UPDATE_DISCOUNT_STATE = {
    "accepted": {
        "body": {
            "discount_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "discount_id": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

UPDATE = {
    "accepted": {
        "body": {
            "id": 1,
            "name": "product",
            "description": "first product",
            "price": 10,
            "created": "2020-12-12"
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "id": 1,
            "name": "product",
            "description": "first product",
            "price": 10,
            "created": "2020-12-12"
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

DELETE = {
    "accepted": {
        "body": {
            "product_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "product_id": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}

DELETE_DISCOUNT = {
    "accepted": {
        "body": {
            "discount_id": 1
        },
        "response": {
            "response": 200, 
            "content": 1
        }
    }, 
    "rejected": {
        "body": {
            "discount_id": 0
        },
        "response": {
            "response": 400, 
            "content": {}
        }
    }
}