
import requests
import json
import os
import constant.product as PRODUCT
import datetime

headers = {
    "Sauthentication": os.environ.get("Sauthentication"),
    "Authentication": os.environ.get("Authentication")
}

def add(client, name, description, price, **kwargs):
    return client.post("/product/add", json=dict(
        name=name,
        description=description,
        price=price
    ), headers=headers)

def test_add(app, client):
    accepted = add(client, **PRODUCT.ADD["accepted"]["body"])
    rejected = add(client, **PRODUCT.ADD["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.ADD["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.ADD["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def addDiscount(client, product_id, state, deadline, porcentage, **kwargs):
    return client.post("/product/add/discount", json=dict(
        product_id=product_id,
        state=state,
        deadline=deadline,
        porcentage=porcentage
    ), headers=headers)

def test_addDiscount(app, client):
    accepted = addDiscount(client, **PRODUCT.ADD_DISCOUNT["accepted"]["body"])
    rejected = addDiscount(client, **PRODUCT.ADD_DISCOUNT["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.ADD_DISCOUNT["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.ADD_DISCOUNT["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def get(client, product_id, **kwargs):
    return client.post("/product/get", json=dict(
        product_id=product_id
    ), headers=headers)

def test_get(app, client):
    accepted = get(client, **PRODUCT.GET["accepted"]["body"])
    rejected = get(client, **PRODUCT.GET["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.GET["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getProductSize(client):
    return client.get("/product/get/size", headers=headers)

def test_getProductSize(app, client):
    accepted = getProductSize(client)
    assert accepted.status_code == 200
    assert PRODUCT.GET_PRODUCT_SIZE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]

def getDiscount(client, discount_id, **kwargs):
    return client.post("/product/get/discount", json=dict(
        discount_id=discount_id
    ), headers=headers)

def test_getDiscount(app, client):
    accepted = getDiscount(client, **PRODUCT.GET_DISCOUNT["accepted"]["body"])
    rejected = getDiscount(client, **PRODUCT.GET_DISCOUNT["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_DISCOUNT["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_DISCOUNT["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getDiscountSize(client):
    return client.get("/product/discount/get/size", headers=headers)

def test_getDiscountSize(app, client):
    accepted = getDiscountSize(client)
    assert accepted.status_code == 200
    assert PRODUCT.GET_DISCOUNT_SIZE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]

def getProductDiscountRange(client, start, qnt, **kwargs):
    return client.get("/product/get/discount/{}/{}".format(start, qnt), headers=headers)

def test_getProductDiscountRange(app, client):
    accepted = getProductDiscountRange(client, **PRODUCT.GET_DISCOUNT_RANGE["accepted"]["params"])
    rejected = getProductDiscountRange(client, **PRODUCT.GET_DISCOUNT_RANGE["rejected"]["params"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_DISCOUNT_RANGE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_DISCOUNT_RANGE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getProductDiscount(client, product_id, **kwargs):
    return client.post("/product/get/discount/product", json=dict(
        product_id=product_id
    ), headers=headers)

def test_getProductDiscount(app, client):
    accepted = getProductDiscount(client, **PRODUCT.GET_PRODUCT_DISCOUNT["accepted"]["body"])
    rejected = getProductDiscount(client, **PRODUCT.GET_PRODUCT_DISCOUNT["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_PRODUCT_DISCOUNT["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_PRODUCT_DISCOUNT["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getProductArrayDiscount(client, products, **kwargs):
    return client.post("/product/get/discount/product/many", json=dict(
        products=products
    ), headers=headers)

def test_getProductArrayDiscount(app, client):
    accepted = getProductArrayDiscount(client, **PRODUCT.GET_PRODUCT_ARRAY_DISCOUNT["accepted"]["body"])
    rejected = getProductArrayDiscount(client, **PRODUCT.GET_PRODUCT_ARRAY_DISCOUNT["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_PRODUCT_ARRAY_DISCOUNT["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_PRODUCT_ARRAY_DISCOUNT["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getProductArray(client, products, **kwargs):
    return client.post("/product/get/product/many", json=dict(
        products=products
    ), headers=headers)

def test_getProductArray(app, client):
    accepted = getProductArray(client, **PRODUCT.GET_PRODUCT_ARRAY["accepted"]["body"])
    rejected = getProductArray(client, **PRODUCT.GET_PRODUCT_ARRAY["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_PRODUCT_ARRAY["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_PRODUCT_ARRAY["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getRange(client, start, qnt, **kwargs):
    return client.get("/product/get/{}/{}".format(start, qnt), headers=headers)

def test_getRange(app, client):
    accepted = getRange(client, **PRODUCT.GET_RANGE["accepted"]["params"])
    rejected = getRange(client, **PRODUCT.GET_RANGE["rejected"]["params"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_RANGE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_RANGE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getProductAll(client):
    return client.get("/product/get/all", headers=headers)

def test_getProductAll(app, client):
    accepted = getProductAll(client)
    rejected = getProductAll(client)
    assert accepted.status_code == 200
    assert PRODUCT.GET_PRODUCT_ALL["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    #assert PRODUCT.GET_PRODUCT_ALL["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getDateRange(client, start, qnt, **kwargs):
    return client.post("/product/get/date/{}/{}".format(start, qnt), json=dict(
        date=str(datetime.datetime.today().date())
    ), headers=headers)

def test_getDateRange(app, client):
    accepted = getDateRange(client, **PRODUCT.GET_DATE_RANGE["accepted"]["body"])
    rejected = getDateRange(client, **PRODUCT.GET_DATE_RANGE["rejected"]["body"])
    print(json.loads(accepted.get_data(as_text=True)))
    assert accepted.status_code == 200
    assert PRODUCT.GET_DATE_RANGE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_DATE_RANGE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getDiscountRange(client, start, qnt, **kwargs):
    return client.get("/product/get/discount/combined/{}/{}".format(start, qnt), headers=headers)

def test_getDiscountRange(app, client):
    accepted = getDiscountRange(client, **PRODUCT.GET_DISCOUNT_RANGE["accepted"]["params"])
    rejected = getDiscountRange(client, **PRODUCT.GET_DISCOUNT_RANGE["rejected"]["params"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_DISCOUNT_RANGE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_DISCOUNT_RANGE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def getNameRange(client, name, start, qnt, **kwargs):
    return client.get("/product/get/name/{}/{}".format(start, qnt), query_string={'name': name}, headers=headers)

def test_getNameRange(app, client):
    accepted = getNameRange(client, **PRODUCT.GET_NAME_RANGE["accepted"]["params"])
    rejected = getNameRange(client, **PRODUCT.GET_NAME_RANGE["rejected"]["params"])
    assert accepted.status_code == 200
    assert PRODUCT.GET_NAME_RANGE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.GET_NAME_RANGE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def updateState(client, product_id, **kwargs):
    return client.put("/product/update/state", json=dict(
        product_id=product_id
    ), headers=headers)

def test_updateState(app, client):
    accepted = updateState(client, **PRODUCT.UPDATE_STATE["accepted"]["body"])
    rejected = updateState(client, **PRODUCT.UPDATE_STATE["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.UPDATE_STATE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.UPDATE_STATE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def updateDiscountState(client, discount_id, **kwargs):
    return client.put("/product/update/state/discount", json=dict(
        discount_id=discount_id
    ), headers=headers)

def test_updateDiscountState(app, client):
    accepted = updateDiscountState(client, **PRODUCT.UPDATE_DISCOUNT_STATE["accepted"]["body"])
    rejected = updateDiscountState(client, **PRODUCT.UPDATE_DISCOUNT_STATE["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.UPDATE_DISCOUNT_STATE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.UPDATE_DISCOUNT_STATE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def update(client, id, name, description, price, created, **kwargs):
    return client.put("/product/update", json=dict(
        id=id, 
        name=name, 
        description=description, 
        price=price, 
        created=created
    ), headers=headers)

def test_update(app, client):
    accepted = update(client, **PRODUCT.UPDATE["accepted"]["body"])
    rejected = update(client, **PRODUCT.UPDATE["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.UPDATE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.UPDATE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def deleteDiscount(client, discount_id, **kwargs):
    return client.delete("/product/delete/discount", json=dict(
        discount_id=discount_id
    ), headers=headers)

def test_deleteDiscount(app, client):
    accepted = deleteDiscount(client, **PRODUCT.DELETE_DISCOUNT["accepted"]["body"])
    rejected = deleteDiscount(client, **PRODUCT.DELETE_DISCOUNT["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.DELETE_DISCOUNT["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.DELETE_DISCOUNT["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]

def delete(client, product_id, **kwargs):
    return client.delete("/product/delete", json=dict(
        product_id=product_id
    ), headers=headers)

def test_delete(app, client):
    accepted = delete(client, **PRODUCT.DELETE["accepted"]["body"])
    rejected = delete(client, **PRODUCT.DELETE["rejected"]["body"])
    assert accepted.status_code == 200
    assert PRODUCT.DELETE["accepted"]["response"]["response"] == json.loads(accepted.get_data(as_text=True))["response"]
    assert PRODUCT.DELETE["rejected"]["response"]["response"] == json.loads(rejected.get_data(as_text=True))["response"]
