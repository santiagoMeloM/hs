
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields
import constant.spec as SPEC
from flask import jsonify

spec = APISpec(
    title=SPEC.API_NAME,
    version=SPEC.VERSION_API_SPEC,
    openapi_version=SPEC.OPENAPI_VERSION,
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)

class IntegerSchema(Schema):
    number = fields.Int(description="An integer", required=True)

class FloatSchema(Schema):
    number = fields.Float(description="A float", required=True)

class StringSchema(Schema):
    string = fields.String(description="A string.", required=True)

class ObjectSchema(Schema):
    object = fields.Dict(description="An object.", required=True)

class BooleanSchema(Schema):
    object = fields.Boolean(description="A boolean.", required=True)


spec.components.schema("Int", schema=IntegerSchema)
spec.components.schema("Float", schema=FloatSchema)
spec.components.schema("String", schema=StringSchema)
spec.components.schema("Object", schema=ObjectSchema)
spec.components.schema("Boolean", schema=BooleanSchema)

def documentation(app):

    with app.test_request_context():
        for fn_name in app.view_functions:
            if fn_name == 'static':
                continue
            view_fn = app.view_functions[fn_name]
            spec.path(view=view_fn)

    @app.route("/documentation.json")
    def swagger():
        return jsonify(spec.to_dict())