
import jwt
import random

KEY_SIZE = 50
DB_PASSWORD = ''.join([chr(random.choice([random.randint(48, 58), random.randint(65, 91), random.randint(97, 123)])) for _ in range(KEY_SIZE)])
JWT_KEY = ''.join([chr(random.choice([random.randint(48, 58), random.randint(65, 91), random.randint(97, 123)])) for _ in range(KEY_SIZE)])
JWT_ALGORITHM = "HS256"
JWT_TOKEN = jwt.encode({"roles": ["admin", "user", "system"]}, JWT_KEY, algorithm=JWT_ALGORITHM).decode("UTF-8")

environment = {
    "IP":"0.0.0.0",
    "PORT":"4200",
    "DOMAIN":"product.highsclothing.shop", #Correct this to be a parameter
    "EMAIL":"santiagosocialb@gmail.com", #Correct this to be a parameter
    "DS_PROTOCOL":"http://",
    "DS_HOSTDISCOVER":"http://0.0.0.0:8000/eureka/apps",
    "DS_PORTDISCOVER":"8000",
    "DS_SERVICENAME":"PRODUCT_SERVICE",
    "DS_POSTINSTANCE":"4200",
    "DS_HEARTBEATDISCOVER":"30",
    "MYSQL_ROOT_PASSWORD": DB_PASSWORD,
    "MYSQL_DATABASE":"Product",
    "MYSQL_USER":"santiago",
    "MYSQL_PASSWORD": DB_PASSWORD,
    "DB_HOST":"product_database",
    "DB_PORT":"3306",
    "DB_USER":"santiago",
    "DB_PASSWORD":DB_PASSWORD,
    "DB_NAME":"Product",
    "JWT_KEY":JWT_KEY,
    "JWT_ALGORITHM":JWT_ALGORITHM,
    "Sauthentication":JWT_TOKEN,
    "Authentication":JWT_TOKEN
}

file = open("env.env", "w")
file.write('\n'.join(["{}={}".format(key, environment[key]) for key in environment]))
file.close()