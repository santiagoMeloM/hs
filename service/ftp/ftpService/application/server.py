
import os
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

env = os.environ

FTP_HOST = env['HOST']
FTP_PORT = env['PORT']
FTP_USER = env['USER']
FTP_PASSWORD = env['PASSWORD']
FTP_FOREIGN_IP = env['FTP_FOREIGN_IP']
FTP_DIRECTORY = "/usr/src/app/application/files"


def main():
    authorizer = DummyAuthorizer()
    authorizer.add_user(FTP_USER, FTP_PASSWORD, FTP_DIRECTORY, perm='elradfmw')

    handler = FTPHandler
    handler.authorizer = authorizer
    handler.banner = "Hellom to HS ftp server."
    handler.permit_foreign_addresses = True

    # Optionally specify range of ports to use for passive connections.
    #handler.passive_ports = range(60000, 65535)

    server = FTPServer((FTP_HOST, FTP_PORT), handler)

    server.max_cons = 256
    server.max_cons_per_ip = 5

    server.serve_forever()


if __name__ == '__main__':
    main()
