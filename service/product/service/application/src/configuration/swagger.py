
from flask_swagger_ui import get_swaggerui_blueprint
import constant.swagger as SWAGGER
import constant.spec as SPEC

blueprint = get_swaggerui_blueprint(
    SWAGGER.URL,
    SWAGGER.API,
    config= {
        "app_name": SPEC.API_NAME
    }
)