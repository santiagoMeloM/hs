
import os

env = os.environ

def establishEnvironment():
    env["IP"]="0.0.0.0"
    env["PORT"]="4200"
    env["DS_PROTOCOL"]="http://"
    env["DS_HOSTDISCOVER"]="http://0.0.0.0:8000/eureka/apps"
    env["DS_PORTDISCOVER"]="8000"
    env["DS_SERVICENAME"]="PRODUCT_SERVICE"
    env["DS_POSTINSTANCE"]="4200"
    env["DS_HEARTBEATDISCOVER"]="30"
    env["DB_HOST"]="0.0.0.0"
    env["DB_PORT"]="4220"
    env["DB_USER"]="santiago"
    env["DB_PASSWORD"]="123"
    env["DB_NAME"]="Product"
    env["JWT_KEY"]="123"
    env["JWT_ALGORITHM"]="HS256"