
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.product as SP

alias = "product"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
@admin
def add():
    """
    ---
    post:
        description: Add product
        parameters:
            - in: body
              name: name
              schema: StringSchema
            - in: body
              name: description
              schema: StringSchema
            - in: body
              name: price
              schema: FloatSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/add/discount".format(alias), methods=["POST"])
@system
@admin
def addDiscount():
    """
    ---
    post:
        description: Add discount
        parameters:
            - in: body
              name: product_id
              schema: IntegerSchema
            - in: body
              name: state
              schema: BooleanSchema
            - in: body
              name: deadline
              schema: StringSchema
            - in: body
              name: porcentage
              schema: FloatSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.addDiscount()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    """
    ---
    post:
        description: Get product by id
        parameters:
            - in: body
              name: product_id
              schema: IntegerSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/size".format(alias), methods=["GET"])
@system
def getProductSize():
    """
    ---
    get:
        description: Get size of products inventory
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductSize()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/discount".format(alias), methods=["POST"])
@system
def getDiscount():
    """
    ---
    post:
        description: Get discount by id
        parameters:
            - in: body
              name: discount_id
              schema: IntegerSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getDiscount()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/discount/get/size".format(alias), methods=["GET"])
@system
def getDiscountSize():
    """
    ---
    get:
        description: Get size of discount inventory
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getDiscountSize()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/discount/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getProductDiscountRange(start, qnt):
    """
    ---
    get:
        description: Get products with active discount in range
        parameters:
            - in: path
              name: start
              description: Starting id
            - in: path
              name: qnt
              description: Quantity of discounts tyo retrieve
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductDiscountRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/discount/product".format(alias), methods=["POST"])
@system
def getProductDiscount():
    """
    ---
    post:
        description: Get discounts by product
        parameters:
            - in: body
              name: product_id
              description: Product id to which discounts has been added to
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductDiscount()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/discount/product/many".format(alias), methods=["POST"])
@system
def getProductArrayDiscount():
    """
    ---
    post:
        description: Get discounts from many products with their id
        parameters:
            - in: body
              name: products
              description: Array of ids of products
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductArrayDiscount()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/product/many".format(alias), methods=["POST"])
@system
def getProductArray():
    """
    ---
    post:
        description: Get products with their id
        parameters:
            - in: body
              name: products
              description: Array of ids of products
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductArray()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)


@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    """
    ---
    get:
        description: Get products in range
        parameters:
            - in: path
              name: start
              description: Starting id
            - in: path
              name: qnt
              description: Quantity of products to obtain
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/all".format(alias), methods=["GET"])
@system
def getProductAll():
    """
    ---
    get:
        description: Get all products
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getProductAll()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/date/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getDateRange(start, qnt):
    """
    ---
    post:
        description: Get product in range created on or before date
        parameters:
            - in: path
              name: start
              description: Starting id
            - in: path
              name: qnt
              description: Quantity of products to obtain
            - in: body
              name: date
              schema: StringSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getDateRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/discount/combined/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getDiscountRange(start, qnt):
    """
    ---
    get:
        description: Get discounts in range with its product information
        parameters:
            - in: path
              name: start
              description: Starting id
            - in: path
              name: qnt
              description: Quantity of products to obtain
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getDiscountRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/name/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getNameRange(start, qnt):
    """
    ---
    get:
        description: Get products with name like :name in range
        parameters:
            - in: args
              name: name
              description: Name to look for on products
            - in: path
              name: start
              description: Starting id
            - in: path
              name: qnt
              description: Quantity of products to obtain
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    data = SP.getNameRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update/state".format(alias), methods=["PUT"])
@system
@admin
def updateState():
    """
    ---
    put:
        description: Update state of product
        parameters:
            - in: body
              name: product_id
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.updateState()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/update/state/discount".format(alias), methods=["PUT"])
@system
@admin
def updateDiscountState():
    """
    ---
    put:
        description: Update state of discount
        parameters:
            - in: body
              name: discount_id
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.updateDiscountState()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    """
    ---
    put:
        description: Update product
        parameters:
            - in: body
              name: id
              schema: IntegerSchema
            - in: body
              name: name
              schema: StringSchema
            - in: body
              name: description
              schema: StringSchema
            - in: body
              name: price
              schema: FloatSchema
            - in: body
              name: created
              schema: StringSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    """
    ---
    delete:
        description: Delete product
        parameters:
            - in: body
              name: product_id
              schema: IntegerSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete/discount".format(alias), methods=["DELETE"])
@system
@admin
def deleteDiscount():
    """
    ---
    delete:
        description: Delete discount
        parameters:
            - in: body
              name: discount_id
              schema: IntegerSchema
        tags:
            - product
    """
    response = {"response": 400, "content": {}}
    result = SP.deleteDiscount()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

