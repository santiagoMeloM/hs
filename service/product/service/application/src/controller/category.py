
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.category as SC

alias = "category"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
@admin
def add():
    """
    ---
    post:
        description: Add category
        parameters:
            - in: body
              name: name
              schema: StringSchema
            - in: body
              name: description
              schema: StringSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    result = SC.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/add/product".format(alias), methods=["POST"])
@system
@admin
def addProductCategory():
    """
    ---
    post:
        description: Assign product to category
        parameters:
            - in: body
              name: product_id
              schema: IntegerSchema
            - in: body
              name: category_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    result = SC.addProductCategory()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    """
    ---
    post:
        description: Get category by id
        parameters:
            - in: body
              name: category_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    """
    ---
    get:
        description: Get categories by range
        parameters:
            - in: path
              name: start
              description: Starting by id
            - in: path
              name: qnt
              description: Quantity of categories
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/name/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getNameRange(start, qnt):
    """
    ---
    get:
        description: Get categories by name like
        parameters:
            - in: path
              name: start
              description: Starting by id
            - in: path
              name: qnt
              description: Quantity of categories
            - in: args
              name: name
              description: Name to look for
              schema: StringSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.getNameRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/product/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getProductRange(start, qnt):
    """
    ---
    post:
        description: Get products related to categories in range
        parameters:
            - in: path
              name: start
              description: Starting by id
            - in: path
              name: qnt
              description: Quantity of products
            - in: body
              name: category_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.getProductRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/size".format(alias), methods=["GET"])
@system
def getCategorySize():
    """
    ---
    get:
        description: Get size of categories inventory
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.getCategorySize()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/product/category/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
@admin
def getCategoryByProductRange(start, qnt):
    """
    ---
    post:
        description: Get categories related to product in range
        parameters:
            - in: path
              name: start
              description: Starting by id
            - in: path
              name: qnt
              description: Quantity of categories
            - in: body
              name: product_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    data = SC.getCategoryByProductRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    """
    ---
    put:
        description: Update category
        parameters:
            - in: body
              name: id
              schema: IntegerSchema
            - in: body
              name: name
              schema: StringSchema
            - in: body
              name: description
              schema: StringSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    result = SC.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    """
    ---
    delete:
        description: Delete category
        parameters:
            - in: body
              name: category_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    result = SC.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete/product".format(alias), methods=["DELETE"])
@system
@admin
def deleteProductCategory():
    """
    ---
    delete:
        description: Delete category
        parameters:
            - in: body
              name: id
              schema: IntegerSchema
            - in: body
              name: product_id
              schema: IntegerSchema
            - in: body
              name: category_id
              schema: IntegerSchema
        tags:
            - category
    """
    response = {"response": 400, "content": {}}
    result = SC.deleteProductCategory()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)


