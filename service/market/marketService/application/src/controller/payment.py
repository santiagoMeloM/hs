
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.payment as SP

alias = "payment"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
def add():
    response = {"response": 400, "content": {}}
    result = SP.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    response = {"response": 400, "content": {}}
    data = SP.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/user/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getUserPayment(start, qnt):
    response = {"response": 400, "content": {}}
    data = SP.getUserPayment(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/order/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getOrderPayment(start, qnt):
    response = {"response": 400, "content": {}}
    data = SP.getOrderPayment(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/date/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getDateOrder(start, qnt):
    response = {"response": 400, "content": {}}
    data = SP.getDateOrder(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SP.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    response = {"response": 400, "content": {}}
    result = SP.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    response = {"response": 400, "content": {}}
    result = SP.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)


