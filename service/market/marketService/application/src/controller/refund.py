
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.refund as SR

alias = "refund"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
@admin
def add():
    response = {"response": 400, "content": {}}
    result = SR.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    response = {"response": 400, "content": {}}
    data = SR.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/payment/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getPaymentRefund(start, qnt):
    response = {"response": 400, "content": {}}
    data = SR.getPaymentRefund(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/date/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getDateOrder(start, qnt):
    response = {"response": 400, "content": {}}
    data = SR.getDateOrder(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SR.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    response = {"response": 400, "content": {}}
    result = SR.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    response = {"response": 400, "content": {}}
    result = SR.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)


