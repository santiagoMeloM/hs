
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.order as SO

alias = "order"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
def add():
    response = {"response": 400, "content": {}}
    result = SO.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    response = {"response": 400, "content": {}}
    data = SO.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/user/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getUserOrder(start, qnt):
    response = {"response": 400, "content": {}}
    data = SO.getUserOrder(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/product/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getProductOrder(start, qnt):
    response = {"response": 400, "content": {}}
    data = SO.getProductOrder(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/date/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getDateOrder(start, qnt):
    response = {"response": 400, "content": {}}
    data = SO.getDateOrder(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SO.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    response = {"response": 400, "content": {}}
    result = SO.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    response = {"response": 400, "content": {}}
    result = SO.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)


