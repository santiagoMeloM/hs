
from configuration.handler import app
import controller.order as CO
import controller.payment as CP
import controller.refund as CR

app.register_blueprint(CO.controller)
app.register_blueprint(CP.controller)
app.register_blueprint(CR.controller)