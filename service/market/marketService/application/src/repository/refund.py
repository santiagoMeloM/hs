
from configuration.app import db

def add(paymentId, state):
    result = False
    try:
        tmp = db.procedure("addRefund", [paymentId, state])
        result = tmp[0]["rows"]
    except: pass
    return result


def get(refundId):
    qry = "SELECT * FROM refund WHERE id='{}'".format(refundId)
    result = db.fetchOne(qry)
    return result

def getPaymentRefund(paymentId, start, qnt):
    qry = "SELECT * FROM refund WHERE payment_id='{}' LIMIT {}, {}".format(paymentId, start, qnt)
    result = db.fetchAll(qry)
    return result

def getDateOrder(date, start, qnt):
    qry = "SELECT * FROM refund WHERE created='{}' LIMIT {}, {}".format(date, start, qnt)
    result = db.fetchAll(qry)
    return result

def getRange(start, qnt):
    qry = "SELECT * FROM refund LIMIT {},{}".format(start, qnt)
    result = db.fetchAll(qry)
    return result


def update(refund):
    result = False
    try:
        tmp = db.procedure("updateRefund", refund.toArray())
        result = tmp[0]["rows"] > 0
    except: pass
    return result

def delete(refundId):
    result = False
    try:
        tmp = db.procedure("deleteRefund", [refundId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result