
from configuration.app import db

def add(userId, productId, state, address, charact, price):
    result = False
    try:
        tmp = db.procedure("addOrder", [userId, productId, state, address, charact, price])
        result = tmp[0]["rows"]
    except: pass
    return result

def get(orderId):
    qry = "SELECT * FROM `order` WHERE id='{}'".format(orderId)
    result = db.fetchOne(qry)
    return result

def getUserOrder(userId, start, qnt):
    qry = "SELECT * FROM `order` WHERE user_id='{}' LIMIT {}, {}".format(userId, start, qnt)
    result = db.fetchAll(qry)
    return result

def getProductOrder(productId, start, qnt):
    qry = "SELECT * FROM `order` WHERE product_id='{}' LIMIT {}, {}".format(productId, start, qnt)
    result = db.fetchAll(qry)
    return result

def getDateOrder(date, start, qnt):
    qry = "SELECT * FROM `order` WHERE created='{}' LIMIT {}, {}".format(date, start, qnt)
    result = db.fetchAll(qry)
    return result

def getRange(start, qnt):
    qry = "SELECT * FROM `order` LIMIT {},{}".format(start, qnt)
    result = db.fetchAll(qry)
    return result

def update(order):
    result = False
    try:
        tmp = db.procedure("updateOrder", order.toArray())
        result = tmp[0]["rows"] > 0
    except: pass
    return result

def delete(orderId):
    result = False
    try:
        tmp = db.procedure("deleteOrder", [orderId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result