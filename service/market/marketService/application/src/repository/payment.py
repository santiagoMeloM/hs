
from configuration.app import db

def add(userId, orderId, amount):
    result = False
    try:
        tmp = db.procedure("addPayment", [userId, orderId, amount])
        result = tmp[0]["rows"]
    except:
        print(122)
    return result

def get(paymentId):
    qry = "SELECT * FROM payment WHERE id='{}'".format(paymentId)
    result = db.fetchOne(qry)
    return result

def getOrderPayment(orderId, start, qnt):
    qry = "SELECT * FROM payment WHERE order_id='{}' LIMIT {}, {}".format(orderId, start, qnt)
    result = db.fetchAll(qry)
    return result

def getUserPayment(userId, start, qnt):
    qry = "SELECT * FROM payment WHERE user_id='{}' LIMIT {}, {}".format(userId, start, qnt)
    result = db.fetchAll(qry)
    return result

def getDateOrder(date, start, qnt):
    qry = "SELECT * FROM payment WHERE created='{}' LIMIT {}, {}".format(date, start, qnt)
    result = db.fetchAll(qry)
    return result

def getRange(start, qnt):
    qry = "SELECT * FROM payment LIMIT {},{}".format(start, qnt)
    result = db.fetchAll(qry)
    return result

def update(payment):
    result = False
    try:
        tmp = db.procedure("updatePayment", payment.toArray())
        result = tmp[0]["rows"] > 0
    except: pass
    return result

def delete(paymentId):
    result = False
    try:
        tmp = db.procedure("deletePayment", [paymentId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result