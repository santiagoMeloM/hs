
import os

env = os.environ

## Debug values
# env['DB_HOST'] = "localhost"
# env['DB_PORT'] = "4420"
# env['DB_USER'] = "santiago"
# env['DB_PASSWORD'] = "123"
# env['DB_NAME'] = "Market"

HOST = env.get("DB_HOST")
PORT = int(env.get("DB_PORT"))
USER = env.get("DB_USER")
PASSWORD = env.get("DB_PASSWORD")
NAME = env.get("DB_NAME")