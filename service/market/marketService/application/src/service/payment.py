
from flask import request
import repository.payment as RP
from configuration.security import generate
from utility.encryption import Encrypt
from model.payment import Payment

def add():
    data = dict(request.get_json())
    userId, orderId, amount = data['user_id'], data['order_id'], data['amount']
    return RP.add(userId, orderId, amount)

def get():
    result = False
    data = dict(request.get_json())
    paymentId = data['payment_id']
    try:
        result = RP.get(paymentId)
    except: pass
    return result

def getOrderPayment(start, qnt):
    result = False
    data = dict(request.get_json())
    orderId = data['order_id']
    try:
        result = RP.getOrderPayment(orderId, start, qnt)
    except: pass
    return result

def getUserPayment(start, qnt):
    result = False
    data = dict(request.get_json())
    userId = data['user_id']
    try:
        result = RP.getUserPayment(userId, start, qnt)
    except: pass
    return result

def getDateOrder(start, qnt):
    result = False
    data = dict(request.get_json())
    date = data['date']
    try:
        result = RP.getDateOrder(date, start, qnt)
    except: pass
    return result

def getRange(start, qnt):
    result = False
    try:
        result = RP.getRange(start, qnt)
    except: pass
    return result

def update():
    result = False
    data = dict(request.get_json())
    try:
        result = RP.update(Payment(**data))
    except: pass
    return result

def delete():
    data = dict(request.get_json())
    paymentId = data['payment_id']
    return RP.delete(paymentId)
