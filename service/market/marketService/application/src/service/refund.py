
from flask import request
import repository.refund as RR
from configuration.security import generate
from utility.encryption import Encrypt
from model.refund import Refund

def add():
    data = dict(request.get_json())
    paymentId, state = data['payment_id'], data['state']
    return RR.add(paymentId, state)

def get():
    result = False
    data = dict(request.get_json())
    refundId = data['refund_id']
    try:
        result = RR.get(refundId)
    except: pass
    return result

def getPaymentRefund(start, qnt):
    result = False
    data = dict(request.get_json())
    paymentId = data['payment_id']
    try:
        result = RR.getPaymentRefund(paymentId, start, qnt)
    except: pass
    return result

def getDateOrder(start, qnt):
    result = False
    data = dict(request.get_json())
    date = data['date']
    try:
        result = RR.getDateOrder(date, start, qnt)
    except: pass
    return result

def getRange(start, qnt):
    result = False
    try:
        result = RR.getRange(start, qnt)
    except: pass
    return result

def update():
    result = False
    data = dict(request.get_json())
    try:
        result = RR.update(Refund(**data))
    except: pass
    return result

def delete():
    data = dict(request.get_json())
    refundId = data['refund_id']
    return RR.delete(refundId)
