
from flask import request
import repository.order as RO
from configuration.security import generate
from utility.encryption import Encrypt
from model.order import Order
import constant.values as VALUES

def add():
    data = dict(request.get_json())
    userId, productId, state, address, charact, price = data['user_id'], data['product_id'], VALUES.INITIAL_STATE, data['address'], data['charact'], data['price']
    return RO.add(userId, productId, state, address, str(charact).replace("'", '"'), price)

def get():
    result = False
    data = dict(request.get_json())
    orderId = data['order_id']
    try:
        result = RO.get(orderId)
    except: pass
    return result

def getUserOrder(start, qnt):
    result = False
    data = dict(request.get_json())
    userId = data['user_id']
    try:
        result = RO.getUserOrder(userId, start, qnt)
    except: pass
    return result

def getProductOrder(start, qnt):
    result = False
    data = dict(request.get_json())
    productId = data['product_id']
    try:
        result = RO.getProductOrder(productId, start, qnt)
    except: pass
    return result

def getDateOrder(start, qnt):
    result = False
    data = dict(request.get_json())
    date = data['date']
    try:
        result = RO.getDateOrder(date, start, qnt)
    except: pass
    return result

def getRange(start, qnt):
    result = False
    try:
        result = RO.getRange(start, qnt)
    except: pass
    return result

def update():
    result = False
    data = dict(request.get_json())
    try:
        result = RO.update(Order(**data))
    except: pass
    return result

def delete():
    data = dict(request.get_json())
    orderId = data['order_id']
    return RO.delete(orderId)
