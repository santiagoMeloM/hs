
class Order:
    def __init__(self, id=None, user_id=None, product_id=None, created=None, state=None, address=None):
        self.id = id
        self.user_id = user_id
        self.product_id = product_id
        self.created = created
        self.state = state
        self.address = address
    
    def getId(self):
        return self.id
    
    def getUserId(self):
        return self.user_id
    
    def getProductId(self):
        return self.product_id
    
    def getCreated(self):
        return self.created
    
    def getState(self):
        return self.state
    
    def getAddress(self):
        return self.address
    
    def toMap(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "product_id": self.product_id,
            "created": self.created,
            "state": self.state,
            "address": self.address
        }
    
    def toArray(self):
        return [self.id, self.user_id, self.product_id, self.created, self.state, self.address]
