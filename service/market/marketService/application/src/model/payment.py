
class Payment:
    def __init__(self, id=None, user_id=None, order_id=None, amount=None, created=None):
        self.id = id
        self.user_id = user_id
        self.order_id = order_id
        self.amount = amount
        self.created = created
    
    def getId(self):
        return self.id
    
    def getUserId(self):
        return self.user_id
    
    def getOrderId(self):
        return self.order_id
    
    def getAmount(self):
        return self.amount
    
    def getCreated(self):
        return self.created
    
    def toMap(self):
        return {
            "id": self.id,
            "user_id": self.user_id,
            "order_id": self.order_id,
            "amount": self.amount,
            "created": self.created
        }
    
    def toArray(self):
        return [self.id, self.user_id, self.order_id, self.amount, self.created]
