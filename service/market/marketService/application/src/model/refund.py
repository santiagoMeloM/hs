
class Refund:
    def __init__(self, id=None, payment_id=None, state=None, created=None):
        self.id = id
        self.payment_id = payment_id
        self.state = state
        self.created = created
    
    def getId(self):
        return self.id
    
    def getPaymentId(self):
        return self.payment_id
    
    def getState(self):
        return self.state
    
    def getCreated(self):
        return self.created
    
    def toMap(self):
        return {
            "id": self.id,
            "payment_id": self.payment_id,
            "state": self.state,
            "created": self.created
        }
    
    def toArray(self):
        return [self.id, self.payment_id, self.state, self.created]
