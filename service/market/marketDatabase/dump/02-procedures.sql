
DELIMITER $$
DROP PROCEDURE IF EXISTS addOrder;
CREATE PROCEDURE addOrder(IN uid int, IN pid int, IN stt varchar(50), IN addrs varchar(100), IN chrct json, IN prc double(20,4))
BEGIN
    INSERT INTO `order` (user_id, product_id, created, state, address, charact, price) VALUES (uid, pid, NOW(), stt, addrs, chrct, prc);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addPayment;
CREATE PROCEDURE addPayment(IN uid int, IN oid int, IN amnt double(20,4))
BEGIN
    DECLARE c int;
    SELECT COUNT(*) INTO c FROM `order` WHERE id = oid;
    IF c > 0 THEN
        INSERT INTO payment (user_id, order_id, amount, created) VALUES (uid, oid, amnt, NOW());
        SELECT LAST_INSERT_ID() as rows;
    ELSE
        SELECT c as rows;
    END IF;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addRefund;
CREATE PROCEDURE addRefund(IN pid int, IN stt varchar(50))
BEGIN
    INSERT INTO refund (payment_id, created, state) VALUES (pid, NOW(), stt);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateOrder;
CREATE PROCEDURE updateOrder(IN oid int, IN uid int, IN pid int, IN crtd date, IN stt varchar(50), IN addrs varchar(100))
BEGIN
    UPDATE `order` SET user_id=uid, product_id=pid, created=crtd, state=stt, address=addrs WHERE id=oid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updatePayment;
CREATE PROCEDURE updatePayment(IN pid int, IN uid int, IN oid int, IN amnt double(20,4), IN crtd date)
BEGIN
    UPDATE payment SET user_id=uid, order_id=oid, created=crtd, amount=amnt WHERE id=pid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateRefund;
CREATE PROCEDURE updateRefund(IN rid int, IN pid int, IN stt varchar(50), IN crtd date)
BEGIN
    UPDATE refund SET payment_id=pid, created=crtd, state=stt WHERE id=rid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteOrder;
CREATE PROCEDURE deleteOrder(IN oid int)
BEGIN
    DELETE FROM `order` WHERE id=oid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deletePayment;
CREATE PROCEDURE deletePayment(IN pid int)
BEGIN
    DELETE FROM payment WHERE id=pid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteRefund;
CREATE PROCEDURE deleteRefund(IN rid int)
BEGIN
    DELETE FROM refund WHERE id=rid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;