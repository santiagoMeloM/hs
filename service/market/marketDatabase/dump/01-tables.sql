
-- tables
-- Table: order
CREATE TABLE `order` (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    product_id int NOT NULL,
    charact json NOT NULL,
    price double(20,4) NOT NULL,
    created date NOT NULL,
    state varchar(50) NOT NULL,
    address varchar(100) NOT NULL,
    CONSTRAINT order_pk PRIMARY KEY (id)
);

-- Table: payment
CREATE TABLE payment (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    order_id int NOT NULL,
    amount double(20,4) NOT NULL,
    created date NOT NULL,
    CONSTRAINT payment_pk PRIMARY KEY (id)
);

-- Table: refund
CREATE TABLE refund (
    id int NOT NULL AUTO_INCREMENT,
    payment_id int NOT NULL,
    created date NOT NULL,
    state varchar(50) NOT NULL,
    CONSTRAINT refund_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: refund_payment (table: refund)
ALTER TABLE refund ADD CONSTRAINT refund_payment FOREIGN KEY refund_payment (payment_id)
    REFERENCES payment (id);

-- Reference: payment_order (table: payment)
ALTER TABLE payment ADD CONSTRAINT payment_order FOREIGN KEY payment_order (order_id)
    REFERENCES `order` (id);

