
-- tables
-- Table: profile
CREATE TABLE profile (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    name varchar(100) NOT NULL,
    address varchar(100) NULL,
    birthdate date NULL,
    CONSTRAINT profile_pk PRIMARY KEY (id)
);

-- Table: role
CREATE TABLE role (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    CONSTRAINT role_pk PRIMARY KEY (id)
);

-- Table: user
CREATE TABLE user (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    created date NOT NULL,
    active boolean NOT NULL,
    UNIQUE INDEX username_unique (username),
    CONSTRAINT user_pk PRIMARY KEY (id)
);

-- Table: user_role
CREATE TABLE user_role (
    id int NOT NULL AUTO_INCREMENT,
    role_id int NOT NULL,
    user_id int NOT NULL,
    CONSTRAINT user_role_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: profile_user (table: profile)
ALTER TABLE profile ADD CONSTRAINT profile_user FOREIGN KEY profile_user (user_id)
    REFERENCES user (id) ON DELETE CASCADE;

-- Reference: user_role_role (table: user_role)
ALTER TABLE user_role ADD CONSTRAINT user_role_role FOREIGN KEY user_role_role (role_id)
    REFERENCES role (id) ON DELETE CASCADE;

-- Reference: user_role_user (table: user_role)
ALTER TABLE user_role ADD CONSTRAINT user_role_user FOREIGN KEY user_role_user (user_id)
    REFERENCES user (id) ON DELETE CASCADE;