
DELIMITER $$
DROP PROCEDURE IF EXISTS getRoles;
CREATE PROCEDURE getRoles(IN usernm varchar(50))
BEGIN
	DECLARE uid int;
    SELECT id INTO uid FROM user WHERE username=usernm;
    SELECT r.id, r.name FROM user_role ur INNER JOIN role r ON (ur.user_id = uid) 
		WHERE role_id=r.id;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS assignRole;
CREATE PROCEDURE assignRole(IN usernm varchar(50), IN rol varchar(100))
BEGIN
	DECLARE rid int;
    DECLARE uid int;
    SELECT id INTO uid FROM user WHERE username=usernm;
    SELECT id INTO rid FROM role WHERE name=rol;
    INSERT INTO user_role (user_id, role_id) VALUES (uid, rid);
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addUser;
CREATE PROCEDURE addUser(IN usernm varchar(50), IN psswrd varchar(100), IN rol varchar(100))
BEGIN
    INSERT INTO user (username, password, active, created) VALUES (usernm, psswrd, 0, NOW());
    SELECT LAST_INSERT_ID() as rows;
    CALL assignRole(usernm, rol);
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS activateUser;
CREATE PROCEDURE activateUser(IN usernm varchar(50))
BEGIN
    UPDATE user SET active=1 WHERE username=usernm;
    SELECT active as rows FROM user WHERE username=usernm;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteUser;
CREATE PROCEDURE deleteUser(IN usernm varchar(50))
BEGIN
    DELETE FROM user WHERE username=usernm;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

