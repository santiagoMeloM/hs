
INSERT INTO role (name) VALUES ("user"), ("admin");
CALL addUser("santiago", "$pbkdf2-sha256$30000$IKQUAqBUCgHAuPeecy6FcA$WqnuqP4UEA/Emx23BHbapwr11SaG1Bhxgy/NpliQuqA", "user");
CALL assignRole("santiago", "admin");
CALL activateUser("santiago");

