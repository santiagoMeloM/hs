import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import constant.email.email as EMAIL
import constant.email.template as TEMPLATE


class Email:
    def __init__(self):
        self.smtp = smtplib.SMTP_SSL(EMAIL.HOST, EMAIL.PORT, context=ssl.create_default_context())
        self.message = MIMEMultipart()
        self.smtp.login(EMAIL.EMAIL, EMAIL.PASSWORD)
    
    def __send(self, email, subject, message):
        self.message["Subject"] = subject
        self.message["From"] = EMAIL.EMAIL
        self.message["To"] = email
        self.message.attach(MIMEText(message, "html"))
        self.smtp.sendmail(EMAIL.EMAIL, email, self.message.as_string())
        return
    
    def emailWelcome(self, email, params):
        template = TEMPLATE.WELCOME[params["language"]]["content"](email, params)
        self.__send(email, TEMPLATE.WELCOME[params["language"]]["subject"], template)
        return