
from flask import request
import repository.user as RU
from configuration.security import generate
from utility.encryption import Encrypt
from configuration.app import catching
import constant.security as SECURITY
from utility.email import Email
from constant.email.email import VERIFY_LINK

def authenticate():
    token = False
    data = dict(request.get_json())
    username, password = data['username'], data['password']
    tmp = RU.authenticate(username)
    if tmp and Encrypt().setString(tmp).validate(password):
        claims = {"roles":RU.getRoles(username), "username":username}
        token = generate(claims, SECURITY.EXPIRE_TIME).decode("ascii")
        if not catching.exists(username):
            catching.setex(username, SECURITY.EXPIRE_TIME, token)
        else:
            token = catching.get(username).decode("ascii")
    return token

def session():
    token = False
    data = dict(request.get_json())
    username = data['username']
    if catching.exists(username):
        token = catching.get(username).decode("ascii")
    return token

def desession():
    result = False
    data = dict(request.get_json())
    username = data['username']
    if catching.exists(username):
        result = catching.expire(username, 0)
    return result

def verifyAccount(user, token):
    result = False
    key = "{}_{}".format(user, token)
    if catching.exists(key):
        tmp = RU.activate(user)
        if tmp: result = catching.expire(key, 0)
    return result

def add():
    data = dict(request.get_json())
    username, password, language = data['username'], data['password'], data["language"]
    password = Encrypt().encrypt(password).getString()
    tmpToken = generate({}, SECURITY.EXPIRE_TIME_TMP).decode("ascii")
    try:
        result = RU.add(username, password)
        catching.setex("{}_{}".format(username, tmpToken), SECURITY.EXPIRE_TIME_TMP, tmpToken)
        smtp = Email().emailWelcome(username, {"language":language, "verify_link": VERIFY_LINK.format(user=username, token=tmpToken)})
    except Exception as e:
        result = False
        RU.delete(username)
        catching.expire("{}_{}".format(username, tmpToken), 0)
    return result

def get():
    result = False
    data = dict(request.get_json())
    username = data['username']
    try:
        result = RU.get(username)
    except: pass
    return result

def delete():
    result = False
    data = dict(request.get_json())
    username, password = data['username'], data['password']
    tmp = RU.authenticate(username)
    if tmp and Encrypt().setString(tmp).validate(password):
        result = RU.delete(username)
    return result
    