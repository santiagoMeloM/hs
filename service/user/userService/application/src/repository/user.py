
from configuration.app import db

def authenticate(username):
    result = False
    qry = "SELECT password FROM user WHERE username='{}' AND active=1".format(username)
    try:
        result = db.fetchOne(qry)['password']
    except: pass
    return result

def add(username, password):
    result = False
    try:
        tmp = db.procedure("addUser", [username, password, "user"])
        result = tmp[0]["rows"]
    except: pass
    return result

def activate(username):
    result = False
    try:
        tmp = db.procedure("activateUser", [username])
        result = tmp[0]["rows"]
    except: pass
    return result

def getRoles(username):
    result = db.procedure("getRoles", [username])
    return [dict(r)['name'] for r in result]

def get(username):
    qry = "SELECT id, username, created FROM user WHERE username='{}'".format(username)
    result = db.fetchOne(qry)
    return result

def delete(username):
    result = False
    try:
        tmp = db.procedure("deleteUser", [username])
        result = tmp[0]["rows"] > 0
    except: pass
    return result