
import os

env = os.environ

## Debug values
# env['DS_HOSTINSTANCE'] = "".join(open("/etc/hostname", 'r').readlines()).split()[0]
# env['DS_PROTOCOL'] = "http://"
# env['DS_HOSTDISCOVER'] = "http://localhost:8000/eureka/apps"
# env['DS_PORTDISCOVER'] = "8000"
# env['DS_SERVICENAME'] = "USER_SERVICE"
# env['DS_POSTINSTANCE'] = "4100"
# env['DS_HEARTBEATDISCOVER'] = "30"



INFO_DISCOVER = {
    "PROTOCOL": env.get('DS_PROTOCOL'),
    "HOSTDISCOVER": env.get('DS_HOSTDISCOVER'),
    "PORTDISCOVER": env.get('DS_PORTDISCOVER'),
    "SERVICENAME": env.get('DS_SERVICENAME'),
    "HOSTINSTANCE": env.get('DS_HOSTINSTANCE'),
    "PORTINSTANCE": env.get('DS_POSTINSTANCE'),
    "HEARTBEATDISCOVER": env.get('DS_HEARTBEATDISCOVER')
}