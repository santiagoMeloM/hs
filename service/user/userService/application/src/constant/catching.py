
import os

env = os.environ

IP = env.get("CATCHING_IP")
PORT = env.get("CATCHING_PORT")
DB = 0
PASSWORD = env.get("CATCHING_PASS")