
from constant.email.images import LOGO

WELCOME = {
    "ENGLISH": {
        "subject": "Welcome to High Support!",
        "content": lambda email, params: """
        <html>
            <body>
                <img src="{logo}" title="Logo" style="display:block" width="300" height="300" />
                <p>Welcome <b>{username}</b>!</p>
                <p>We are glad that you have decided to join us, please verify your account with the link below before you can log in:</p>
                <a href="{verify_link}">Verify my account</a><br><br>
                <p>Thank you again so much for choosing us!</p><br>
                <i>Sincerely</i>,
                <p>High Support Team</p>
            </body>
        </html>
        """.format(username=email, verify_link=params["verify_link"], logo=LOGO)
    },
    "SPANISH": {
        "subject": "Bienvenido a High Support!",
        "content": lambda email, params: """
        <html>
            <body>
                <img src="{logo}" title="Logo" style="display:block" width="300" height="300" />
                <p>Bienvenido <b>{username}</b>!</p>
                <p>Estamos orgullosos que te unas a nosotros, por favor verifica tu cuenta con el link de abajo antes de poder iniciar sesión:</p>
                <a href="{verify_link}">Verificar mi cuenta</a><br><br>
                <p>Muchas gracias de nuevo por escogernos!</p><br>
                <i>Sinceramente</i>,
                <p>Equipo High Support</p>
            </body>
        </html>
        """.format(username=email, verify_link=params["verify_link"], logo=LOGO)
    }
}