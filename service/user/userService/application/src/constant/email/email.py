

import os

env = os.environ

# env['EMAIL_ACCOUNT'] = "hsclothingspprt@gmail.com"
# env['EMAIL_PASSWORD'] = "MiChichi07"
# env['EMAIL_HOST'] = "smtp.gmail.com"
# env['EMAIL_PORT'] = "465"

EMAIL = env.get("EMAIL_ACCOUNT")
PASSWORD = env.get("EMAIL_PASSWORD")
HOST = env.get("EMAIL_HOST")
PORT = int(env.get("EMAIL_PORT"))

VERIFY_LINK = "https://inventory.highsclothing.shop/hs/user/user/verify/{user}/{token}"