
import os

env = os.environ

## debug values
# env["IP"] = "0.0.0.0"
# env["PORT"] = "4100"

IP = env.get("IP")
PORT = env.get("PORT")