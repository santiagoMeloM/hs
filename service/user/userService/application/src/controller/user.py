
from flask import Blueprint, request, jsonify
from configuration.auth import system, user
import service.user as SU

alias = "user"

controller = Blueprint(alias, __name__)

@controller.route("/authenticate", methods=["POST"])
@system
def authenticate():
    response = {"response": 400, "content": {}}
    token = SU.authenticate()
    if token:
        response = {"response": 200, "content": {"token": token}}
    return jsonify(response)

@controller.route("/session", methods=["POST"])
@system
def session():
    response = {"response": 400, "content": {}}
    token = SU.session()
    if token:
        response = {"response": 200, "content": {"token": token}}
    return jsonify(response)

@controller.route("/desession", methods=["POST"])
@system
@user
def desession():
    response = {"response": 400, "content": {}}
    result = SU.desession()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/verify/<string:user>/<string:token>".format(alias), methods=["GET"])
def verify(user, token):
    response = {"response": 400, "content": {}}
    result = SU.verifyAccount(user, token)
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
def add():
    response = {"response": 400, "content": {}}
    result = SU.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/getUser".format(alias), methods=["POST"])
@user
def getUser():
    response = {"response": 400, "content": {}}
    data = SU.get()
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@user
def delete():
    response = {"response": 400, "content": {}}
    result = SU.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

