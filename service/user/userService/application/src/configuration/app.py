
from flask import Flask
from eurekaClientShare.client.client import DiscoveryClient
from constant.discover import INFO_DISCOVER
from configuration.database import Database
import redis
import constant.catching as CATCHING
# from flask_cors import CORS

db = Database()
discover = DiscoveryClient(INFO_DISCOVER)
catching = redis.Redis(host=CATCHING.IP, port=CATCHING.PORT, db=CATCHING.DB)

app = Flask(__name__)
# CORS(app, resources={r"/*": {"origins": "*"}})
# app.config['CORS_HEADERS'] = 'Content-Type'

