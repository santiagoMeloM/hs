
import os

env = os.environ

## Debug values
# env['JWT_KEY'] = "123"
# env['JWT_ALGORITHM'] = "HS256"

KEY = env.get("JWT_KEY")
ALGORITHM = env.get("JWT_ALGORITHM")