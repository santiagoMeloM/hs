
const app = require("./src/configuration/router");
const discovery = require("./src/configuration/discovery");
const CONN = require("./src/constant/connection");
const cors = require("cors");

app.use(cors());
app.get("/", (req, res) => {
    res.send("Hello from producer");
});


// discovery.start((error) => {
//     if (error == null) {
//         app.listen(CONN.PORT);
//     }
// });

app.listen(CONN.PORT);

process.on('SIGINT', function() {
    discovery.stop((error) => {
        if (error == null) {
            console.log("Producer turning off...");
            process.exit();
        }
    });
});
