
const ADD_MEDIA = require("./transactions/addMedia");
const ADD_PRODUCT = require("./transactions/addProduct");

const CONST = {
    TRANSACTIONS: {
        ADD_MEDIA: ADD_MEDIA,
        ADD_PRODUCT: ADD_PRODUCT
    }
}

module.exports = CONST;