
const CONST = {
    JWT: {
        KEY: process.env["JWT_KEY"],
        ALGORITHM: process.env["JWT_ALGORITHM"]
    },
    ROLES: {
        SYSTEM: ["system"],
        ADMIN: ["admin"],
        USER: ["user"]
    }
};

module.exports = CONST;