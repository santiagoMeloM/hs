
SUCCESS = (element = "") => {return {
    response: 200,
    content: `Processing ${element}`
}}

FORBIDDEN = (element = "") => {return {
    response: 403,
    content: `Cant start processing ${element}`
}}

ERROR = (element = "") => {return {
    response: 400,
    content: `Cant start processing ${element}`
}}

module.exports = {
    SUCCESS,
    FORBIDDEN,
    ERROR
}