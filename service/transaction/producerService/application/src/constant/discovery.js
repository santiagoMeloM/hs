
const CONN = require("./connection");

const CONST = {
    INSTANCE: {
        APP: process.env["APP_INSTANCE"],
        HOSTNAME: process.env["HOSTNAME_INSTANCE"],
        PORT: CONN.PORT,
        IP_ADDRESS: process.env["IPADDRESS_INSTANCE"],
    }, 
    HOST: {
        IP: process.env["HOST_DISCOVERY"],
        PORT: parseInt(process.env["PORT_DISCOVERY"]),
        PATH: process.env["PATH_DISCOVERY"]
    }
}

module.exports = CONST;