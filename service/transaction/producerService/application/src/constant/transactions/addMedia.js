
const TYPES = require("../media/types");

const CONST = {
    INFO: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/media/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "type": body["type"]
            }
        }
    },
    CONTENT: (body, params, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/media/add/content`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "image": body["image"],
                "folder": TYPES[params["type"].toString()]["folder"],
                "ext": TYPES[params["type"].toString()]["ext"]
            }
        }
    },
    ASSIGN: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/folder/assign`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "media_id": body["media_id"],
                "folder_id": body["folder_id"]
            }
        }
    },
    ACTIVATE: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/product/update/state`,
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "product_id": body["product_id"]
            }
        }
    }
}

module.exports = CONST;