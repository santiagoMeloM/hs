
const CONST = {
    ORDER: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/market/order/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "user_id": body["user_id"],
                "product_id": body["product_id"],
                "address": body["address"],
                "charact": body["charact"],
                "price": body["price"]
            }
        }
    },
    PAYMENT: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/market/payment/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "user_id": body["user_id"],
                "order_id": 0,
                "amount": body["amount"]
            }
        }
    } // lacking update product characteristics and update order status
}

module.exports = CONST;