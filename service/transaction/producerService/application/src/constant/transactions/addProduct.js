
const CONST = {
    PRODUCT: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/product/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "name": body["name"],
                "description": body["description"],
                "price": body["price"]
            }
        }
    },
    ASSIGN_CATEGORY: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/category/add/product`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "product_id": 0,
                "category_id": body["category_id"]
            }
        }
    },
    CHARACTERISTIC: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/charact/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "name": body["name"],
                "content": body["content"]
            }
        }
    },
    ASSIGN_CHARACTERISTIC: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/charact/add/product`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "product_id": 0,
                "charact_id": 0
            }
        }
    },
    FOLDER: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/folder/add`,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["sauthentication"],
                "Authentication": headers["authentication"]
            },
            json: {
                "product_id": body["product_id"]
            }
        }
    }
}

module.exports = CONST;