
const CONST = {
    "1": {
        "media": "image/jpeg",
        "folder": "data/real",
        "ext": "jpg"
    },
    "2": {
        "media": "video/mp4",
        "folder": "videos",
        "ext": "mp4"
    }
};

module.exports = CONST;