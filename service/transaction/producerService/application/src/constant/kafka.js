
const CONST = {
    SERVICE: {
        PRODUCER: "PRODUCER"
    },
    BROKERS: process.env["KAFKA_BROKERS"].split(","),
    TOPICS: {
        TRANSACTION: "TRANSACTION",
        ROLLBACK: "ROLLBACK"
    }
}

module.exports = CONST;