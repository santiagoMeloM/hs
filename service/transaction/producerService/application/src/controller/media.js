
const router = require("express").Router();
const service = require("../service/media/media");
const auth = require("../configuration/auth");
const RESPONSE = require("../constant/response");
const ELEMENT = require("../constant/response");


router.post("/addMedia", auth.system, auth.admin, (req, res) => {
    let tmp = null;
    service.addMedia(req).then(result => {
        tmp = result ? RESPONSE.SUCCESS : RESPONSE.ERROR;
        let response = tmp(ELEMENT.MEDIA);
        res.status(response.response).json(response);
    });
});

router.post("/addProduct", auth.system, auth.admin, (req, res) => {
    let tmp = null;
    service.addProduct(req).then(result => {
        tmp = result ? RESPONSE.SUCCESS : RESPONSE.ERROR;
        let response = tmp(ELEMENT.PRODUCT);
        res.status(response.response).json(response);
    });
});



module.exports = router;