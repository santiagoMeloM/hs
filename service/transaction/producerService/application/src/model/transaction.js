
const { Operation } = require("./operation");
const OPERATION = require("../constant/operation");

class Transaction {

    constructor(title) {
        this.title = title;
        this.transactions = new Map();
        this.results = {};
    }

    setTransactions(transactions) {
        this.transactions = transactions;
        return this;
    }

    setString(transaction) {
        let tmp = transaction.split("\n");
        let result = new Map();
        this.title = tmp[0];
        this.results = JSON.parse(tmp[1]);
        for (let i = 2; i < tmp.length; i++) {
            try {
                let keyValue = tmp[i].split("@@---@@");
                result.set(keyValue[0], new Operation(null,null,null).setString(keyValue[1]));
            } catch (error) {
            }
        }
        this.transactions = result;
        return this;
    }

    addOperation(id, title, petition) {
        this.transactions.set(title, new Operation(id, title, petition));
        if (Array.from(this.transactions.keys()).length == 1) this.transactions.get(title).setPending();
        return this;
    }

    addResult(key, value) {
        this.results[key] = value;
        return this;
    }
    
    getString() {
        let tmp = `${this.title}\n`;
        tmp += `${JSON.stringify(this.results)}\n`;
        this.transactions.forEach((operation, key) => {
            tmp += `${key}@@---@@${operation.getString()}\n`;
        });
        return tmp;
    }

    getPending() {
        return Array.from(this.transactions.values()).filter(operation => {
            return operation.getStatus() == OPERATION.STATUS["pen"];
        })[0];
    }

    getError() {
        return Array.from(this.transactions.values()).filter(operation => {
            return operation.getStatus() == OPERATION.STATUS["err"];
        })[0];
    }

    getResult() {
        return this.results;
    }

    getTransaction() {
        return this.transactions;
    }

    getTitle() {
        return this.title;
    }

}

module.exports = {
    Transaction
};