
const express = require("express");
const app = express();

const bodyParse = require("body-parser");

app.use(express.json());
app.use(bodyParse.raw({
    type: "video/mp4",
    limit: "100mb"
}));

app.use(bodyParse.raw({
    type: "image/jpeg",
    limit: "50mb"
}));

module.exports = app;