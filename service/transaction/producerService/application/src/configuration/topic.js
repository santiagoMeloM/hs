

const { Kafka } = require("kafkajs");
const KAFKA = require("../constant/kafka");

start(KAFKA.TOPICS.TRANSACTION, 1);
start(KAFKA.TOPICS.ROLLBACK, 1);

async function start(topicName, partitions) {
    try {
        const kafka = new Kafka({
            "clientId": 'temporalTopicSetting',
            "brokers": KAFKA.BROKERS
        });

        console.log("Connection opened");
        const admin = kafka.admin();

        await admin.connect();

        await admin.createTopics({
            "topics": [{
                "topic": topicName,
                "numPartitions": partitions
            }]
        });
        console.log("Created topics");

        await admin.disconnect();
        console.log("Connection closed");

    } catch(error) {
        console.log("Error creating topic", error);
    } finally {
        process.exit(0);
    }
}
