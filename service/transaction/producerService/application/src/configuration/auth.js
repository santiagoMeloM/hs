
const jwt = require("jsonwebtoken");
const AUTH = require("../constant/auth");
const RESPONSE = require("../constant/response");


const system = (req, res, next) => {
    let forbidden = RESPONSE.FORBIDDEN();
    try {
        const sauthentication = req.headers['sauthentication'];
        if (sauthentication) {
            jwt.verify(sauthentication, AUTH.JWT.KEY, { algorithms: [AUTH.JWT.ALGORITHM] }, (err, decoded) => {
                if (AUTH.ROLES.SYSTEM.every((val) => {return decoded["roles"].indexOf(val) >= 0;})) {
                    console.log("passed system");
                    return next();
                } else {
                    return res.status(forbidden.CODE).send(forbidden);
                }
            });
        } else {
            return res.status(forbidden.CODE).send(forbidden);
        }
    } catch {
        return res.status(forbidden.CODE).send(forbidden);
    }
};

const admin = (req, res, next) => {
    let forbidden = RESPONSE.FORBIDDEN();
    try {
        const authentication = req.headers['authentication'];
        if (authentication) {
            jwt.verify(authentication, AUTH.JWT.KEY, { algorithms: [AUTH.JWT.ALGORITHM] }, (err, decoded) => {
                if (AUTH.ROLES.ADMIN.every((val) => {return decoded["roles"].indexOf(val) >= 0;})) {
                    console.log("passed admin");
                    return next();
                } else {
                    return res.status(forbidden.CODE).send(forbidden);
                }
            });
        } else {
            return res.status(forbidden.CODE).send(forbidden);
        }
    } catch {
        return res.status(forbidden.CODE).send(forbidden);
    }
};

const user = (req, res, next) => {
    let forbidden = RESPONSE.FORBIDDEN();
    try {
        const authentication = req.headers['authentication'];
        if (authentication) {
            jwt.verify(authentication, AUTH.JWT.KEY, { algorithms: [AUTH.JWT.ALGORITHM] }, (err, decoded) => {
                if (AUTH.ROLES.USER.every((val) => {return decoded["roles"].indexOf(val) >= 0;})) {
                    console.log("passed user");
                    return next();
                } else {
                    return res.status(forbidden.CODE).send(forbidden);
                }
            });
        } else {
            return res.status(forbidden.CODE).send(forbidden);
        }
    } catch {
        return res.status(forbidden.CODE).send(forbidden);
    }
};


module.exports = {
    system, 
    admin, 
    user
};