
const Eureka = require('eureka-js-client').Eureka;
const DISCOVERY = require("../constant/discovery");

const client = new Eureka({
    instance: {
        app: DISCOVERY.INSTANCE.APP,
        hostName: DISCOVERY.INSTANCE.HOSTNAME,
        ipAddr: DISCOVERY.INSTANCE.IP_ADDRESS,
        port: {
            '$': DISCOVERY.INSTANCE.PORT,
            '@enabled': 'true',
        },
        vipAddress: DISCOVERY.INSTANCE.IP_ADDRESS,
        dataCenterInfo: {
            '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
            name: 'MyOwn',
        }
    },
    eureka: {
        host: DISCOVERY.HOST.IP,
        port: DISCOVERY.HOST.PORT,
        servicePath: DISCOVERY.HOST.PATH
    }
});

module.exports = client;