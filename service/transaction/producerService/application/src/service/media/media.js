
const { Transaction } = require("../../model/transaction");
const { KafkaClient } = require("../../utility/kafkaClient");
const MEDIA = require("../../constant/media");
const KAFKA = require("../../constant/kafka");
const kafkaClient = require("../../utility/kafkaClient");
const imageService = require("../image");


let addMedia = (req) => {
    return new Promise((resolve, reject) => {
        try {
            let params = req.query;
            let headers = req.headers;
            let body = req.body
            let transaction = new Transaction("ADD_MEDIA");
            transaction.addOperation("1", "INFO", MEDIA.TRANSACTIONS.ADD_MEDIA.INFO(params, headers));
            transaction.addOperation("2", "CONTENT", MEDIA.TRANSACTIONS.ADD_MEDIA.CONTENT(body, params, headers));
            transaction.addOperation("3", "ASSIGN", MEDIA.TRANSACTIONS.ADD_MEDIA.ASSIGN(params, headers));
            transaction.addOperation("4", "ACTIVATE", MEDIA.TRANSACTIONS.ADD_MEDIA.ACTIVATE(params, headers));
            let client = new KafkaClient(KAFKA.SERVICE.PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.TRANSACTION);
            client.produce(transaction.getString(), 0);
            resolve(true);
        } catch(error) {
            resolve(false);
        }
    });
}

let addProduct = (req) => {
    return new Promise((resolve, reject) => {
        try {
            let headers = req.headers;
            let body = req.body
            let transaction = new Transaction("ADD_PRODUCT");
            transaction.addOperation("1", "PRODUCT", MEDIA.TRANSACTIONS.ADD_PRODUCT.PRODUCT(body, headers));
            transaction.addOperation("2", "ASSIGN_CATEGORY", MEDIA.TRANSACTIONS.ADD_PRODUCT.ASSIGN_CATEGORY(body, headers));
            transaction.addOperation("3", "CHARACTERISTIC", MEDIA.TRANSACTIONS.ADD_PRODUCT.CHARACTERISTIC(body, headers));
            transaction.addOperation("4", "ASSIGN_CHARACTERISTIC", MEDIA.TRANSACTIONS.ADD_PRODUCT.ASSIGN_CHARACTERISTIC(body, headers));
            transaction.addOperation("5", "FOLDER", MEDIA.TRANSACTIONS.ADD_PRODUCT.FOLDER(body, headers));
            let client = new KafkaClient(KAFKA.SERVICE.PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.TRANSACTION);
            client.produce(transaction.getString(), 0);
            resolve(true);
        } catch(error) {
            resolve(false);
        }
    });
}

module.exports = {
    addMedia,
    addProduct
}