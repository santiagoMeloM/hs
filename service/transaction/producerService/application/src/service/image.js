

let getBinaryBuffer = (req) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(req.body);
        } catch (error) {
            reject(error);
        }
    });
};

let getBinaryBinary = (req) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(req.body.toString("utf-8"));
        } catch (error) {
            reject(error);
        }
    });
};

let getBinaryBase64 = (req) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(req.body.toString("base64"));
        } catch (error) {
            reject(error);
        }
    });
};

module.exports = {
    getBinaryBuffer,
    getBinaryBinary,
    getBinaryBase64
};