
const {Kafka} = require('kafkajs');

class KafkaClient {

    constructor(serviceName, brokers, topic) {
        this.client = new Kafka({
            "clientId": serviceName,
            "brokers": brokers
        });
        this.topic = topic;
    }

    async consume(group, func) {
        const consumer = this.client.consumer({"groupId": group});
        await consumer.connect();
        await consumer.subscribe({
            "topic": this.topic,
            "fromBeginning": true
        });
        await consumer.run({
            "eachMessage": async (response) => {
                func(response);
            }
        });
    }

    async produce(msg, partition) {
        const producer = this.client.producer();
        await producer.connect();
        await producer.send({
            "topic": this.topic,
            "messages": [
                {
                    "value": msg,
                    "partition": partition
                }
            ]
        });
        await producer.disconnect();
    }
}

module.exports = {
    KafkaClient
}