
const { KafkaClient } = require("./src/utility/kafkaClient");
const { Transaction } = require("./src/model/transaction");
const SERVICES = require("./src/constant/service");
const KAFKA = require("./src/constant/kafka");

let client = new KafkaClient(KAFKA.SERVICE.CONSUMER, KAFKA.BROKERS, KAFKA.TOPICS.ROLLBACK);

client.consume(KAFKA.CONSUMER.GROUPS.MEDIA, (result) => {
    let transaction = new Transaction(null).setString(result);
    let pending = transaction.getPending();
    let error = transaction.getError();
    if (!(typeof pending === "undefined") || !(typeof error === "undefined")) {
        try {
            SERVICES[transaction.getTitle()](transaction, pending).then(result => {
                let tmpClient = new KafkaClient(KAFKA.SERVICE.CONSUMER_PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.ROLLBACK);
                tmpClient.produce(result.getString(), 0);
            });
        } catch (error) {
            console.log(`${transaction.getTitle()} -- Transaction failed`);
            sendProduce(transaction.getString());
        }
    } else {
        console.log(`${transaction.getTitle()} -- Transaction completed`);
    }
});

async function sendProduce(msg) {
    setTimeout(function() {
        let tmpClient = new KafkaClient(KAFKA.SERVICE.CONSUMER_PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.ROLLBACK);
        tmpClient.produce(msg, 0);
    }, 2000);
}