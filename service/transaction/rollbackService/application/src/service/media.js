
const request = require("request");
const MEDIA = require("../constant/media");

let addMediaRollback = (transaction, pending) => {
    let bodyStack = MEDIA.BODY_STACK.ADD_MEDIA[pending.getTitle()](pending.getPetition(), transaction.getResult());
    return new Promise((resolve, reject) => {
        pending.setPetition(bodyStack);
        request(pending.getPetition(), (error, res, body) => {
            if (error == null && body["response"] == 200) {
                transaction.addResult(pending.getTitle(), body["content"]);
                transaction.getTransaction().get(pending.getTitle()).setEnded();
            } else {
                transaction.addResult(pending.getTitle(), "ERROR");
                transaction.getTransaction().get(pending.getTitle()).setError();
            }
            resolve(transaction);
        }, (error) => {
            transaction.addResult(pending.getTitle(), "ERROR");
            transaction.getTransaction().get(pending.getTitle()).setError();
            resolve(transaction);
        });
    });
};

let addProductRollback = (transaction, pending) => {
    let bodyStack = MEDIA.BODY_STACK.ADD_PRODUCT[pending.getTitle()](pending.getPetition(), transaction.getResult());
    return new Promise((resolve, reject) => {
        pending.setPetition(bodyStack);
        request(pending.getPetition(), (error, res, body) => {
            if (error == null && body["response"] == 200) {
                transaction.addResult(pending.getTitle(), body["content"]);
                transaction.getTransaction().get(pending.getTitle()).setEnded();
            } else {
                transaction.addResult(pending.getTitle(), "ERROR");
                transaction.getTransaction().get(pending.getTitle()).setError();
            }
            resolve(transaction);
        }, (error) => {
            transaction.addResult(pending.getTitle(), "ERROR");
            transaction.getTransaction().get(pending.getTitle()).setError();
            resolve(transaction);
        });
    });
};


module.exports = {
    addMediaRollback,
    addProductRollback
}