
const OPERATION = require("../constant/operation");

class Operation {
    
    constructor(id, title, petition) {
        this.id = id;
        this.title = title;
        this.status = OPERATION.STATUS["cre"];
        this.petition = petition;
    }

    setPending() {
        this.status = OPERATION.STATUS["pen"];
        return this;
    }

    setError() {
        this.status = OPERATION.STATUS["err"];
        return this;
    }

    setEnded() {
        this.status = OPERATION.STATUS["end"];
        return this;
    }
    
    setPetition(petition) {
        this.petition = petition;
        return this;
    }

    setString(operation) {
        let tmp = operation.split("|");
        this.id = tmp[0];
        this.title = tmp[1];
        this.status = OPERATION.STATUS[tmp[2]];
        this.petition = JSON.parse(tmp[3]);
        return this;
    }

    getId() {
        return this.id;
    }

    getTitle() {
        return this.title;
    }

    getStatus() {
        return this.status;
    }

    getPetition() {
        return this.petition;
    }
    
    getString() {
        let petition = JSON.stringify(this.petition);
        return `${this.id}|${this.title}|${OPERATION.KEY[this.status]}|${petition}`;
    }
}

module.exports = {
    Operation
}