
const CONST = {
    BODY_STACK: {
        ADD_MEDIA: {
            INFO: (petition, results) => {
                return petition;
            },
            CONTENT: (petition, results) => {
                return petition;
            },
            ASSIGN: (petition, results) => {
                return petition;
            }
        },
        ADD_PRODUCT: {
            PRODUCT: (petition, results) => {
                return petition;
            },
            CHARACTERISTIC: (petition, results) => {
                return petition;
            },
            ASSIGN_CHARACTERISTIC: (petition, results) => {
                return petition;
            },
            ASSIGN_CATEGORY: (petition, results) => {
                return petition;
            }
        }
    }
}

module.exports = CONST;