
const CONST = {
    SERVICE: {
        CONSUMER: "CONSUMER_ROLLBACK",
        CONSUMER_PRODUCER: "CONSUMER_ROLLBACK_PRODUCER"
    },
    BROKERS: process.env["KAFKA_BROKERS"].split(","),
    TOPICS: {
        TRANSACTION: "TRANSACTION",
        ROLLBACK: "ROLLBACK"
    },
    CONSUMER: {
        GROUPS: {
            MEDIA: "second"
        }
    }
}

module.exports = CONST;