
const mediaService = require("../service/media");

const CONST = {
    "ROLL_ADD_MEDIA": mediaService.addMediaRollback,
    "ROLL_ADD_PRODUCT": mediaService.addProductRollback
}

module.exports = CONST;