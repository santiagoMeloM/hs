
const CONST = {
    STATUS: {
        "cre": "CREATED",
        "pen": "PENDING",
        "err": "ERROR",
        "end": "ENDED"
    },
    KEY: {
        "CREATED": "cre",
        "PENDING": "pen",
        "ERROR": "err",
        "ENDED": "end"
    }
}

module.exports = CONST;