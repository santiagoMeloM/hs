
const { KafkaClient } = require("./src/utility/kafkaClient");
const { Transaction } = require("./src/model/transaction");
const SERVICES = require("./src/constant/service");
const KAFKA = require("./src/constant/kafka");

let client = new KafkaClient(KAFKA.SERVICE.CONSUMER, KAFKA.BROKERS, KAFKA.TOPICS.TRANSACTION);

client.consume(KAFKA.CONSUMER.GROUPS.MEDIA, (result) => {
    let transaction = new Transaction(null).setString(result);
    let pending = transaction.getPending();
    let error = transaction.getError();
    if (typeof error === "undefined") {
        if (!(typeof pending === "undefined")) {
            try {
                operateTransaction(transaction, pending);
            } catch (error) {
                console.log(`${transaction.getTitle()} -- Transaction failed`);
                operateRollback(transaction);
            }
        } else {
            console.log(`${transaction.getTitle()} -- Transaction completed`);
        }
    } else {
        console.log(`${transaction.getTitle()} -- Transaction failed`);
        operateRollback(transaction);
    }
});


let operateTransaction = (transaction, pending) => {
    SERVICES[transaction.getTitle()](transaction, pending).then(result => {
        console.log(transaction.getResult());
        let tmpClient = new KafkaClient(KAFKA.SERVICE.CONSUMER_PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.TRANSACTION);
        tmpClient.produce(result.getString(), 0);
    });
}

let operateRollback = (transaction) => {
    SERVICES[`ROLL_${transaction.getTitle()}`](transaction).then(result => {
        let tmpClient = new KafkaClient(KAFKA.SERVICE.CONSUMER_PRODUCER, KAFKA.BROKERS, KAFKA.TOPICS.ROLLBACK);
        tmpClient.produce(result.getString(), 0);
    });
}