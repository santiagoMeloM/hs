
const CONST = {
    SERVICE: {
        CONSUMER: "CONSUMER_TRANSACTION",
        CONSUMER_PRODUCER: "CONSUMER_TRANSACTION_PRODUCER"
    },
    BROKERS: process.env["KAFKA_BROKERS"].split(","),
    TOPICS: {
        TRANSACTION: "TRANSACTION",
        ROLLBACK: "ROLLBACK"
    },
    CONSUMER: {
        GROUPS: {
            MEDIA: "first"
        }
    }
}

module.exports = CONST;