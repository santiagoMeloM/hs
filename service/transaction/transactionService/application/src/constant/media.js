
const ROLL_ADD_MEDIA = require("./rollback/addMedia");
const ROLL_ADD_PRODUCT = require("./rollback/addProduct");
const BS_ADD_MEDIA = require("./bodyStack/addMedia");
const BS_ADD_PRODUCT = require("./bodyStack/addProduct");
const FWRD_ADD_MEDIA = require("./forward/addMedia");
const FWRD_ADD_PRODUCT = require("./forward/addProduct");

const CONST = {
    ROLLBACK: {
        ADD_MEDIA: ROLL_ADD_MEDIA,
        ADD_PRODUCT: ROLL_ADD_PRODUCT
    },
    FORWARD: {
        ADD_MEDIA: FWRD_ADD_MEDIA,
        ADD_PRODUCT: FWRD_ADD_PRODUCT
    }, 
    BODY_STACK: {
        ADD_MEDIA: BS_ADD_MEDIA,
        ADD_PRODUCT: BS_ADD_PRODUCT
    },
    ROLLBACK_NAME: {
        "ADD_MEDIA": "ROLL_ADD_MEDIA",
        "ADD_PRODUCT": "ROLL_ADD_PRODUCT"
    }
}

module.exports = CONST;