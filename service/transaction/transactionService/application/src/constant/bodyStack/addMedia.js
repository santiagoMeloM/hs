
const CONST = {
    INFO: (petition, results) => {
        return petition;
    },
    CONTENT: (petition, results) => {
        petition["url"] = `${petition["url"]}/${results["INFO"]}.${petition["json"]["ext"]}`
        return petition;
    },
    ASSIGN: (petition, results) => {
        petition["json"]["media_id"] = results["INFO"];
        return petition;
    },
    ACTIVATE: (petition, results) => {
        return petition;
    }
};

module.exports = CONST;