
const CONST = {
    PRODUCT: (petition, results) => {
        return petition;
    },
    ASSIGN_CATEGORY: (petition, results) => {
        petition["json"]["product_id"] = results["PRODUCT"];
        return petition;
    },
    CHARACTERISTIC: (petition, results) => {
        return petition;
    },
    ASSIGN_CHARACTERISTIC: (petition, results) => {
        petition["json"]["product_id"] = results["PRODUCT"];
        petition["json"]["charact_id"] = results["CHARACTERISTIC"];
        return petition;
    },
    FOLDER: (petition, results) => {
        petition["json"]["product_id"] = results["PRODUCT"];
        return petition;
    }
};

module.exports = CONST;