
const mediaService = require("../service/media");
const productService = require("../service/product");
const media = require("../service/media");

const CONST = {
    "ADD_MEDIA": mediaService.addMedia,
    "ROLL_ADD_MEDIA": mediaService.addMediaRollback,
    "ADD_PRODUCT": productService.addProduct,
    "ROLL_ADD_PRODUCT": productService.addProductRollback
}

module.exports = CONST;