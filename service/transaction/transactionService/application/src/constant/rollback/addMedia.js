
const CONST = {
    INFO: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/media/delete`,
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["Sauthentication"],
                "Authentication": headers["Authentication"]
            },
            json: {
                "media_id": body
            }
        }
    },
    CONTENT: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/media/media/delete/content`,
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["Sauthentication"],
                "Authentication": headers["Authentication"]
            },
            json: {
                "filename": body["filename"],
                "folder": body["folder"]
            }
        }
    }
};

module.exports = CONST;