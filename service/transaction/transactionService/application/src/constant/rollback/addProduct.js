
const CONST = {
    PRODUCT: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/product/delete`,
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["Sauthentication"],
                "Authentication": headers["Authentication"]
            },
            json: {
                "product_id": body
            }
        }
    },
    CHARACTERISTIC: (body, headers) => {
        return {
            url: `http://${process.env["ROUTING_SERVICE"]}/hs/product/charact/delete`,
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Sauthentication": headers["Sauthentication"],
                "Authentication": headers["Authentication"]
            },
            json: {
                "charact_id": body
            }
        }
    }
};

module.exports = CONST;