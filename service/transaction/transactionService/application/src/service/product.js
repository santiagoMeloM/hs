
const request = require("request");
const MEDIA = require("../constant/media");
const { Transaction } = require("../model/transaction");

let addProduct = (transaction, pending) => {
    let next = MEDIA.FORWARD.ADD_PRODUCT[pending.getTitle()];
    let bodyStack = MEDIA.BODY_STACK.ADD_PRODUCT[pending.getTitle()](pending.getPetition(), transaction.getResult());
    return new Promise((resolve, reject) => {
        pending.setPetition(bodyStack);
        request(pending.getPetition(), (error, res, body) => {
            if (error == null && body["response"] == 200) {
                transaction.addResult(pending.getTitle(), body["content"]);
                transaction.getTransaction().get(pending.getTitle()).setEnded();
                if (next != null) transaction.getTransaction().get(next).setPending();
            } else {
                transaction.addResult(pending.getTitle(), "ERROR");
                transaction.getTransaction().get(pending.getTitle()).setError();
            }
            resolve(transaction);
        }, (error) => {
            transaction.addResult(pending.getTitle(), "ERROR");
            transaction.getTransaction().get(pending.getTitle()).setError();
            resolve(transaction);
        });
    });
};

let addProductRollback = (transaction) => {
    return new Promise((resolve, reject) => {
        let newTransaction = new Transaction(MEDIA.ROLLBACK_NAME[transaction.getTitle()]);
        if (transaction.getResult().hasOwnProperty("PRODUCT") && transaction.getResult()["PRODUCT"] != "ERROR") {
            let info = transaction.getTransaction().get("PRODUCT");
            let infoBody = transaction.getResult()["PRODUCT"];
            newTransaction.addOperation("0", "PRODUCT", MEDIA.ROLLBACK.ADD_PRODUCT.PRODUCT(infoBody, info.getPetition()["headers"]));
        }
        if (transaction.getResult().hasOwnProperty("CHARACTERISTIC") && transaction.getResult()["CHARACTERISTIC"] != "ERROR") {
            let info = transaction.getTransaction().get("CHARACTERISTIC");
            let infoBody = transaction.getResult()["CHARACTERISTIC"];
            newTransaction.addOperation("0", "CHARACTERISTIC", MEDIA.ROLLBACK.ADD_PRODUCT.CHARACTERISTIC(infoBody, info.getPetition()["headers"]));
        }
        newTransaction.getTransaction().forEach((value, key) => {
            newTransaction.getTransaction().get(key).setPending();
        });
        resolve(newTransaction);
    });
};


module.exports = {
    addProduct,
    addProductRollback
}