
const request = require("request");
const MEDIA = require("../constant/media");
const { Transaction } = require("../model/transaction");

let addMedia = (transaction, pending) => {
    let next = MEDIA.FORWARD.ADD_MEDIA[pending.getTitle()];
    let bodyStack = MEDIA.BODY_STACK.ADD_MEDIA[pending.getTitle()](pending.getPetition(), transaction.getResult());
    return new Promise((resolve, reject) => {
        pending.setPetition(bodyStack);
        request(pending.getPetition(), (error, res, body) => {
            if (error == null && body["response"] == 200) {
                transaction.addResult(pending.getTitle(), body["content"]);
                transaction.getTransaction().get(pending.getTitle()).setEnded();
                if (next != null) transaction.getTransaction().get(next).setPending();
            } else {
                transaction.addResult(pending.getTitle(), "ERROR");
                transaction.getTransaction().get(pending.getTitle()).setError();
            }
            resolve(transaction);
        }, (error) => {
            transaction.addResult(pending.getTitle(), "ERROR");
            transaction.getTransaction().get(pending.getTitle()).setError();
            resolve(transaction);
        });
    });
};

let addMediaRollback = (transaction) => {
    return new Promise((resolve, reject) => {
        let newTransaction = new Transaction(MEDIA.ROLLBACK_NAME[transaction.getTitle()]);
        if (transaction.getResult().hasOwnProperty("INFO") && transaction.getResult()["INFO"] != "ERROR") {
            let info = transaction.getTransaction().get("INFO");
            let infoBody = transaction.getResult()["INFO"];
            newTransaction.addOperation("0", "INFO", MEDIA.ROLLBACK.ADD_MEDIA.INFO(infoBody, info.getPetition()["headers"]));
        } 
        if (transaction.getResult().hasOwnProperty("CONTENT") && transaction.getResult()["CONTENT"] != "ERROR") {
            let content = transaction.getTransaction().get("CONTENT");
            let contentUrl = content.getPetition()["url"].split("/");
            let contentBody = {"folder": contentUrl[6], "filename": contentUrl[7]};
            newTransaction.addOperation("0", "CONTENT", MEDIA.ROLLBACK.ADD_MEDIA.CONTENT(contentBody, content.getPetition()["headers"]));

        }
        newTransaction.getTransaction().forEach((value, key) => {
            newTransaction.getTransaction().get(key).setPending();
        });
        resolve(newTransaction);
    });
};


module.exports = {
    addMedia,
    addMediaRollback
}