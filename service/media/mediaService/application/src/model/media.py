
class Media:
    def __init__(self, id=None, type=None, created=None):
        self.id = id
        self.type = type
        self.created = created
    
    def getId(self):
        return self.id
    
    def getType(self):
        return self.type
    
    def getCreated(self):
        return self.created
    
    def toMap(self):
        return {
            "id": self.id,
            "type": self.type,
            "created": self.created
        }
    
    def toArray(self):
        return [self.id, self.type, self.created]
