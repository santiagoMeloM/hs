
from flask import Blueprint, request, jsonify, Response
from configuration.auth import system, user, admin
import service.media as SM

alias = "media"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
@admin
def add():
    response = {"response": 400, "content": {}}
    result = SM.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/add/content/<string:filename>".format(alias), methods=["POST"])
@system
@admin
def addContent(filename):
    response = {"response": 400, "content": {}}
    result = SM.addContent(filename)
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get".format(alias), methods=["POST"])
@system
def get():
    response = {"response": 400, "content": {}}
    data = SM.get()
    if data:
        try:
            response = {"response": 200, "content": data}
        except: pass
    return jsonify(response)

@controller.route("/{}/get/products".format(alias), methods=["POST"])
@system
def getProductArray():
    response = {"response": 400, "content": {}}
    data = SM.getProductArray()
    if data:
        try:
            response = {"response": 200, "content": data}
        except: pass
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SM.getRange(start, qnt)
    if data:
        try:
            response = {"response": 200, "content": data}
        except: pass
    return jsonify(response)

@controller.route("/{}/get/folder/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def getFolderRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SM.getFolderRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/update".format(alias), methods=["PUT"])
@system
@admin
def update():
    response = {"response": 400, "content": {}}
    result = SM.update()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    response = {"response": 400, "content": {}}
    result = SM.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/delete/content".format(alias), methods=["DELETE"])
@system
@admin
def deleteContent():
    response = {"response": 400, "content": {}}
    result = SM.deleteContent()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)