
from flask import Blueprint, request, jsonify
from configuration.auth import system, user, admin
import service.folder as SF

alias = "folder"

controller = Blueprint(alias, __name__)

@controller.route("/{}/add".format(alias), methods=["POST"])
@system
@admin
def add():
    response = {"response": 400, "content": {}}
    result = SF.add()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/assign".format(alias), methods=["POST"])
@system
@admin
def addMediaFolder():
    response = {"response": 400, "content": {}}
    result = SF.addMediaFolder()
    if result:
        response = {"response": 200, "content": result}
    return jsonify(response)

@controller.route("/{}/get/<int:productId>".format(alias), methods=["GET"])
@system
def getProductFolder(productId):
    response = {"response": 400, "content": {}}
    data = SF.getProductFolder(productId)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/<int:start>/<int:qnt>".format(alias), methods=["GET"])
@system
def getRange(start, qnt):
    response = {"response": 400, "content": {}}
    data = SF.getRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/get/product/<int:start>/<int:qnt>".format(alias), methods=["POST"])
@system
def get(start, qnt):
    response = {"response": 400, "content": {}}
    data = SF.getMediaProductRange(start, qnt)
    if data:
        response = {"response": 200, "content": data}
    return jsonify(response)

@controller.route("/{}/delete".format(alias), methods=["DELETE"])
@system
@admin
def delete():
    response = {"response": 400, "content": {}}
    result = SF.delete()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)

@controller.route("/{}/unassign".format(alias), methods=["DELETE"])
@system
@admin
def deleteMediaFolder():
    response = {"response": 400, "content": {}}
    result = SF.deleteMediaFolder()
    if result:
        response = {"response": 200, "content": True}
    return jsonify(response)