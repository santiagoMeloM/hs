
from configuration.app import db

def add(type):
    result = False
    try:
        tmp = db.procedure("addMedia", [type])
        result = tmp[0]["rows"]
    except: pass
    return result

def getCount(folderId):
    qry = """SELECT COUNT(*) as count FROM media m INNER JOIN media_folder mf ON (m.id = mf.media_id)
            WHERE mf.folder_id='{}'""".format(folderId)
    result = db.fetchAll(qry)
    return result

def get(mediaId):
    qry = "SELECT * FROM media WHERE id='{}'".format(mediaId)
    result = db.fetchOne(qry)
    return result

def getRange(start, qnt):
    qry = "SELECT * FROM media LIMIT {},{}".format(start, qnt)
    result = db.fetchAll(qry)
    return result

def getProductArray(products):
    products = ', '.join([str(product) for product in products])
    qry = """SELECT m.*, f.product_id as product_id  FROM folder f 
                INNER JOIN media_folder mf ON (mf.folder_id = f.id)  
                INNER JOIN media m ON (m.id = mf.media_id) 
             WHERE f.product_id IN ({})""".format(products)
    result = db.fetchAll(qry)
    return result

def getAllFolder(folderId):
    qry = """SELECT m.id, type, created FROM media m INNER JOIN media_folder mf ON (m.id = mf.media_id)
            WHERE mf.folder_id='{}'""".format(folderId)
    result = db.fetchAll(qry)
    return result

def getFolderRange(folderId, start, qnt):
    qry = """SELECT m.id, type, filepath, created FROM media m INNER JOIN media_folder mf ON (m.id = mf.media_id)
            WHERE mf.folder_id='{}' LIMIT {},{}""".format(folderId, start, qnt)
    result = db.fetchAll(qry)
    return result

def update(media):
    result = False
    try:
        tmp = db.procedure("updateMedia", media.toArray())
        result = tmp[0]["rows"] > 0
    except: pass
    return result

def delete(mediaId):
    result = False
    try:
        tmp = db.procedure("deleteMedia", [mediaId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result