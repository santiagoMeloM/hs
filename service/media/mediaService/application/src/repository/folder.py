
from configuration.app import db

def add(productId):
    result = False
    try:
        tmp = db.procedure("addFolder", [productId])
        result = tmp[0]["rows"]
    except: pass
    return result

def addMediaFolder(mediaId, folderId):
    result = False
    try:
        tmp = db.procedure("assignMedia", [mediaId, folderId])
        result = tmp[0]["rows"]
    except: pass
    return result

def getProductFolder(productId):
    qry = "SELECT * FROM folder WHERE product_id = {}".format(productId)
    result = db.fetchAll(qry)
    return result

def getRange(start, qnt):
    qry = "SELECT * FROM folder LIMIT {},{}".format(start, qnt)
    result = db.fetchAll(qry)
    return result

def getMediaProductRange(productId, start, qnt):
    qry = """SELECT m.*  FROM folder f 
                INNER JOIN media_folder mf ON (mf.folder_id = f.id)  
                INNER JOIN media m ON (m.id = mf.media_id) 
             WHERE f.product_id='{}' LIMIT {},{}""".format(productId, start, qnt)
    result = db.fetchAll(qry)
    return result

def deleteMediaFolder(mediaFolderId):
    result = False
    try:
        tmp = db.procedure("unassignMedia", [mediaFolderId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result

def delete(folderId):
    result = False
    try:
        tmp = db.procedure("deleteFolder", [folderId])
        result = tmp[0]["rows"] > 0
    except: pass
    return result
