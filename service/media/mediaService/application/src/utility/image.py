
from io import BytesIO
import PIL
from PIL import Image
from constant.media import IMAGE

def imageResize(decoded, ext):
    ext = IMAGE["ext"][ext]
    with BytesIO(decoded) as buff:
        with Image.open(buff) as tmpImg:
            tmpImg.thumbnail((IMAGE["width"], IMAGE["height"]), PIL.Image.ANTIALIAS)
            with BytesIO() as result:
                tmpImg.save(result, format=ext)
                return result.getvalue()