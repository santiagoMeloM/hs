
from ftplib import FTP
from threading import Thread, Event
from queue import Queue, Empty
from io import BytesIO
import constant.ftp as FTPCONST
import base64

class Ftp:
    def __init__(self):
        self.ftp = FTP('')
        self.ftp.connect(FTPCONST.HOST, int(FTPCONST.PORT))
        self.ftp.login(FTPCONST.USER, FTPCONST.PASSWORD)
        self.timeout = FTPCONST.TIMEOUT

    def getBytes(self, folder, filename):
        self.ftp.cwd(folder)
        self.ftp.retrbinary("RETR {}".format(filename) , self.bytes.put)
        self.bytes.join()
        self.finished.set()

    def sendBytes(self):
        result = []
        while not self.finished.is_set():
            try:
                result.append(self.bytes.get(timeout=self.timeout))
                #yield self.bytes.get(timeout=self.timeout)
                self.bytes.task_done()
            except Empty:
                self.finished.wait(self.timeout)
        self.worker.join()
        return b''.join(result)

    def download(self, folder, filename):
        if filename not in self.ftp.nlst(folder):
            return None
        self.bytes = Queue()
        self.finished = Event()
        self.worker = Thread(target=self.getBytes, args=(folder, filename))
        self.worker.start()
        return self.sendBytes()
    
    def upload(self, folder, filename, file):
        result = None
        try:
            self.ftp.cwd(folder)
            result = self.ftp.storbinary("STOR {}".format(filename), BytesIO(file))
            self.ftp.quit()
        except:
            self.ftp.connect(FTPCONST.HOST, int(FTPCONST.PORT))
            self.ftp.login(FTPCONST.USER, FTPCONST.PASSWORD)
            self.ftp.cwd(folder)
            result = self.ftp.storbinary("STOR {}".format(filename), BytesIO(file))
            self.ftp.quit()
        return result
    
    def uploadBytes(self, folder, filename, file):
        result = None
        try:
            self.ftp.cwd(folder)
            result = self.ftp.storbinary("STOR {}".format(filename), file)
            self.ftp.quit()
        except:
            self.ftp.connect(FTPCONST.HOST, int(FTPCONST.PORT))
            self.ftp.login(FTPCONST.USER, FTPCONST.PASSWORD)
            self.ftp.cwd(folder)
            result = self.ftp.storbinary("STOR {}".format(filename), file)
            self.ftp.quit()
        return result
    
    def delete(self, folder, filename):
        self.ftp.cwd(folder)
        result = self.ftp.delete(filename)
        return result
