
import os

env = os.environ

## debug values
# env["FTP_HOST"] = "0.0.0.0"
# env["FTP_PORT"] = "5000"
# env["FTP_USER"] = "santiago"
# env["FTP_PASSWORD"] = "123"

HOST = env.get("FTP_HOST")
PORT = env.get("FTP_PORT")
USER = env.get("FTP_USER")
PASSWORD = env.get("FTP_PASSWORD")
TIMEOUT = 0.01