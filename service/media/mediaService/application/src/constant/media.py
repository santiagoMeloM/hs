
TYPE = lambda id, product_id, created: {
    "1": {
        "name": "{}.jpg".format(id),
        "media": "image/jpeg",
        "folder_thumb": "data/thumb",
        "folder_real": "data/real",
        "product": product_id,
        "created": created
    },
    "2": {
        "name": "{}.mp4".format(id),
        "media": "video/mp4",
        "folder": "videos",
        "product": product_id,
        "created": created
    }
}

IMAGE = {
    "width": 300,
    "height": 300,
    "ext": {
        "jpg": "JPEG"
    }
}