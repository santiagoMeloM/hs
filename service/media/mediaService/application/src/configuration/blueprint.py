
from configuration.handler import app
import controller.media as MC
import controller.folder as FC

app.register_blueprint(MC.controller)
app.register_blueprint(FC.controller)