
from flask import request, jsonify
import repository.media as RM
from configuration.security import generate
from utility.encryption import Encrypt
from model.media import Media
from constant.media import TYPE
from utility.ftp import Ftp
import base64
import utility.image as UImage

def add():
    data = dict(request.get_json())
    type = data['type']
    return RM.add(type)

def addContent(filename):
    result = False
    data = dict(request.get_json())
    image = base64.b64decode(data["image"])
    try:
        Ftp().upload(data["folder"], filename, image)
        resized = UImage.imageResize(image, filename.split(".")[1])
        result = Ftp().upload("data/thumb", filename, resized)
    except: pass
    return result

def get():
    result = False
    data = dict(request.get_json())
    mediaId = data['media_id']
    try:
        media = RM.get(mediaId)
        tmp = TYPE(media['id'], 0, media['created'])[str(media['type'])]
        result =  {
            "detail": tmp,
            "content": base64.b64encode(Ftp().download(tmp["folder_real"], tmp["name"])).decode('ascii')
        }
        result = result if result["content"] != None else False
    except: pass
    return result

def getProductArray():
    result = False
    data = dict(request.get_json())
    products, unit = data['products'], data["unit"]
    try:
        result = []
        tmp = RM.getProductArray(products)
        for media in tmp:
            type = TYPE(media['id'], media['product_id'], media['created'])
            if str(media['type']) in type:
                tmp = type[str(media['type'])]
                tmpFolder = tmp["folder_real"] if unit else tmp["folder_thumb"]
                content =  {
                    "detail": tmp,
                    "content": base64.b64encode(Ftp().download(tmpFolder, tmp["name"])).decode('ascii')
                }
                if (content["content"] != None): result.append(content)
    except: pass
    return result
    
def getRange(start, qnt):
    result = False
    try:
        tmp = RM.getRange(start, qnt)
        result = []
        for media in tmp:
            type = TYPE(media['id'], 0, media['created'])
            if str(media['type']) in type:
                tmp = type[str(media['type'])]
                content =  {
                    "detail": tmp,
                    "content": base64.b64encode(Ftp().download(tmp["folder_thumb"], tmp["name"])).decode('ascii')
                }
                if (content["content"] != None): result.append(content)
    except: pass
    return result

def getFolderRange(start, qnt):
    data = dict(request.get_json())
    folderId = data['folder_id']
    return RM.getFolderRange(folderId, start, qnt)

def update():
    result = False
    data = dict(request.get_json())
    try:
        result = RM.update(Media(**data))
    except: pass
    return result

def delete():
    data = dict(request.get_json())
    mediaId = data['media_id']
    return RM.delete(mediaId)

def deleteContent():
    result = False
    data = dict(request.get_json())
    filename, folder = data["filename"], data["folder"]
    try:
        result = Ftp().delete(folder, filename)
    except: pass
    return result