
from flask import request
import repository.folder as RF
from configuration.security import generate
from utility.encryption import Encrypt
import repository.media as RM
from utility.ftp import Ftp

def add():
    data = dict(request.get_json())
    productId = data['product_id']
    return RF.add(productId)

def addMediaFolder():
    data = dict(request.get_json())
    mediaId, folderId = data['media_id'], data['folder_id']
    return RF.addMediaFolder(mediaId, folderId)

def getProductFolder(productId):
    result = False
    try:
        result = RF.getProductFolder(productId)
    except: pass
    return result

def getRange(start, qnt):
    result = False
    try:
        result = RF.getRange(start, qnt)
    except: pass
    return result

def getMediaProductRange(start, qnt):
    data = dict(request.get_json())
    productId = data['product_id']
    return RF.getMediaProductRange(productId, start, qnt)

def deleteMediaFolder():
    data = dict(request.get_json())
    mediaFolderId = data['media_folder_id']
    return RF.deleteMediaFolder(mediaFolderId)

def delete():
    result = False
    data = dict(request.get_json())
    folderId = data['folder_id']
    medias = RM.getAllFolder(folderId)
    for media in medias:
        mediaId = media["id"]
        try:
            Ftp().delete("images", "{}.jpg".format(mediaId))
            Ftp().delete("videos", "{}.mp4".format(mediaId))
        except: pass
        RM.delete(mediaId)
    if (RM.getCount(folderId)[0]["count"] == 0):
        result = RF.delete(folderId)
    return result