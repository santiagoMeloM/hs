
DELIMITER $$
DROP PROCEDURE IF EXISTS addMedia;
CREATE PROCEDURE addMedia(IN ti tinyint)
BEGIN
    INSERT INTO media (type, created) VALUES (ti, NOW());
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS addFolder;
CREATE PROCEDURE addFolder(IN pid int)
BEGIN
    INSERT INTO folder (product_id) VALUES (pid);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS assignMedia;
CREATE PROCEDURE assignMedia(IN mid int, IN fid int)
BEGIN
    INSERT INTO media_folder (media_id, folder_id) VALUES (mid, fid);
    SELECT LAST_INSERT_ID() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS getProductFolder;
CREATE PROCEDURE getProductFolder(IN pid int)
BEGIN
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS updateMedia;
CREATE PROCEDURE updateMedia(IN mid int, IN ti tinyint, IN crtd date)
BEGIN
    UPDATE media SET type=ti, created=crtd WHERE id=mid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteMedia;
CREATE PROCEDURE deleteMedia(IN mid int)
BEGIN
    DELETE FROM media WHERE id=mid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS deleteFolder;
CREATE PROCEDURE deleteFolder(IN fid int)
BEGIN
    DELETE FROM folder WHERE id=fid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS unassignMedia;
CREATE PROCEDURE unassignMedia(IN mfid int)
BEGIN
    DELETE FROM media_folder WHERE id=mfid;
    SELECT ROW_COUNT() as rows;
END$$
DELIMITER ;