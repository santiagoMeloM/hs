
-- tables
-- Table: folder
CREATE TABLE folder (
    id int NOT NULL AUTO_INCREMENT,
    product_id int NOT NULL,
    CONSTRAINT folder_pk PRIMARY KEY (id)
);

-- Table: media
CREATE TABLE media (
    id int NOT NULL AUTO_INCREMENT,
    type tinyint NOT NULL,
    created date NOT NULL,
    CONSTRAINT media_pk PRIMARY KEY (id)
);

-- Table: media_folder
CREATE TABLE media_folder (
    id int NOT NULL AUTO_INCREMENT,
    folder_id int NOT NULL,
    media_id int NOT NULL,
    CONSTRAINT media_folder_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: media_folder_folder (table: media_folder)
ALTER TABLE media_folder ADD CONSTRAINT media_folder_folder FOREIGN KEY media_folder_folder (folder_id)
    REFERENCES folder (id) ON DELETE CASCADE;

-- Reference: media_folder_media (table: media_folder)
ALTER TABLE media_folder ADD CONSTRAINT media_folder_media FOREIGN KEY media_folder_media (media_id)
    REFERENCES media (id) ON DELETE CASCADE;