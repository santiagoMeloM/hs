
import { CONSTANTS as PRODUCT } from 'src/app/constant/product.constant';
import { CONSTANTS as MEDIA } from 'src/app/constant/media.constant';
import { CONSTANTS as GLOBAL } from 'src/app/constant/global.constant';
import { CONSTANTS as PRODUCER } from 'src/app/constant/producer.constant';
import { CONSTANTS as FILE } from 'src/app/constant/file.constant';
import { CONSTANTS as SITES } from 'src/app/constant/sites.constant';
import { CONSTANTS as AUTH } from 'src/app/constant/authentication.constant';
import { CONSTANTS as LANGUAGE } from 'src/app/constant/language.constant';
import { CONSTANTS as VALUES } from 'src/app/constant/values.constant';
import { CONSTANTS as NOTICE } from 'src/app/constant/notice.constant';
import { CONSTANTS as CHECKOUT } from 'src/app/constant/checkout.constant';

export const environment = {
  production: false,
  HOST: "https://inventory.highsclothing.shop/hs",
  GLOBAL: GLOBAL,
  RELOAD_QUOTA: 10,
  DISCOVER: {
    HOST: "0.0.0.0",
    PORT: "8000",
    INSTANCE: "eureka/apps"
  },
  ROUTE: {
    NAME: "ROUTE_SERVER",
    TAG: "hs"
  },
  RESPONSE: {
    SUCCESS: 200
  },
  LANGUAGE: LANGUAGE,
  SITES: SITES,
  AUTH: AUTH,
  PRODUCER: PRODUCER,
  PRODUCT: PRODUCT,
  MEDIA: MEDIA,
  FILE: FILE,
  VALUES: VALUES,
  NOTICE: NOTICE,
  CHECKOUT: CHECKOUT
};
