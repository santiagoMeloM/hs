
export const CONSTANTS = {
    CLOSE_TIME: 2000,
    OPTIONS: {
        EMPTY: {
          design: {
              cross: false,
              back: false,
              load: false,
              msg: false,
              loading: "none"
            },
            actionable: {
              ok: false,
              okcancel: false
            },
            content: {
              msg: ""
            }
        },
        OK: (msg: string) => {return {
            design: {
                cross: false,
                back: true,
                load: false,
                msg: true,
                loading: "none"
              },
              actionable: {
                ok: true,
                okcancel: false
              },
              content: {
                msg: msg
              }
        }},
        OKCANCEL: (msg: string) => {return {
          design: {
              cross: false,
              back: true,
              load: false,
              msg: true,
              loading: "none"
            },
            actionable: {
              ok: false,
              okcancel: true
            },
            content: {
              msg: msg
            }
        }},
        LOADING: () => {return {
          design: {
              cross: false,
              back: false,
              load: true,
              msg: false,
              loading: "white"
            },
            actionable: {
              ok: false,
              okcancel: false
            },
            content: {
              msg: ""
            }
        }}
    }
}