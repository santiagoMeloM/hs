
export const CONSTANTS = {
    PRODUCT: {
        GET: {
            ALL: {
                URL: "product/product/get/all"
            },
            RANGE: {
                URL: "product/product/get",
                QNT: 10
            },
            CATEGORY: {
                URL: "product/category/get/product",
                QNT: 10
            },
            DISCOUNT: {
                URL: "product/product/get/discount",
                QNT: 10
            },
            SIZE: {
                URL: "product/product/get/size"
            }
        },
        POST: {
            DATE: {
                ADMIN: {
                    QNT: 10
                },
                URL: "product/product/get/date",
                QNT: 10
            },
            MANY: {
                URL: "product/product/get/product/many"
            }
        },
        DELETE: {
            PRODUCT: {
                URL: "product/product/delete",
            }
        }
    },
    DISCOUNT: {
        POST: {
            RANGE: {
                URL: "product/product/get/discount/product/many"
            },
            ADD: {
                URL: "product/product/add/discount"
            }
        },
        GET: {
            RANGE: {
                URL: "product/product/get/discount/combined"
            },
            SIZE: {
                URL: "product/product/discount/get/size"
            }
        },
        PUT: {
            STATE: {
                URL: "product/product/update/state/discount"
            }
        },
        DELETE: {
            SINGLE: {
                URL: "product/product/delete/discount"
            }
        }
    },
    CATEGORY: {
        GET: {
            NAME: {
                URL: "product/product/get/name",
                QNT: 1
            },
            RANGE: {
                URL: "product/category/get",
                QNT: 100
            },
            ADMIN: {
                QNT: 10
            },
            SIZE: {
                URL: "product/category/get/size"
            }
        },
        POST: {
            ADD: {
                URL: "product/category/add"
            },
            ASSIGN: {
                URL: "product/category/add/product"
            },
            UNASSIGN: {
                URL: "product/category/delete/product"
            },
            PRODUCT: {
                URL: "product/category/get/product/category",
                QNT: 10
            }
        },
        DELETE: {
            URL: "product/category/delete"
        }
    }, 
    CHARACTERISTIC: {
        POST: {
            PRODUCT: {
                URL: "product/charact/get/product"
            },
            DELETE: {
                URL: "product/charact/delete"
            },
            DELETE_PRODUCT: {
                URL: "product/charact/delete/product"
            }
        },
        PUT: {
            UPDATE: {
                URL: "product/charact/update"
            }
        }
    }
}