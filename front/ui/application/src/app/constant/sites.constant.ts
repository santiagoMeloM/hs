
export const CONSTANTS = {
    DATA: {
        ADMINISTRATION: {
            "redirect": "/login",
            "funcs": ["verifyRoles"],
            "params": {
                "roles": ["admin"]
            }
        },
        LOGIN: {
            "redirect": "/",
            "funcs": ["notLogged"],
            "params": {}
        },
        REGISTER: {
            "redirect": "/",
            "funcs": ["notLogged"],
            "params": {}
        },
        CHECKOUT: {
            "redirect": "/login",
            "funcs": ["verifyRoles"],
            "params": {
                "roles": ["user"]
            }
        }
    }
}