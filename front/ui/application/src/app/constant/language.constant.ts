
import { CONSTANTS as SPANISH } from "src/app/constant/languages/spanish.constant";
import { CONSTANTS as ENGLISH } from "src/app/constant/languages/english.constant";

export const CONSTANTS = (language, all) => {
    const languages = {
        "spanish": SPANISH,
        "english": ENGLISH
    };
    return all ? languages : languages[language];
}