
export const CONSTANTS = {
    KEY: "spanish",
    IMAGE: "assets/flags/spanish-flag.png",
    ERROR: "Ocurrio un error inesperado, lo sentimos!",
    NOTFOUND: "Pagina no encontrada",
    APP: {
        POPUP: {
            TITLE: "Informacion",
            BUTTON: {
                OK: "Listo",
                CANCEL: "Cancelar"
            },
            BAG: {
                TITLE: "Bolsa",
                PURCHASE: "Confirmar Compra",
                TOTAL: "Precio total"
            }
        },
        IMAGE: "assets/main/doodle.jpg"
    },
    MAIN: {
        PLATFORM: {
            REGISTER: "Registrarse",
            SIGNIN: "Ingresar",
            SIGNOUT: "Cerrar Sesión"
        },
        SEARCH: "Buscar",
        MENU: {
            HOME: "Inicio",
            RELEASES: "Novedades",
            CATEGORIES: "Categorias",
            DEALS: "Ofertas"
        }, 
        HELPMENU: {
            FEEDBACK: "Opinion",
            TRACKING: "Ordenes",
            ABOUT: "Nosotros",
            HELP: "Ayuda"
        },
        FOOTER: {
            COPYRIGHT: (year) => `© ${year} High Support. Todos los derechos reservados`,
            GUIDES: "Guias",
            TERMOFUSE: "Terminos de Uso",
            TERMOFSALE: "Terminos de Descuento",
            PRIVACYPOLICY: "Politica de Privacidad"
        }
    },
    DETAIL: {
        TITLE: "Detalles"
    },
    PRODUCT: {
        NEW: "NUEVO",
        SEE_MORE: "Ver Mas",
        CART: {
            ADD_TO_CART: "Al Carrito",
            ON_CART: "En Carrito",
            OFF_CART: "Fuera"
        }
    },
    DEALS: {
        TITLE: "Ofertas"
    },
    CATEGORY: {
        TITLE: "Categorias"
    },
    RELEASES: {
        TITLE: "Novedades"
    },
    AUTHENTICATION: {
        TITLE: "Ingreso",
        LOGIN: "Ingresar",
        LOADING: "Cargando",
        INCORRECT_DATA: "Usuario o Contraseña incorrecta",
        INPUT: {
            EMAIL: "Correo Electronico",
            PASSWORD: "Contraseña"
        },
        TERMS: "He leido y acepto los terminos y condiciones de High Support"
    },
    REGISTER: {
        TITLE: "Registro",
        REGISTER: "Registarse",
        CREATED: (name) => {return `Bienvenid@ ${name} a High Support!`},
        NOTCREATED: "No fue posible crear una cuenta",
        INPUT: {
            EMAIL: "Correo Electronico",
            PASSWORD: "Contraseña",
            REPEAT_PASSWORD: "Repetir Contraseña"
        }
    },
    CHECKOUT: {
        VALUES: {
            ORIGINAL: "Precio Original",
            DISCOUNT: "Descontado",
            TOTAL: "Total"
        },
        PAY: "Pagar Ahora"
    },
    ADMIN: {
        TITLE: "Administración",
        MESSAGE: {
            ADD_PRODUCT: {
                CONFIRMATION: (result: any) => {return `Producto ${result ? "prosesando!" : "NO prosesando"}`}
            },
            ADD_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "prosesando!" : "NO prosesando"}`}
            },
            ADD_DISCOUNT: {
                CONFIRMATION: (result: any) => {return `Descuento ${result ? "prosesando!" : "NO prosesando"}`}
            },
            ADD_MEDIA: {
                CONFIRMATION: (result: any) => {return `Media ${result ? "prosesando!" : "NO prosesando"}`}
            },
            DELETE_PRODUCT: {
                QUESTION: "Desea borrar producto?",
                CONFIRMATION: (result: any) => {return `Producto ${result ? "borrado" : "NO borrado"}`}
            },
            DELETE_CATEGORY: {
                QUESTION: "Desea borrar categoria?",
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "borrada" : "NO borrada"}`}
            },
            DELETE_DISCOUNT: {
                QUESTION: "Desea borrar descuento?",
                CONFIRMATION: (result: any) => {return `Descuento ${result ? "borrado" : "NO borrado"}`}
            },
            DELETE_MEDIA: {
                QUESTION: "Desea borrar media?",
                CONFIRMATION: (result: any) => {return `Media ${result ? "borrada" : "NO borrada"}`}
            },
            UPDATE_PRODUCT: {
                CONFIRMATION: (result: any) => {return `Producto ${result ? "actualizado!" : "NO actualizado"}`}
            },
            ASSIGN_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "asignada!" : "NO asignada"}`}
            },
            UNASSIGN_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "desconectada!" : "NO desconectada"}`}
            }
        },
        ACTIONS: {
            PRODUCTS: "Productos",
            CATEGORIES: "Categorias",
            DISCOUNT: "Descuentos",
            ADD_PRODUCT: "Añadir Producto",
            ADD_CATEGORY: "Añadir Categoria",
            ADD_DISCOUNT: "Añadir Descuento"
        },
        DETAIL: {
            TITLE: "Detalles",
            ADD_MEDIA: "Añadir Media",
            ASSIGN: "Asignar",
            UNASSIGN: "Remover",
            CHARACT: {
                UPDATE: "Actualizar",
                SIZE: "Escoger un tamaño",
                COLOR: "Escoger un color",
                NEW_COLOR: "Agregar Color"
            }
        }, 
        MODULE: {
            BACK: "Atras",
            NEXT: "Siguiente",
            ADD_CATEGORIES: {
                TITLE: "Agregar Categorias",
                INPUT: {
                    NAME: "Nombre", 
                    DESCRIPTION: "Descripcion"
                },
                ADD_CATEGORY: "Agregar Categoria"
            },
            CATEGORIES: {
                TITLE: "Categorias"
            },
            ADD_DISCOUNTS: {
                TITLE: "Agregar Descuentos",
                INPUT: {
                    DEADLINE: "Fecha de Expiración",
                    PORCENTAGE: "Porcentage",
                    PRODUCTS: "Seleccione un producto"
                },
                ADD_DISCOUNT: "Agregar Descuento"
            },
            DISCOUNTS: {
                TITLE: "Descuentos"
            },
            ADD_PRODUCTS: {
                TITLE: "Agregar Productos",
                INPUT: {
                    NAME: "Nombre",
                    PRICE: "Precio",
                    DESCRIPTION: "Descripción",
                    CATEGORY: "Seleccione una categoria"
                },
                ADD_PRODUCT: "Agregar Producto"
            },
            PRODUCTS: {
                TITLE: "Productos"
            }

        }
    }
}