
export const CONSTANTS = {
    KEY: "english",
    IMAGE: "assets/flags/english-flag.png",
    ERROR: "An unexpected error occured, we are sorry!",
    NOTFOUND: "Page not found",
    APP: {
        POPUP: {
            TITLE: "Information",
            BUTTON: {
                OK: "Ok",
                CANCEL: "Cancel"
            },
            BAG: {
                TITLE: "Bag",
                PURCHASE: "Confirm Purchase",
                TOTAL: "Total price"
            }
        },
        IMAGE: "assets/main/doodle.jpg"
    },
    MAIN: {
        PLATFORM: {
            REGISTER: "Register",
            SIGNIN: "Log In",
            SIGNOUT: "Log Out"
        },
        SEARCH: "Search",
        MENU: {
            HOME: "Home",
            RELEASES: "Releases",
            CATEGORIES: "Categories",
            DEALS: "Deals"
        }, 
        HELPMENU: {
            FEEDBACK: "Feedback",
            TRACKING: "Orders",
            ABOUT: "About",
            HELP: "Help"
        },
        FOOTER: {
            COPYRIGHT: (year) => `© ${year} High Support. All rights reserved.`,
            GUIDES: "Guides",
            TERMOFUSE: "Terms of Use",
            TERMOFSALE: "Terms of Sales",
            PRIVACYPOLICY: "Privacy Policy"
        }
    },
    DETAIL: {
        TITLE: "Details"
    },
    PRODUCT: {
        NEW: "NEW",
        SEE_MORE: "See More",
        CART: {
            ADD_TO_CART: "Add to Cart",
            ON_CART: "On Cart",
            OFF_CART: "Out of Cart"
        }
    },
    DEALS: {
        TITLE: "Deals"
    },
    CATEGORY: {
        TITLE: "Categories"
    },
    RELEASES: {
        TITLE: "Releases"
    },
    AUTHENTICATION: {
        TITLE: "Signin",
        LOGIN: "Log In",
        LOADING: "Loading",
        INCORRECT_DATA: "Incorrect username or password",
        INPUT: {
            EMAIL: "Email",
            PASSWORD: "Password"
        },
        TERMS: "I have read and accept the High Support terms and conditions "
    },
    REGISTER: {
        TITLE: "Registration",
        REGISTER: "Register",
        CREATED: (name) => {return `Welcome ${name} to High Support!`},
        NOTCREATED: "The account could not be created",
        INPUT: {
            EMAIL: "Email",
            PASSWORD: "Password",
            REPEAT_PASSWORD: "Repeat Password"
        }
    },
    CHECKOUT: {
        VALUES: {
            ORIGINAL: "Original Price",
            DISCOUNT: "Discounted",
            TOTAL: "Total"
        },
        PAY: "Pay Now"
    },
    ADMIN: {
        TITLE: "Administration",
        MESSAGE: {
            ADD_PRODUCT: {
                CONFIRMATION: (result: any) => {return `Product ${result ? "processing!" : "NOT processing"}`}
            },
            ADD_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Category ${result ? "processing!" : "NOT processing"}`}
            },
            ADD_DISCOUNT: {
                CONFIRMATION: (result: any) => {return `Discount ${result ? "processing!" : "NOT processing"}`}
            },
            ADD_MEDIA: {
                CONFIRMATION: (result: any) => {return `Media ${result ? "processing!" : "NO processing"}`}
            },
            DELETE_PRODUCT: {
                QUESTION: "Do you want to delete product?",
                CONFIRMATION: (result: any) => {return `Product ${result ? "deleted!" : "NOT deleted"}`}
            },
            DELETE_CATEGORY: {
                QUESTION: "Do you want to delete category?",
                CONFIRMATION: (result: any) => {return `Category ${result ? "deleted!" : "NOT deleted"}`}
            },
            DELETE_DISCOUNT: {
                QUESTION: "Do you want to delete discount?",
                CONFIRMATION: (result: any) => {return `Discount ${result ? "deleted!" : "NOT deleted"}`}
            },
            DELETE_MEDIA: {
                QUESTION: "Desea borrar media?",
                CONFIRMATION: (result: any) => {return `Media ${result ? "deleted" : "NOT deleted"}`}
            },
            UPDATE_PRODUCT: {
                CONFIRMATION: (result: any) => {return `Producto ${result ? "updated!" : "NOT updated"}`}
            },
            ASSIGN_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "assigned!" : "NOT assigned"}`}
            },
            UNASSIGN_CATEGORY: {
                CONFIRMATION: (result: any) => {return `Categoria ${result ? "unassigned!" : "NOT unassigned"}`}
            }
        },
        ACTIONS: {
            PRODUCTS: "Products",
            CATEGORIES: "Categories",
            DISCOUNT: "Discounts",
            ADD_PRODUCT: "Add Product",
            ADD_CATEGORY: "Add Category",
            ADD_DISCOUNT: "Add Discount"
        },
        DETAIL: {
            TITLE: "Details",
            ADD_MEDIA: "Add Media",
            ASSIGN: "Assign",
            UNASSIGN: "Remove",
            CHARACT: {
                UPDATE: "Update",
                SIZE: "Choose a size",
                COLOR: "Choose a color",
                NEW_COLOR: "Agregar Color"
            }
        }, 
        MODULE: {
            BACK: "Back",
            NEXT: "Next",
            ADD_CATEGORIES: {
                TITLE: "Add Categories",
                INPUT: {
                    NAME: "Name", 
                    DESCRIPTION: "Description"
                },
                ADD_CATEGORY: "Add Category"
            },
            CATEGORIES: {
                TITLE: "Categories"
            },
            ADD_DISCOUNTS: {
                TITLE: "Add Discounts",
                INPUT: {
                    DEADLINE: "Expired by",
                    PORCENTAGE: "Porcentage",
                    PRODUCTS: "Select a product"
                },
                ADD_DISCOUNT: "Add Discount"
            },
            DISCOUNTS: {
                TITLE: "Discounts"
            },
            ADD_PRODUCTS: {
                TITLE: "Add Products",
                INPUT: {
                    NAME: "Name",
                    PRICE: "Price",
                    DESCRIPTION: "Description",
                    CATEGORY: "Select a category"
                },
                ADD_PRODUCT: "Add Product"
            },
            PRODUCTS: {
                TITLE: "Products"
            }

        }
    }
}