
export const CONSTANTS = {
    POST: {
        LOGIN: {
            URL: "user/authenticate"
        },
        SESSION: {
            URL: "user/session"
        },
        DESESSION: {
            URL: "user/desession"
        },
        CREATE: {
            URL: "user/user/add"
        }
    }
}