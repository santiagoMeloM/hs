import { HttpHeaders } from '@angular/common/http';
import { CONSTANTS as COUNTRY_CURRENCY } from "src/app/constant/coin_code.constant";

export const CONSTANTS = {
    NEW: () => {
        let weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 7);
        return weekAgo;
    },
    SIZES: [
        {
            "key": "xs",
            "name": "XS",
            "avaliable": false
        },
        {
            "key": "s",
            "name": "S",
            "avaliable": false
        },
        {
            "key": "m",
            "name": "M",
            "avaliable": false
        },
        {
            "key": "l",
            "name": "L",
            "avaliable": false
        },
        {
            "key": "xl",
            "name": "XL",
            "avaliable": false
        }
    ],
    COUNTRY: {
        CURRENCY: COUNTRY_CURRENCY
    },
    CURRENCY: {
        IP: {
            URL: "https://api.ipify.org?format=json",
            HEADERS: new HttpHeaders({})
        },
        CONVERT: {
            URL: "https://currency-exchange.p.rapidapi.com/exchange",
            PARAMETERS: (from, zone) => { return {
                "from": `${from}`,
                "to": `${zone}`
            }},
            HEADERS: new HttpHeaders({
                "x-rapidapi-host": `currency-exchange.p.rapidapi.com`,
                "x-rapidapi-key": `507f98831amsh30301c6cb6e82eap1d610fjsn58a865cba546`
            })
        },
        ZONE: {
            URL: "https://ip-country.p.rapidapi.com/v1/ip-country",
            PARAMETERS: (ip) => { return {
                "ipaddr": ip
            }},
            HEADERS: new HttpHeaders({
                "x-rapidapi-host": `ip-country.p.rapidapi.com`,
                "x-rapidapi-key": `507f98831amsh30301c6cb6e82eap1d610fjsn58a865cba546`
            })
        }
    }
}