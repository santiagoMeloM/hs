
export const CONSTANTS = {
    CONFIGURATION: {
        key: "c64277309ed9f8a816e6a4944b670442",
        test: true
    },
    DATA: (amount: number, name: string, description: string, country: string, responseUrl: string) => {return {
        name: name,
        amount: amount,
        description: name,
        country: country,
        response: responseUrl,
        external: "true"
    }}
}