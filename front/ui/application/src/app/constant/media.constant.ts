
export const CONSTANTS = {
    MEDIA: {
        POST: {
            PRODUCTS: {
                URL: "media/media/get/products"
            },
            MEDIA: {
                URL: "producer/addMedia"
            }
        },
        GET: {
            FOLDER: {
                URL: "media/folder/get"
            }
        },
        DELETE: {
            URL: "media/media/delete"
        }
    },
    FOLDER: {
        DELETE: {
            URL: "media/folder/delete"
        }
    }
}