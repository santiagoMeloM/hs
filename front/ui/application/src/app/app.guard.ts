import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AuthorizationService } from 'src/app/service/configuration/authorization/authorization.service';
import { NoticeService } from './service/configuration/notice/notice.service';
import { RoutingService } from './service/configuration/routing/routing.service';
import { FunctionsService } from './service/configuration/functions/functions.service';

@Injectable()
export class AppGuard implements CanActivate {
    
    constructor(public router: Router,
                private routingService: RoutingService, 
                private functionsService: FunctionsService) {}
    
    public canActivate(route: ActivatedRouteSnapshot): Promise<boolean> | boolean {
        return new Promise<boolean>((resolve, reject) => {
            let siteFuncs: string[] = route.data["funcs"];
            let siteParams: Object = route.data["params"];
            let siteRedirect: string = route.data["redirect"];
            siteFuncs.forEach(name => {
                this.functionsService.addFunc(name);
            });
            this.functionsService.setParams(siteParams);
            this.functionsService.checkFuncs().then(result => {
                if (!result) {
                    this.routingService.routeTo(siteRedirect, {});
                }
                resolve(result);
            });
        });
    }

}