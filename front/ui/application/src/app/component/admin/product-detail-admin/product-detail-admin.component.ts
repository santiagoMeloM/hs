import { Component, OnInit } from '@angular/core';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { ProductModel } from 'src/app/model/product/produc.model';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { MediaService } from 'src/app/service/media/media.service';
import { environment } from 'src/environments/environment';
import { ProductService } from 'src/app/service/product/product.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoryModel } from 'src/app/model/product/category.model';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { CharacteristicModel } from 'src/app/model/product/characteristic';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';

@Component({
  selector: 'app-product-detail-admin',
  templateUrl: './product-detail-admin.component.html',
  styleUrls: ['./product-detail-admin.component.scss']
})
export class ProductDetailAdminComponent implements OnInit {

  private product: ProductModel;
  private folderId: number;
  private imageUpload: any;
  private productMedia: Object[] = [];

  private scrolled: number = 0;

  private categories: CategoryModel[] = [];
  private productCategories: CategoryModel[] = [];
  private charact: CharacteristicModel;
  private formGroup: FormGroup = new FormGroup({
    category: new FormControl('', [Validators.required])
  });
  private formUpdateGroup: FormGroup = new FormGroup({
    size: new FormControl('', [Validators.required]),
    sizeQnt: new FormControl('', [Validators.required, Validators.min(0)]),
    color: new FormControl('', [Validators.required]),
    colorQnt: new FormControl('', [Validators.required, Validators.min(0)]),
    colorNew: new FormControl('', [Validators.required, Validators.min(0)])
  });


  constructor(private routingService: RoutingService, 
              public router: Router, 
              private title: Title, 
              private mediaService: MediaService, 
              private productService: ProductService, 
              private languageService: LanguageService, 
              private noticeService: NoticeService) {
                this.router.events.subscribe((event: Event) => {
                  if (event instanceof NavigationEnd) {
                    this.folderId = null;
                    this.setProduct();
                  }
                });
              }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  public goBack(): void {
    this.routingService.routeTo("/administration/store/products", null);
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["DETAIL"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
    this.productService.getCategories(0).then(result => {
      this.categories = result;
    });
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public getFormUpdateGroup(): FormGroup {
    return this.formUpdateGroup;
  }

  public getCategories(): CategoryModel[] {
    return this.categories;
  }

  private setProduct(): void {
    this.routingService.getParameters().then(result => {
      this.product = new ProductModel().deserealize(result);
      this.productService.getCharacteristicProducts(this.product.getId()).then(charact => {
        this.charact = charact;
      });
      this.mediaService.getProductFolder(this.product.getId()).then(data => {
        this.folderId = data;
      });
      this.mediaService.getMediaProducts([this.product.getId()]).then(result => {
        result.forEach(media => {
          this.productMedia.push({ detail: media.getDetail(), image: media.getBase64()});
        });
      });
      this.productService.getCategoriesByProduct(0, this.product.getId()).then(result => {
        this.productCategories = result;
      })
      this.imageUpload = null;
      this.title.setTitle(`${this.product.getId()}.${this.product.getName()} - Administration`);
    });
  }

  public getProduct(): ProductModel {
    return this.product;
  }

  public getFolderId(): number {
    return this.folderId;
  }

  public getImageUploaded(): Object {
    return this.imageUpload;
  }

  public getMedias(): Object[] {
    return this.productMedia;
  }

  public getProductCategories(): CategoryModel[] {
    return this.productCategories;
  }

  public inCategory(category: any): boolean {
    let tmp = this.productCategories.filter(element => {
      category = category == "" ? -1 : parseInt(category);
      return element.getId() == category;
    });
    return tmp.length > 0;
  }

  public getCharactUpdated(): boolean {
    let result = false;
    if (typeof this.charact !== "undefined")
      result = this.charact.getUpdated();
    return result;
  }

  public saveImage(image: any): void {
    let file: File = image.files[0];
    let reader: FileReader = new FileReader();
    reader.onloadend = (e) => {
      this.imageUpload = { "image": reader.result, "type": file.type };
      this.imageUpload["image"] = this.imageUpload["image"].split(',')[1];
    }
    reader.readAsDataURL(file);
  }

  public resetQnt(): void {
    this.formUpdateGroup.get("colorQnt").reset();
  }

  public getSizeContent(): Object[] {
    let result: Object[] = [];
    let names: Object = {};
    environment.GLOBAL.SIZES.forEach(obj => {
      names[obj["key"]] = obj["name"];
    })
    Object.keys(this.charact.getContent()["sizes"]).forEach(key => {
      result.push({ "name": names[key], "key": key});
    });
    return result;
  }

  public getColorContent(): string[] {
    let result: string[] = [];
    let data: any = this.formUpdateGroup.value;
    if (data["size"] != "") {
      result = Object.keys(this.charact.getContent()["sizes"][data["size"]]);
    }
    return result;
  }

  public getColorQntPlaceholder(): number {
    let result: number = 0;
    let data: any = this.formUpdateGroup.value;
    if (data["size"] != "" && data["color"] != ""){
      result = this.charact.getContent()["sizes"][data["size"]][data["color"]];
    }
    return result;
  }

  public updatePropertyColorCharact(): void {
    let data: Object = this.formUpdateGroup.value;
    if (data["size"] != "" && data["color"] != "") {
      let content: Object = this.charact.getContent();
      content["sizes"][data["size"]][data["color"]] = data["colorQnt"];
      this.charact.setContent(content);
    }
  }


  public addColorCharact(): void {
    let data: Object = this.formUpdateGroup.value;
    this.formUpdateGroup.get("colorNew").reset();
    if (data["colorNew"] != "" && data["size"] != "") {
      let content: Object = this.charact.getContent();
      content["sizes"][data["size"]][`${data["colorNew"]}`] = 0;
      this.charact.setContent(content);
    }
  }

  public assignCategory(): void {
    let data: Object = this.formGroup.value;
    this.noticeService.startLoading().then(info => {
      this.productService.assingCategory(data["category"], this.product.getId()).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["ASSIGN_CATEGORY"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

  public unassingCategory(): void {
    let data: Object = this.formGroup.value;
    this.noticeService.startLoading().then(info => {
      this.productService.unassingCategory(data["category"], this.product.getId()).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["UNASSIGN_CATEGORY"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

  public submitMedia(): void {
    if (this.imageUpload != null) {
      let type: number = environment.FILE[this.imageUpload["type"]];
      this.noticeService.startLoading().then(info => {
        this.mediaService.addProductMedia(type, this.folderId, this.product.getId(), this.imageUpload).then(result => {
          this.noticeService.closeNotice();
          this.imageUpload = null;
          let tmp = typeof result !== "undefined";
          if (tmp) {
            this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["ADD_MEDIA"]["CONFIRMATION"](tmp)).then(info => {
              this.noticeService.closeNotice();
              if (result){
                window.location.reload();
              }
            });
          }
        }, (error) => {
          this.noticeService.ok(this.language()["ERROR"]).then(result => {
            this.noticeService.closeNotice();
          });
        });
      });
    }
  }

  public deleteMedia(id: number): void {
    if (this.productMedia.length >= 2) {
      this.noticeService.startLoading().then(info => {
        this.mediaService.deleteMedia(id).then(result => {
          this.noticeService.closeNotice();
          if (result) {
            this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["DELETE_MEDIA"]["CONFIRMATION"](result)).then(info => {
              this.noticeService.closeNotice();
              if (result){
                window.location.reload();
              }
            });
          }
        }, (error) => {
          this.noticeService.ok(this.language()["ERROR"]).then(result => {
            this.noticeService.closeNotice();
          });
        });
      });
    }
  }

  public updateCharacteristics(): void {
    let data = this.formUpdateGroup.value;
    let content: Object = this.charact.getContent();
    this.noticeService.startLoading().then(info => {
      this.productService.updateCharacteristic(this.charact).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["UPDATE_PRODUCT"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

  public scroll(target: HTMLElement): void {
    this.scrolled = (this.scrolled+1)%this.productMedia.length;
    target.scrollTop = 200*(this.scrolled);
  }
  
}
