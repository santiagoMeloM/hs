import { Component, OnInit } from '@angular/core';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

@Component({
  selector: 'app-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.scss']
})
export class AdminMainComponent implements OnInit {

  constructor(public routingService: RoutingService, 
              private languageService: LanguageService) { }

  ngOnInit(): void {
  }

  public language(): Object {
    return this.languageService.getMessages();
  }

}
