import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product/product.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';

@Component({
  selector: 'app-category-admin',
  templateUrl: './category-admin.component.html',
  styleUrls: ['./category-admin.component.scss']
})
export class CategoryAdminComponent implements OnInit {

  private formGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required])
  });

  constructor(private productService: ProductService, 
              private title: Title, 
              private languageService: LanguageService, 
              private noticeService: NoticeService) {
  }
  
  public language(): Object {
    return this.languageService.getMessages();
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["MODULE"]["ADD_CATEGORIES"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public onSubmit(): void {
    let data = this.formGroup.value;
    this.noticeService.startLoading().then(info => {
      this.formGroup.reset();
      this.productService.addCategory(data["name"], data["description"]).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["ADD_CATEGORY"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

}
