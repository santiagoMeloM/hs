import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountAdminComponent } from './discount-admin.component';

describe('DiscountAdminComponent', () => {
  let component: DiscountAdminComponent;
  let fixture: ComponentFixture<DiscountAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
