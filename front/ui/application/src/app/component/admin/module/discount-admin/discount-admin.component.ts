import { Component, OnInit } from '@angular/core';
import { CategoryModel } from 'src/app/model/product/category.model';
import { ProductService } from 'src/app/service/product/product.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductModel } from 'src/app/model/product/produc.model';
import { Title } from '@angular/platform-browser';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-discount-admin',
  templateUrl: './discount-admin.component.html',
  styleUrls: ['./discount-admin.component.scss']
})
export class DiscountAdminComponent implements OnInit {

  private products: ProductModel[];

  private formGroup: FormGroup = new FormGroup({
    product: new FormControl('', [Validators.required]),
    deadline: new FormControl('', [Validators.required]),
    porcentage: new FormControl('', [Validators.required])
  });

  constructor(private productService: ProductService, 
              private title: Title, 
              private languageService: LanguageService, 
              private noticeService: NoticeService) {
  }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["MODULE"]["ADD_DISCOUNTS"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
    this.setProducts();
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public getProducts(): ProductModel[] {
    return this.products;
  }

  public setProducts(): void {
    this.productService.getProductsAll().then(result => {
      this.products = result;
    });
  }

  public onSubmit(): void {
    let data = this.formGroup.value;
    let product_id: number = parseInt(data["product"]);
    let state: number = 1;
    this.noticeService.startLoading().then(info => {
      this.formGroup.reset();
      this.productService.addDiscount(product_id, state, data["deadline"], data["porcentage"]).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["ADD_DISCOUNT"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
    
  }

}
