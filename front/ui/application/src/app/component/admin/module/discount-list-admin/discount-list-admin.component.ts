import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product/product.service';
import { Router, Event, NavigationEnd } from '@angular/router';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { environment } from 'src/environments/environment';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { MediaService } from 'src/app/service/media/media.service';
import { Title } from '@angular/platform-browser';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

@Component({
  selector: 'app-discount-list-admin',
  templateUrl: './discount-list-admin.component.html',
  styleUrls: ['./discount-list-admin.component.scss']
})
export class DiscountListAdminComponent implements OnInit {

  private discounts: Object[] = [];
  private page: number;
  private pagesSize: number;

  constructor(private productService: ProductService, 
              private mediaService: MediaService, 
              public router: Router, 
              private routingService: RoutingService, 
              private noticeService: NoticeService, 
              private title: Title, 
              private languageService: LanguageService) {
      this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          this.setDiscounts();
        }
    });
  }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  private setDiscounts(): void {
    this.routingService.getParameters().then(data => {
      let num: any = data != null ? data["pg"] : 0;
      this.page = num/environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
      this.discounts = [];
      this.productService.getDiscounts(num).then(result => {
        this.discounts = result;
      });
      this.productService.getDiscountSize().then(result => {
        this.pagesSize = Math.floor(parseInt(result["count"])/environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT);
        console.log(this.pagesSize);
      });
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["MODULE"]["DISCOUNTS"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
  }

  public getDiscounts(): Object[] {
    return this.discounts;
  }

  public updateState(id: number): void {
    this.productService.updateDiscountState(id).then(result => {
      window.location.reload();
    });
  }

  public deleteDiscount(id: number): void {
    console.log(this.language()["ADMIN"]["MESSAGE"]);
    this.noticeService.okCancel(this.language()["ADMIN"]["MESSAGE"]["DELETE_DISCOUNT"]["QUESTION"]).then(result => {
      this.noticeService.closeNotice();
      if (result) {
        this.noticeService.startLoading().then(info => {
          this.productService.deleteDiscount(id).then(result => {
            this.noticeService.closeNotice();
            this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["DELETE_DISCOUNT"]["CONFIRMATION"](result)).then(result => {
              if (result) {
                window.location.reload();
              }
            });
          }, (error) => {
            this.noticeService.ok(this.language()["ERROR"]).then(result => {
              this.noticeService.closeNotice();
            });
          });
        });
      }
    });
  }

  public nextPage(): void {
    this.page += 1;
    let next: number = this.page * environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
    this.routingService.routeTo("/administration/store/discounts", { "pg": next });
  }

  public backPage(): void {
    this.page = this.page-1 > 0 ? this.page-1 : 0;
    let next: number = this.page * environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
    this.routingService.routeTo("/administration/store/discounts", { "pg": next });
  }

  public backValid(): boolean {
    return this.page > 0;
  }

  public nextValid(): boolean {
    return this.page < this.pagesSize;
  }

}
