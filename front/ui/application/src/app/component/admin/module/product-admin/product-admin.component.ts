import { Component, OnInit } from '@angular/core';
import { CategoryModel } from 'src/app/model/product/category.model';
import { ProductService } from 'src/app/service/product/product.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';

@Component({
  selector: 'app-product-admin',
  templateUrl: './product-admin.component.html',
  styleUrls: ['./product-admin.component.scss']
})
export class ProductAdminComponent implements OnInit {

  private categories: CategoryModel[] = [];
  private formGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required, Validators.min(0)]),
    description: new FormControl('', [Validators.required]),
    category: new FormControl('', [Validators.required])
  });

  constructor(private productService: ProductService, 
              private title: Title, 
              private languageService: LanguageService, 
              private noticeService: NoticeService) {
  }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["MODULE"]["ADD_PRODUCTS"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
    this.productService.getCategories(0).then(result => {
      this.categories = result;
    });
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public getCategories(): CategoryModel[] {
    return this.categories;
  }

  public onSubmit(): void {
    let data = this.formGroup.value;
    this.noticeService.startLoading().then(info => {
      this.formGroup.reset();
      this.productService.addProduct(data["name"], data["description"], parseInt(data["price"]), parseInt(data["category"])).then(result => {
        this.noticeService.closeNotice();
        if (result) {
          this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["ADD_PRODUCT"]["CONFIRMATION"](result)).then(info => {
            this.noticeService.closeNotice();
            if (result){
              window.location.reload();
            }
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(result => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

}
