import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product/product.service';
import { ProductModel } from 'src/app/model/product/produc.model';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { environment } from 'src/environments/environment';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { MediaService } from 'src/app/service/media/media.service';
import { Title } from '@angular/platform-browser';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

@Component({
  selector: 'app-product-list-admin',
  templateUrl: './product-list-admin.component.html',
  styleUrls: ['./product-list-admin.component.scss']
})
export class ProductListAdminComponent implements OnInit {

  private products: ProductModel[] = [];
  private page: number;
  private pagesSize: number;

  constructor(private productService: ProductService, 
              private mediaService: MediaService, 
              public router: Router, 
              private routingService: RoutingService, 
              private noticeService: NoticeService, 
              private title: Title, 
              private languageService: LanguageService) {
      this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          this.setProducts();
        }
    });
  }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  private setProducts(): void {
    this.routingService.getParameters().then(data => {
      let num: any = data != null ? data["pg"] : 0;
      this.page = num/environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
      this.products = [];
      this.productService.getProductsDateAdmin(new Date(), num).then(result => {
        this.products = result;
      });
      this.productService.getProductSize().then(result => {
        this.pagesSize = Math.floor(parseInt(result["count"])/environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT);
      });
    });
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["ADMIN"]["MODULE"]["PRODUCTS"]["TITLE"]} - ${this.language()["ADMIN"]["TITLE"]}`);
  }

  public getProducts(): ProductModel[] {
    return this.products;
  }

  public detailProduct(product: ProductModel): void {
    this.routingService.routeTo("/administration/product", product.getMap());
  }

  public deleteProduct(id: number): void {
    this.noticeService.okCancel(this.language()["ADMIN"]["MESSAGE"]["DELETE_PRODUCT"]["QUESTION"]).then(result => {
      this.noticeService.closeNotice();
      if (result) {
        this.noticeService.startLoading().then(info => {
          let charactId: number;
          this.productService.getCharacteristicProducts(id).then(charact => {
            charactId = charact.getId();
          });
          this.productService.deleteProduct(id).then(result => {
            this.productService.deleteCharacteristic(charactId).then(result => {});
            this.mediaService.getProductFolder(id).then(folderId => {
              this.mediaService.deleteFolder(folderId).then(deleted => {});
            });
            this.noticeService.closeNotice();
            this.noticeService.ok(this.language()["ADMIN"]["MESSAGE"]["DELETE_PRODUCT"]["CONFIRMATION"](result)).then(result => {
              if (result) {
                window.location.reload();
              }
            });
          }, (error) => {
            this.noticeService.ok(this.language()["ERROR"]).then(result => {
              this.noticeService.closeNotice();
            });
          });
        });
      }
    });
  }

  public nextPage(): void {
    this.page += 1;
    let next: number = this.page * environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
    this.routingService.routeTo("/administration/store/products", { "pg": next });
  }

  public backPage(): void {
    this.page = this.page-1 > 0 ? this.page-1 : 0;
    let next: number = this.page * environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT;
    this.routingService.routeTo("/administration/store/products", { "pg": next });
  }

  public backValid(): boolean {
    return this.page > 0;
  }

  public nextValid(): boolean {
    return this.page < this.pagesSize;
  }

}
