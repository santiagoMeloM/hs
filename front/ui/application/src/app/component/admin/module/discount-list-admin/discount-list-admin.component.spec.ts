import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountListAdminComponent } from './discount-list-admin.component';

describe('DiscountListAdminComponent', () => {
  let component: DiscountListAdminComponent;
  let fixture: ComponentFixture<DiscountListAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountListAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountListAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
