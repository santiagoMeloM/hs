import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { SecurityService } from 'src/app/service/configuration/security/security.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  private formGroup: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private authenticationService: AuthenticationService, 
              private noticeService: NoticeService, 
              private securityService: SecurityService, 
              private routingService: RoutingService, 
              private languageService: LanguageService, 
              private title: Title, 
              private router: Router) {
                this.router.events.subscribe((event: Event) => {
                  if (event instanceof NavigationEnd) {
                    this.formGroup.reset();
                  }
                });
              }

  public language(): Object {
    return this.languageService.getMessages();
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["AUTHENTICATION"]["TITLE"]} - High Support`);
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public login(): void {
    let data: Object = this.formGroup.value;
    this.formGroup.get("password").reset();
    this.noticeService.startLoading().then(info => {
      this.authenticationService.authentication(data["username"], data["password"]).then(result => {
        if (result) {
          this.securityService.setAuthToken(result["token"]);
          this.routingService.goBack();
          this.noticeService.closeNotice();
        } else {
          this.noticeService.ok(this.language()["AUTHENTICATION"]["INCORRECT_DATA"]).then(res => {
            this.noticeService.closeNotice();
          });
        }
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(res => {
          this.noticeService.closeNotice();
        });
      });
    });
  }

}
