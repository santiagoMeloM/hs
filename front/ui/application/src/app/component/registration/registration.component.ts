import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { SecurityService } from 'src/app/service/configuration/security/security.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  private formGroup: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    repeatPassword: new FormControl('', [Validators.required]),
    agree: new FormControl('', [Validators.requiredTrue])
  }, {validators: this.checkPasswords });

  public checkPasswords(group: FormGroup) {
    let password = group.get('password').value;
    let repeatPassword = group.get('repeatPassword').value;
    return password === repeatPassword ? null : { notSame: true };
  }

  constructor(private authenticationService: AuthenticationService, 
              private noticeService: NoticeService, 
              private securityService: SecurityService, 
              private routingService: RoutingService, 
              private languageService: LanguageService, 
              private title: Title, 
              private router: Router) {
                this.router.events.subscribe((event: Event) => {
                  if (event instanceof NavigationEnd) {
                    this.formGroup.reset();
                  }
                });
              }

  public language(): Object {
    return this.languageService.getMessages();
  }

  ngOnInit(): void {
    this.title.setTitle(`${this.language()["REGISTER"]["TITLE"]} - High Support`);
  }

  public getFormGroup(): FormGroup {
    return this.formGroup;
  }

  public create(): void {
    let data: Object = this.formGroup.value;
    this.noticeService.startLoading().then(info => {
      this.formGroup.reset();
      this.authenticationService.createUser(data["username"], data["password"]).then(result => {
        let msg: string = result ? this.language()["REGISTER"]["CREATED"](data["username"]) : this.language()["REGISTER"]["NOTCREATED"];
        this.noticeService.ok(msg).then(res => {
          this.noticeService.closeNotice();
          this.routingService.routeTo("/login", {});
        });
      }, (error) => {
        this.noticeService.ok(this.language()["ERROR"]).then(res => {
          this.noticeService.closeNotice();
        });
      });
    });
    
  }

}
