
export const SECTIONS: any = (body: Object) => { 
    return [
        {
            name: "HOME",
            path: ""
        },
        {
            name: "RELEASES",
            path: "releases"
        },
        {
            name: "CATEGORIES",
            sub: body["Categories"]
        },
        {
            name: "DEALS",
            path: "deals"
        }
    ];
};

export const SUBSECTIONS: Object[] = [
    {
        name: "TRACKING",
        path: "info/track"
    },
    {
        name: "FEEDBACK",
        path: "info/feedback"
    },
    {
        name: "ABOUT",
        path: "info/about"
    },
    {
        name: "HELP",
        path: "info/help"
    },
];