import { Component, OnInit } from '@angular/core';
import { SECTIONS, SUBSECTIONS } from 'src/app/component/client/main/main.constant';
import { ProductService } from 'src/app/service/product/product.service';
import { CategoryModel } from 'src/app/model/product/category.model';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { FunctionsService } from 'src/app/service/configuration/functions/functions.service';
import { AuthorizationService } from 'src/app/service/configuration/authorization/authorization.service';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { SecurityService } from 'src/app/service/configuration/security/security.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { CartService } from 'src/app/service/configuration/cart/cart.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  private categories: CategoryModel[] = [];
  private logged: boolean = false;

  private saved_languages: Object[];
  private saved_language: Object;

  private displayLang: boolean = false;
  private displayLangTimeout: any = null;

  constructor(public routingService: RoutingService, 
              private productService: ProductService, 
              private authorizationService: AuthorizationService, 
              private securityService: SecurityService, 
              private languageService: LanguageService, 
              public cartService: CartService) {
              }
  
  public setLanguage(key: string) {
    if (key != this.saved_language["KEY"]) {
      this.languageService.setLanguage(key);
      window.location.reload();
    }
  }

  public displayLanguage(): boolean {
    return this.displayLang;
  }

  public toggleLanguage(): void {
    clearTimeout(this.displayLangTimeout);
    this.displayLang = !this.displayLang;
    if (this.displayLang) {
      this.displayLangTimeout = setTimeout(() => {
        this.displayLang = false;
      }, 2000);
    }
  }
  
  public language(): Object {
    return this.saved_language;
  }

  public languages(): Object[] {
    return this.saved_languages;
  }

  public getSections() : Object[] {
    let categories: Object[] = [];
    this.categories.forEach(category => {
      categories.push(category.getSubMenu("categories"));
    });
    return SECTIONS({ "Categories": categories });
  }

  public getSubSections() : Object[] {
    return SUBSECTIONS;
  }

  ngOnInit(): void {
    this.saved_language = this.languageService.getMessages();
    this.saved_languages = Object.values(this.languageService.getAll());
    this.productService.getCategories(0).then(result => {
      this.categories = result;
    });
    this.authorizationService.notLogged({}).then(result => {
      this.logged = result;
    });
  }

  public getLogged(): boolean {
    return this.logged;
  }

  public goLogin(): void {
    this.routingService.routeTo("/login", {});
  }

  public logOut(): void {
    this.securityService.deleteAuthToken();
  }

  public register(): void {
    this.routingService.routeTo("/register", {});
  }


}
