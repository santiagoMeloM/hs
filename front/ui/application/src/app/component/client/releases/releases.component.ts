import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProductService } from 'src/app/service/product/product.service';
import { ProductDisplayComponent } from 'src/app/component/client/product/product-display/product-display.component';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { environment } from 'src/environments/environment';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

@Component({
  selector: 'app-releases',
  templateUrl: './releases.component.html',
  styleUrls: ['./releases.component.scss']
})
export class ReleasesComponent implements OnInit {

  @ViewChild('productDisplay') productDisplay: ProductDisplayComponent;

  constructor(private routingService: RoutingService,
              private titleService: Title,
              public productService: ProductService, 
              public router: Router, 
              private languageService: LanguageService) {
                this.router.events.subscribe((event: Event) => {
                  if (event instanceof NavigationEnd) {
                    this.setProducts();
                  }
                });
              }

  public language(): Object {
    return this.languageService.getMessages();
  }

  ngOnInit(): void {
    this.titleService.setTitle(`${this.language()["RELEASES"]["TITLE"]} - High Support`);
    
  }

  private setProducts(): void {
    this.routingService.getParameters().then(data => {
      if (typeof this.productDisplay !== "undefined") {
        this.productDisplay.reset();
        this.productDisplay.getProducts(this.deliverProducts.bind(this));
      }
    });
  }

  public deliverProducts(number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      resolve(this.productService.getProductsDate(new Date(), number));
    });
  }

}
