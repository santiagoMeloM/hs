import { Component, OnInit, Input } from '@angular/core';
import { ProductModel } from 'src/app/model/product/produc.model';
import { ProductService } from 'src/app/service/product/product.service';
import { MediaService } from 'src/app/service/media/media.service';
import { environment } from 'src/environments/environment';
import { CurrencyService } from 'src/app/service/configuration/currency/currency.service';

@Component({
  selector: 'app-product-display',
  templateUrl: './product-display.component.html',
  styleUrls: ['./product-display.component.scss']
})
export class ProductDisplayComponent implements OnInit {

  @Input() productsFunc: any;
  @Input() place: string;

  private page: number = 0;
  private products: Map<string, ProductModel> = new Map();
  private media: Map<string, Object> = new Map();
  private discount: Map<string, Object> = new Map();
  private mediaArray: Map<string, string[]> = new Map();

  constructor(private productService: ProductService, 
              private mediaService: MediaService,
              private currencyService: CurrencyService) { }

  ngOnInit(): void {
    this.getProducts(this.productsFunc);
  }

  public whichPlace(): string {
    return this.place;
  }

  public reset() {
    this.page = 0;
    this.products.clear();
    this.media.clear();
    this.discount.clear();
    this.mediaArray.clear();
  }

  public getProductsSaved(): Map<string, ProductModel> {
    return this.products;
  }

  public getDiscountsSaved(): Map<string, Object> {
    return this.discount;
  }

  public getMediaSaved(): Map<string, Object> {
    return this.media;
  }

  public getMediaArraySaved(): Map<string, string[]> {
    return this.mediaArray;
  }

  public getProductKeys(): any {
    return Array.from(this.products.keys());
  }

  public getCurrency(): string {
    return this.currencyService.getCurrencyName();
  }

  private setProductsEmpty(data: ProductModel[]): number[] {
    let productIds: number[] = [];
    data.forEach(product => {
      this.products.set(product.getId().toString(), product);
      this.media.set(product.getId().toString(), {"load":false, "image": ""});
      this.discount.set(product.getId().toString(), {"discount":false, "discounted": product.getPrice().toFixed(2) });
      productIds.push(product.getId());
    });
    return productIds;
  }

  private setDiscounts(productIds: number[]): void {
    this.productService.getProductDiscounts(productIds).then(discounts => {
      discounts.forEach(discounted => {
        if (discounted.getState() == 1) {
          let lastPrice: number = this.products.get(discounted.getProductId().toString()).getPrice();
          let amount: number = (lastPrice/100)*(100-discounted.getPorgentage());
          this.products.get(discounted.getProductId().toString()).setPrice(amount);
          this.discount.get(discounted.getProductId().toString())["discount"] = true;
        }
      });
      this.products.forEach((value, key) => {
        value.setPrice(parseFloat(this.products.get(key).getPrice().toFixed(2)));
        this.products.set(key, value);
      });
    });
  }

  private setMedias(productIds: number[]): void {
    this.mediaService.getMediaProducts(productIds).then(medias => {
      let productMedia: Map<string, string[]> = new Map();
      medias.forEach(media => {
        let details: Object = media.getDetail();
        if (!productMedia.has(details["product"].toString())) {
          productMedia.set(details['product'].toString(), [media.getBase64()]);
          this.media.get(details['product'].toString())["image"] = media.getBase64();
          setTimeout(() => {
            this.media.get(media.getDetail()['product'].toString())["load"] = true;
          }, 200);
        } else {
          productMedia.get(details["product"].toString()).push(media.getBase64());
        }
      });
      this.mediaArray = productMedia;
    });
  }

  public getProducts(productsFunc): void {
    productsFunc(this.page).then(data => {
      let productIds: number[] = this.setProductsEmpty(data);
      this.setDiscounts(productIds);
      this.setMedias(productIds);
    });
  }

}
