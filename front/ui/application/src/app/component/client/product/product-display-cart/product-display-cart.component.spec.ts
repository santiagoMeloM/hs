import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDisplayCartComponent } from './product-display-cart.component';

describe('ProductDisplayCartComponent', () => {
  let component: ProductDisplayCartComponent;
  let fixture: ComponentFixture<ProductDisplayCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDisplayCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDisplayCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
