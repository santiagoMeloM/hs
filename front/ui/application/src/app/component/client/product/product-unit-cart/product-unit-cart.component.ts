import { Component, OnInit, Input } from '@angular/core';
import { CartService } from 'src/app/service/configuration/cart/cart.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { ProductModel } from 'src/app/model/product/produc.model';
import { environment } from 'src/environments/environment';
import { CurrencyService } from 'src/app/service/configuration/currency/currency.service';

@Component({
  selector: 'app-product-unit-cart',
  templateUrl: './product-unit-cart.component.html',
  styleUrls: ['./product-unit-cart.component.scss']
})
export class ProductUnitCartComponent implements OnInit {

  private intervalId: any = null;

  @Input() place: string;
  @Input() id: number;
  @Input() name: string;
  @Input() price: number;
  @Input() discounted: number;
  @Input() currency: string;
  @Input() new: boolean;
  @Input() discount: boolean;
  @Input() load: boolean = false;
  @Input() image: string = "assets/product/white.png";
  @Input() images: string[] = [];

  private size: Object;
  private color: string;
  private quantity: number;
  private max_quantity: number;

  constructor(private cartService: CartService, 
              private languageService: LanguageService, 
              private routingService: RoutingService, 
              private currencyService: CurrencyService) { }

  ngOnInit(): void {
    let content: Object = this.cartService.getCartProductsContent()[this.id];
    if (typeof content !== "undefined") {
      this.color = content["color"];
      this.quantity = parseInt(content["quantity"]);
      this.max_quantity = parseInt(content["max_quantity"]);
      this.size = environment.GLOBAL.SIZES.filter(size => {
        return content["size"] == size["key"];
      })[0];
    }
  }

  public getConvertedPrice(price: number): number {
    return this.currencyService.getConvertedPrice(price);
  }

  public getConvertedCurrency(): string {
    return this.currencyService.getCurrencyName();
  }

  public language(): Object {
    return this.languageService.getMessages();
  }

  public whichPlace(): string {
    return this.place;
  }

  public getQuantity(): number {
    return this.quantity;
  }

  public getSize(): string {
    let result: string = typeof this.size !== "undefined" ? this.size["name"] : "";
    return result;
  }

  public getColor(): string {
    return this.color;
  }

  public actionMsg(): string {
    let result: string;
    switch (this.place) {
      case "cart":
        result = this.isOnCart() ? this.language()["PRODUCT"]["CART"]["ON_CART"] : this.language()["PRODUCT"]["CART"]["OFF_CART"];
        break;
      case "detail":
        result = this.isOnCart() ? this.language()["PRODUCT"]["CART"]["ON_CART"] : this.language()["PRODUCT"]["CART"]["ADD_TO_CART"];
        break;
      case "main":
        result = this.language()["PRODUCT"]["SEE_MORE"];
        break;
    };
    return result;
  }

  public getItemFunction(): any {
    let result: any;
    switch (this.place) {
      case "cart":
        result = ()=>{this.cartService.deleteFromCart(this.id)};
        break;
      case "detail":
        result = this.addToCart.bind(this);
        break;
      case "main":
        result = () => {this.routingService.routeTo("/detail", {"id": this.id})};
        break;
    };
    return result;
  }

  public setImage(): void {
    let index: number = 1%this.images.length;
    this.image = this.images[index];
    this.intervalId = setInterval(() => {
      index = (index+1)%this.images.length;
      this.image = this.images[index];
    }, 700);
  }

  public resetImage(): void {
    if (this.intervalId != null) {
      clearInterval(this.intervalId);
      this.image = this.images[0];
    }
  }

  public addToCart(): void {
    if (!this.cartService.isOnCart(this.id))
      this.cartService.addToCart(this.id);
    else
      this.cartService.deleteFromCart(this.id);
  }

  public isOnCart(): boolean {
    return this.cartService.isOnCart(this.id);
  }

  private alterProduct(): void {
    let productObject: Object = {
      "id": this.id,
      "size": this.size["key"],
      "color": this.color,
      "quantity": this.quantity,
      "max_quantity": this.max_quantity
    };
    this.cartService.addToCart(productObject);
  }

  public incrementQuantity(): void {
    if (this.max_quantity > this.quantity) {
      this.quantity += 1;
      this.alterProduct();
    }
  }

  public decrementQuantity(): void {
    if (this.quantity > 0) {
      this.quantity -= 1;
      this.alterProduct();
    }
  }

}
