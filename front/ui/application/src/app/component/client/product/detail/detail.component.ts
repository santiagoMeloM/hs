import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { ProductService } from 'src/app/service/product/product.service';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { ProductModel } from 'src/app/model/product/produc.model';
import { MediaService } from 'src/app/service/media/media.service';
import { CartService } from 'src/app/service/configuration/cart/cart.service';
import { Title } from '@angular/platform-browser';
import { CharacteristicModel } from 'src/app/model/product/characteristic';
import { environment } from 'src/environments/environment';
import { CurrencyService } from 'src/app/service/configuration/currency/currency.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
    
  private product: ProductModel;
  public discount: boolean = false;
  public discounted: string;
  public images: string[] = [];
  public imageSeen: number = 0;
  public load: boolean = false;
  public image: string = "assets/product/white.png";
  public currency: string;

  private characteristics: CharacteristicModel = new CharacteristicModel();
  private sizes: Object[] = [];
  private sizeQuantities: Object = {};
  private quantity: number = 0;

  public selections: Object = {"size": null, "color": null};

  private intervalId: any = null;

  constructor(private routingService: RoutingService,
              private titleService: Title,
              public productService: ProductService, 
              private mediaService: MediaService,
              public router: Router, 
              private languageService: LanguageService, 
              private cartService: CartService, 
              private currencyService: CurrencyService) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.setProducts();
      }
    });
  }

  public language(): Object {
    return this.languageService.getMessages();
  }

  ngOnInit(): void {
    this.titleService.setTitle(`${this.language()["DETAIL"]["TITLE"]} - High Support`);
  }

  public getProduct(): ProductModel {
    return this.product;
  }

  public isOnCart(): boolean {
    return this.cartService.isOnCart(this.product.getId());
  }

  public getQuantity(): number {
    return this.quantity;
  }

  public isDiscounted(): boolean {
    return parseInt(this.discounted) > this.getProduct().getPrice();
  }

  public addToCart(): void {
    let productObject: Object = {
      "id": this.product.getId(),
      "size": this.selections["size"],
      "color": this.selections["color"],
      "quantity": this.quantity,
      "max_quantity": this.sizeQuantities[this.selections["size"]][this.selections["color"]]
    };
    if (!this.cartService.isOnCart(this.product.getId()))
      this.cartService.addToCart(productObject);
    else
      this.cartService.deleteFromCart(this.product.getId());
  }

  private setProducts(): void {
    this.routingService.getParameters().then(data => {
      this.currency = this.currencyService.getCurrencyName();
      this.productService.getProductIds([data["id"]]).then(result => {
        this.product = new ProductModel().deserealize(result[0]);
        this.discounted = this.product.getPrice().toFixed(2);
        this.discount = true;
        this.load = false;
        this.selections = { "size": null, "color": null };
        this.setDiscounts();
        this.setMedias();
        this.setCharact();
      });
    });
  }

  private setDiscounts(): void {
    this.productService.getProductDiscounts([this.product.getId()]).then(discounts => {
      discounts.forEach(discounted => {
        if (discounted.getState() == 1) {
          let lastPrice: number = this.product.getPrice();
          let amount: number = (lastPrice/100)*(100-discounted.getPorgentage());
          this.product.setPrice(amount);
          this.discount = true;
        }
      });
      this.product.setPrice(parseFloat(this.product.getPrice().toFixed(2)));
    });
  }

  private setMedias(): void {
    this.mediaService.getMediaProducts([this.product.getId()], true).then(medias => {
      medias.forEach(media => {
        this.images.push(media.getBase64());
      });
      this.image = this.images[0];
      setTimeout(() => {
        this.load = true;
      }, 200);
    });
  }

  public setImage(): void {
    let index: number = 1%this.images.length;
    this.image = this.images[index];
    this.imageSeen = index;
    this.intervalId = setInterval(() => {
      index = (index+1)%this.images.length;
      this.image = this.images[index];
      this.imageSeen = index;
    }, 700);
  }

  public resetImage(): void {
    if (this.intervalId != null) {
      clearInterval(this.intervalId);
      this.imageSeen = 0;
      this.image = this.images[0];
    }
  }

  public setCharact(): void {
    this.productService.getCharacteristicProducts(this.product.getId()).then(result => {
      this.characteristics = result;
      this.setSizeCharacteristic();
    });
  }

  public getSizeCharacteristic(): Object[] {
    return this.sizes;
  }

  public setSizeCharacteristic(): void {
    let result: Object[] = environment.GLOBAL.SIZES;
    if (typeof this.characteristics.getContent() !== "undefined") {
      result = [];
      environment.GLOBAL.SIZES.forEach(size => {
        let tmpAvaliable: number = 0;
        let data = this.characteristics.getContent()["sizes"][size["key"]];
        this.sizeQuantities[size["key"]] = {};
        Object.keys(data).forEach(color => {
          this.sizeQuantities[size["key"]][`#${color}`] = data[color];
          tmpAvaliable += data[color];
        });
        let sizeObject: Object = {"key": size["key"], "name": size["name"], "avaliable": false};
        if (tmpAvaliable > 0) {
          sizeObject["avaliable"] = true;
        }
        result.push(sizeObject);
      });
    }
    this.sizes = result;
  }

  public getColorCharacteristic(): Object[] {
    let result: Object[] = [];
    let charact: Object = this.characteristics.getContent();
    if (typeof charact !== "undefined" && this.selections["size"] != null) {
      Object.keys(charact["sizes"][this.selections["size"]]).forEach(color => {
        let colorObject: Object = {"hex": `#${color}`, "avaliable": false};
        let value: number = charact["sizes"][this.selections["size"]][color];
        if (value > 0) {
          colorObject["avaliable"] = true;
        }
        result.push(colorObject);
      });
    }
    return result;
  }

  public selectSize(key: string): void {
    this.selections["size"] = key;
    this.selections["color"] = null;
    this.quantity = 0;
  }

  public selectColor(key: string): void {
    this.selections["color"] = key;
    this.quantity = 0;
  }

  public incrementQuantity(): void {
    let charact: Object = this.characteristics.getContent();
    if (typeof charact !== "undefined" && this.selections["size"] != null && this.selections["color"] != null) {
      if (this.sizeQuantities[this.selections["size"]][this.selections["color"]] > this.quantity) {
        this.quantity += 1;
      }
    }
  }

  public decrementQuantity(): void {
    if (this.quantity > 0) {
      this.quantity -= 1;
    }
  }

}
