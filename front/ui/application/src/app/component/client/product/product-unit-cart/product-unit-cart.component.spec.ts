import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUnitCartComponent } from './product-unit-cart.component';

describe('ProductUnitCartComponent', () => {
  let component: ProductUnitCartComponent;
  let fixture: ComponentFixture<ProductUnitCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUnitCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUnitCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
