import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProductService } from 'src/app/service/product/product.service';
import { ProductDisplayComponent } from 'src/app/component/client/product/product-display/product-display.component';
import { RoutingService } from 'src/app/service/configuration/routing/routing.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  @ViewChild('productDisplay') productDisplay: ProductDisplayComponent;

  private counter: number = 0;

  private categoryId: number;
  public categoryName: string;
  public categoryDescription: string;

  constructor(private routingService: RoutingService,
              private titleService: Title,
              public productService: ProductService, 
              public router: Router, 
              private languageService: LanguageService) {
                this.router.events.subscribe((event: Event) => {
                    if (event instanceof NavigationEnd) {
                      this.setCategoryAndProducts();
                    }
                });
                
              }
  
  public language(): Object {
    return this.languageService.getMessages();
  }
  
  ngOnInit(): void {
    this.titleService.setTitle(`${this.language()["CATEGORY"]["TITLE"]} - High Support`);
  }
  
  private setCategoryAndProducts(): void {
    this.routingService.getParameters().then(data => {
      this.categoryId = parseInt(data["sub"]);
      this.categoryName = data["name"];
      this.categoryDescription = data["description"];
      this.titleService.setTitle(`${this.categoryName} - ${this.language()["CATEGORY"]["TITLE"]}`);
      if (typeof this.productDisplay !== "undefined") {
        this.productDisplay.reset();
        this.productDisplay.getProducts(this.deliverProducts.bind(this));
      }
    });
  }

  public deliverProducts(number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      resolve(this.productService.getProductsCategory(number, this.categoryId));
    });
  }

}
