import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CartService } from 'src/app/service/configuration/cart/cart.service';
import { SecurityService } from 'src/app/service/configuration/security/security.service';
import { ProductService } from 'src/app/service/product/product.service';
import { MediaService } from 'src/app/service/media/media.service';
import { CurrencyService } from 'src/app/service/configuration/currency/currency.service';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';

declare const ePayco;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  private amount: number;
  private name: string;
  private description: string;
  private country: string;
  private responseUrl: string;

  public loaded: boolean = false;
  public active: boolean = false;

  private products: Map<string, Object> = new Map();
  private originalPrice: number = 0;

  constructor(private cartService: CartService, 
              private securityService: SecurityService, 
              private productService: ProductService, 
              private mediaService: MediaService,
              public currencyService: CurrencyService, 
              private noticeService: NoticeService, 
              private languageService: LanguageService) { }

  ngOnInit(): void {
    this.noticeService.startLoading().then(info => {
      this.setData().then(info => {
        this.noticeService.closeNotice();
      });
    });
  }

  public language(): Object {
    return this.languageService.getMessages();
  }

  public getOriginalPrice(): number {
    return this.originalPrice;
  }

  public getAmount(): number {
    return this.amount;
  }

  public getDiscount(): number {
    return parseFloat((this.originalPrice-this.amount).toFixed(2));
  }

  public getProducts(): Object[] {
    let result: Object[] = [];
    this.products.forEach((value, key) => {
      result.push(value);
    });
    return result;
  }

  private setProducts(ids: number[], details: Object): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.productService.getProductIds(ids).then(result => {
        result.forEach(product => {
          product.setPrice(this.currencyService.getConvertedPrice(product.getPrice()));
          let id: string = `${product.getId()}`;
          let content: Object = {
            id: product.getId(),
            name: product.getName(),
            price: product.getPrice(),
            quantity: details[id]['quantity'],
            color: details[id]['color'],
            size: environment.GLOBAL.SIZES.filter(size => {
              return details[id]['size'] == size["key"];
            })[0]['name']
          };
          this.originalPrice += (product.getPrice()*details[id]["quantity"]);
          this.products.set(id, content);
        });
        this.amount = this.currencyService.getConvertedPrice(this.amount);
        let tmp: Object = {};
        this.products.forEach((value, key) => {
          tmp[key] = value;
        });
        this.description = JSON.stringify(tmp);
        this.mediaService.getMediaProducts(ids).then(result => {
          result.forEach(media => {
            let id: string = `${media.getDetail()['product']}`;
            let tmpProduct: Object = this.products.get(id);
            tmpProduct["image"] = media.getBase64();
            this.products.set(id, tmpProduct);
          });
          resolve(true);
        });
      });
    });
  }

  private setCartData(): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      let content: any = this.cartService.getCartProductsContent();
      let order: any;
      let watchData = () => {
        order = this.cartService.getOrderInfo();
        if (order["totalPrice"] == 0) { // Gotta fix the discount cant take price less then 0
          setTimeout(watchData, 250);
        } else {
          resolve({
            content: content,
            order: order
          });
        }
      }
      watchData();
    });
  }

  private setData(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.setCartData().then(cartData => {
        this.securityService.getAuthDecodedToken().then(result => {
          let userData = result;
          this.amount = cartData['order']['totalPrice'];
          this.name = userData['username'];
          let ids: number[] = [];
          Object.keys(cartData["content"]).forEach(key => {
            ids.push(parseInt(key));
          });
          this.country = this.currencyService.getZone();
          this.responseUrl = "https://google.com" // Need to fix this for a real resonse url
          this.setProducts(ids, cartData['content']).then(info => {
            this.loaded = true;
            resolve(true);
          });
        });
      });
    });
  }

  public openCheckout(): void {
    if (this.loaded) {
      this.noticeService.startLoading().then(info => {
        this.active = true;
        let handler: any = ePayco.checkout.configure(environment.CHECKOUT.CONFIGURATION);
        handler.open(environment.CHECKOUT.DATA(this.amount, this.name, this.description, this.country, this.responseUrl));
      });
    }
  }

  public hey(a) {
    console.log(a);
    return "hey";
  }

}
