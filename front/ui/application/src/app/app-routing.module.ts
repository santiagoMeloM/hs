import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from 'src/app/component/client/main/main.component';
import { HomeComponent } from 'src/app/component/client/home/home.component';
import { ReleasesComponent } from 'src/app/component/client/releases/releases.component';
import { CategoriesComponent } from 'src/app/component/client/categories/categories.component';
import { DealsComponent } from 'src/app/component/client/deals/deals.component';
import { StoreComponent } from 'src/app/component/admin/store/store.component';
import { AdminMainComponent } from 'src/app/component/admin/admin-main/admin-main.component';
import { ProductDetailAdminComponent } from 'src/app/component/admin/product-detail-admin/product-detail-admin.component';
import { ProductAdminComponent } from 'src/app/component/admin/module/product-admin/product-admin.component';
import { ProductListAdminComponent } from 'src/app/component/admin/module/product-list-admin/product-list-admin.component';
import { CategoryAdminComponent } from 'src/app/component/admin/module/category-admin/category-admin.component';
import { DiscountAdminComponent } from 'src/app/component/admin/module/discount-admin/discount-admin.component';
import { CategoryListAdminComponent } from 'src/app/component/admin/module/category-list-admin/category-list-admin.component';
import { DiscountListAdminComponent } from 'src/app/component/admin/module/discount-list-admin/discount-list-admin.component';
import { AppGuard } from 'src/app/app.guard';
import { environment } from 'src/environments/environment';
import { AuthenticationComponent } from './component/authentication/authentication.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { DetailComponent } from './component/client/product/detail/detail.component';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { FeedbackComponent } from './component/information/feedback/feedback.component';
import { AboutComponent } from './component/information/about/about.component';
import { HelpComponent } from './component/information/help/help.component';
import { TrackingComponent } from './component/information/tracking/tracking.component';
import { InformationComponent } from './component/information/information.component';
import { InformativeComponent } from './component/information/informative/informative.component';
import { CheckoutComponent } from './component/client/checkout/checkout.component';

export const routes: Routes = [
  {
    path: "login",
    canActivate: [AppGuard],
    data: environment.SITES.DATA.LOGIN,
    component: AuthenticationComponent
  },
  {
    path: "register",
    canActivate: [AppGuard],
    data: environment.SITES.DATA.REGISTER,
    component: RegistrationComponent
  },
  {
    path: "checkout",
    canActivate: [AppGuard],
    data: environment.SITES.DATA.CHECKOUT,
    component: CheckoutComponent
  },
  {
    path: "info",
    component: InformationComponent,
    children: [
      {
        path: "feedback",
        component: FeedbackComponent
      },
      {
        path: "about",
        component: AboutComponent
      },
      {
        path: "help",
        component: HelpComponent
      },
      {
        path: "track",
        component: TrackingComponent
      }
    ]
  },
  {
    path: "informative",
    component: InformativeComponent,
  },
  {
    path: "administration",
    canActivate: [AppGuard],
    data: environment.SITES.DATA.ADMINISTRATION,
    component: AdminMainComponent,
    children: [
      {
        path: "store",
        component: StoreComponent,
        children: [
          {
            path: "padding",
            component: ProductAdminComponent
          },
          {
            path: "products",
            component: ProductListAdminComponent
          },
          {
            path: "discounts",
            component: DiscountListAdminComponent
          },
          {
            path: "categories",
            component: CategoryListAdminComponent
          },
          {
            path: "cadding",
            component: CategoryAdminComponent
          },
          {
            path: "dadding",
            component: DiscountAdminComponent
          }
        ]
      },
      {
        path: "product",
        component: ProductDetailAdminComponent
      }
    ]
  }, 
  {
    path: "",
    component: MainComponent,
    children: [
      { 
        path: "",
        component: HomeComponent
      },
      { 
        path: "releases",
        component: ReleasesComponent
      },
      { 
        path: "categories",
        component: CategoriesComponent
      },
      { 
        path: "deals",
        component: DealsComponent
      }, 
      {
        path: "detail",
        component: DetailComponent
      }
    ]
  },
  {
    path: "notfound",
    component: NotfoundComponent
  },
  {
    path: "**",
    redirectTo: "notfound"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
