
import { Component } from '@angular/core';
import { NoticeService } from 'src/app/service/configuration/notice/notice.service';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { CartModalService } from 'src/app/service/configuration/cart/cart-modal/cart-modal-service/cart-modal.service';
import { CurrencyService } from './service/configuration/currency/currency.service';
import { NoticeModalService } from './service/configuration/notice/notice-modal/notice-modal-service/notice-modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'application';
  
  constructor(public noticeService: NoticeService, 
              private languageService: LanguageService,
              public cartModalService: CartModalService,
              public noticeModalService: NoticeModalService,
              private currencyService: CurrencyService) {
                this.currencyService.load();
              }

  public language(): Object {
    return this.languageService.getMessages();
  }

}
