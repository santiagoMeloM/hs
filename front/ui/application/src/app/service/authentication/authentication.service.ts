import { Injectable } from '@angular/core';
import { DiscoverService } from '../configuration/discover/discover.service';
import { environment } from 'src/environments/environment';
import { LanguageService } from '../configuration/language/language.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private discoverService: DiscoverService, private languageService: LanguageService) { }

  public authentication(username: string, password: string): Promise<any> {
    let body: Object = { "username": username, "password": password };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.AUTH.POST.LOGIN.URL, {}, body, []);
    let result: Promise<any> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        } else {
          resolve(false);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public session(username: string): Promise<any> {
    let body: Object = { "username": username };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.AUTH.POST.SESSION.URL, {}, body, []);
    let result: Promise<any> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        } else {
          resolve(false);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public desession(username: string): Promise<any> {
    let body: Object = { "username": username };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.AUTH.POST.DESESSION.URL, {}, body, []);
    let result: Promise<any> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        } else {
          resolve(false);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public createUser(username: string, password: string): Promise<boolean> {
    let body: Object = { "username": username, "password": password, "language": this.languageService.getLanguage() };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.AUTH.POST.CREATE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        } else {
          resolve(false);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

}
