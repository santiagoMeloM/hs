import { Injectable } from '@angular/core';
import { AuthorizationService } from '../authorization/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor(private authorizationService: AuthorizationService) { }

  readonly detail: Object = {
    "verifyRoles": this.authorizationService.verifyRole.bind(this.authorizationService),
    "notLogged": this.authorizationService.notLogged.bind(this.authorizationService)
  }

  private funcs: any[] = [];
  private params: Object = {};

  public async checkFuncs(): Promise<boolean> {
      return new Promise<boolean>(async (resolve, reject) => {
          let result: boolean = true;
          for (let i = 0; i < this.funcs.length; i++) {
              await this.funcs[i](this.params).then(response => {
                result = result && response;
              });
          }
          this.funcs = [];
          this.params = {};
          resolve(result);
      });
  }

  public addFunc(func: string): void {
    this.funcs.push(this.detail[func]);
  }

  public setParams(params: Object): void {
    this.params = params;
  }

}
