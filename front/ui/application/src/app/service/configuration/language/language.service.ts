import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private language: string = "spanish";

  constructor(private cookieService: CookieService) {
    if (this.cookieService.get("lang")) {
      this.language = this.cookieService.get("lang");
    }
  }

  public setLanguage(language: string): void {
    this.cookieService.set("lang", language);
  }

  public getMessages(): Object {
    return environment.LANGUAGE(this.language, false);
  }

  public getLanguage(): string {
    return environment.LANGUAGE(this.language, false)["KEY"].toUpperCase();
  }

  public getAll(): Object {
    return environment.LANGUAGE(this.language, true);
  }
  
}
