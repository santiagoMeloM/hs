import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from "@auth0/angular-jwt";
import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  
  private system_token: string = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6WyJzeXN0ZW0iLCJhZG1pbiJdfQ.3mcy-rPeWxxRMqydA_IGA6iqVtEwbwgjTBRDlb-Z1Xo";

  constructor(private cookieService: CookieService, private authenticationService: AuthenticationService) {
    this.cookieService.set("system", this.system_token,  0, "/");
  }

  private jwtHelperService: JwtHelperService = new JwtHelperService();

  private getAuthToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (this.cookieService.get("authtoken")) {
        let decodedData: Object = this.jwtHelperService.decodeToken(this.cookieService.get("authtoken"));
        this.authenticationService.session(decodedData["username"]).then(result => {
          if (!result) {
            this.cookieService.delete("authtoken");
            reject();
          }
          resolve(result["token"]);
        });
      } else {
        reject();
      }
    });
  }

  public getTransactionToken(): string {
    return this.cookieService.get("authtoken");
  }

  public setAuthToken(token: string): void {
    this.cookieService.set("authtoken", token,  0, "/");
    this.getAuthDecodedToken().then(decoded => {}, (error) => {
      this.deleteAuthToken();
    });
  }

  public deleteAuthToken(): void {
    console.log(this.cookieService.get("authtoken"))
    let decodedData: Object = this.jwtHelperService.decodeToken(this.cookieService.get("authtoken"));
    this.authenticationService.desession(decodedData["username"]).then(result => {
      if (result) {
        this.cookieService.delete("authtoken");
        window.location.reload();
      }
    });
  }

  public getAuthDecodedToken(): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      try {
        this.getAuthToken().then(token => {
          if (token) {
            let decodedData: Object = this.jwtHelperService.decodeToken(token);
            resolve(decodedData);
          } else {
            reject();
          }
        }, (error) => {
          reject();
        });
      } catch(error) {
        resolve(null);
      }
    });
  }

  public getSystemDecodedToken(): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      try {
        let decodedData: Object = this.jwtHelperService.decodeToken(this.cookieService.get("system"));
        resolve(decodedData);
      } catch(error) {
        resolve(null);
      }
    });
  }


}
