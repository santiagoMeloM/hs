import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoticeModalService {

  private display: boolean = false;
  private function: any = () => {};
  private response: Object = {
    valid: false,
    value: null
  };

  constructor() { }

  public setFunction(func: any): void {
    this.function = func;
  }

  private reset(): void {
    this.display = false;
    this.function = () => {};
    this.response = {
      valid: false,
      value: null
    };
  }

  public getDisplay(): boolean {
    return this.display;
  }

  public onDisplay(): Promise<any>{
    this.display = true;
    return new Promise<any>((resolve, reject) => {resolve(true)});
  }

  public onDisplayResponse(): Promise<any> {
    this.display = true;
    return new Promise<any>((resolve, reject) => {
      function waitForFoo(){
        if (this.response['valid']) {
          let tmp: any = this.response['value'];
          return resolve(tmp);
        }
        setTimeout(waitForFoo.bind(this), 200);
      };
      waitForFoo.bind(this)();
    });
  }

  public offDisplay(): void {
    this.reset();
  }

  public setDisplayResponse(result: any): void {
    this.response = {"valid":true, "value": result};
  }
  
  public autoClose(time: number): Promise<any> {
    this.onDisplay();
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        this.offDisplay();
        resolve(true);
      }, time);
    }); 
  }

}
