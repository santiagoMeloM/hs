import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NoticeModalService } from './notice-modal/notice-modal-service/notice-modal.service';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  private options: Object = environment.NOTICE.OPTIONS.EMPTY;

  constructor(private modalService: NoticeModalService) {}

  public getOptions(): Object {
    return this.options;
  }

  public ok(msg: string): Promise<any> {
    this.options = environment.NOTICE.OPTIONS.OK(msg);
    return this.modalService.onDisplayResponse();
  }

  public okCancel(msg: string): Promise<any> {
    this.options = environment.NOTICE.OPTIONS.OKCANCEL(msg);
    return this.modalService.onDisplayResponse();
  }

  public startLoading(): Promise<any> {
    this.options = environment.NOTICE.OPTIONS.LOADING();
    return this.modalService.onDisplay();
  }

  public closeNotice(): void {
    this.modalService.offDisplay();
  }

  public closeNoticeTimed(): void {
    setTimeout(() => {
      this.modalService.offDisplay();
    }, environment.NOTICE.CLOSE_TIME);
  }

}
