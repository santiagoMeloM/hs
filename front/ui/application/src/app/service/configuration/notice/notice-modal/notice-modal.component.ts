import { Component, OnInit } from '@angular/core';
import { NoticeModalService } from './notice-modal-service/notice-modal.service';
import { NoticeService } from '../notice.service';
import { LanguageService } from '../../language/language.service';

@Component({
  selector: 'app-notice-modal',
  templateUrl: './notice-modal.component.html',
  styleUrls: ['./notice-modal.component.scss']
})
export class NoticeModalComponent implements OnInit {

  constructor(public modalService: NoticeModalService, 
              public noticeService: NoticeService, 
              private languageService: LanguageService) { }

  ngOnInit(): void {}

  public language(): Object {
    return this.languageService.getMessages();
  }

}
