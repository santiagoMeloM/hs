import { TestBed } from '@angular/core/testing';

import { NoticeModalService } from './notice-modal.service';

describe('NoticeModalService', () => {
  let service: NoticeModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoticeModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
