import { Injectable } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { NONE_TYPE } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  private counter: number = 0;
  private lastpage: string = null;

  constructor(private routerService: Router, 
              private route: ActivatedRoute, 
              private  router: Router, 
              private location: Location) {
                this.router.events.subscribe((event: Event) => {
                  if (event instanceof NavigationEnd) {
                    if (this.counter >= environment.RELOAD_QUOTA) {
                      this.counter = 0;
                      window.location.reload();
                    }
                    this.counter += 1;
                  }
                });
              }

  public getParameters(): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      this.route.queryParams.subscribe(params => {
        let info: any = params["info"] || null;
        resolve(info != null ? JSON.parse(atob(info)) : null);
      });
    });
  }

  public routeTo(url: string, params: Object): void {
    this.lastpage = this.location.path();
    if (typeof url !== "undefined") {
      let paramsCheck: boolean = params != null && Object.keys(params).length > 0;
      let parameters: Object = paramsCheck ? { info: btoa(JSON.stringify(params)) } : {};
    if (typeof url !== undefined)
      this.routerService.navigate([`/${url}`], { queryParams: parameters }).then(result => {
      });
    }
  }

  public goBack(optional?: Object) {
    if (this.lastpage != null) {
      this.routeTo(this.lastpage, {});
    } else if (typeof optional !== "undefined") {
      this.routeTo(optional["url"], optional["params"]);
    } else {
      this.routeTo("", {});
    }
  }

}
