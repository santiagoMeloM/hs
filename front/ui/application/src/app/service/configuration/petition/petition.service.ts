import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Response } from 'src/app/model/configuration/response.model';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class PetitionService {

  constructor(private http: HttpClient, 
              private cookieService: CookieService) { }

  private getHeader(): HttpHeaders {
    return new HttpHeaders({
      "Authentication": this.cookieService.get("authtoken") || "",
      "Sauthentication": this.cookieService.get("system") || "",
      "Content-Type": "application/json"
    });
  }

  private getParameter(parameters: any): any {
    return new HttpParams({ fromObject: parameters });
  }

  private completeUrl(url: string, args: string[]): string {
    args.forEach(argument => {
      url += `/${argument}`;
    });
    return url;
  }

  public getRaw(url: string, parameters: Object, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.get(
        this.completeUrl(url, args), 
        { 
          headers: this.getHeader(), 
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(result);
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

  public getVanilla(url: string, parameters: Object, args: string[], headers: HttpHeaders): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.get(
        this.completeUrl(url, args), 
        {
          headers: headers,
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(result);
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

  public get(url: string, parameters: Object, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.get(
        this.completeUrl(url, args), 
        { 
          headers: this.getHeader(), 
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(new Response().deserealize(result));
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

  public post(url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.post(
        this.completeUrl(url, args), 
        JSON.stringify(body),
        { 
          headers: this.getHeader(), 
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(new Response().deserealize(result));
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

  public put(url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.put(
        this.completeUrl(url, args), 
        JSON.stringify(body),
        { 
          headers: this.getHeader(), 
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(new Response().deserealize(result));
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

  public delete(url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      this.http.request(
        'delete',
        this.completeUrl(url, args),
        {
          body: JSON.stringify(body),
          headers: this.getHeader(), 
          params: this.getParameter(parameters) 
        }).subscribe(result => {
          resolve(new Response().deserealize(result));
        }, (error) => {
          reject(error);
        });
    });
    return response;
  }

}
