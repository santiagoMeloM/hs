import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartModalService {

  private display: boolean = false;
  private function: any = () => {};

  constructor() { }

  public setFunction(func: any): void {
    this.function = func;
  }

  public getDisplay(): boolean {
    return this.display;
  }

  public onDisplay(): void {
    this.function();
    this.display = true;
  }

  public offDisplay(): void {
    this.display = false;
  }
  
}
