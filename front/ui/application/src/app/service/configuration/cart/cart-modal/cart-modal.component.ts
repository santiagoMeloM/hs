import { Component, OnInit, ViewChild } from '@angular/core';
import { LanguageService } from 'src/app/service/configuration/language/language.service';
import { CartModalService } from './cart-modal-service/cart-modal.service';
import { ProductService } from 'src/app/service/product/product.service';
import { ProductDisplayCartComponent } from 'src/app/component/client/product/product-display-cart/product-display-cart.component';
import { CartService } from '../cart.service';
import { CurrencyService } from '../../currency/currency.service';
import { RoutingService } from '../../routing/routing.service';

@Component({
  selector: 'app-cart-modal',
  templateUrl: './cart-modal.component.html',
  styleUrls: ['./cart-modal.component.scss']
})
export class CartModalComponent implements OnInit {

  @ViewChild('productDisplay') productDisplay: ProductDisplayCartComponent;

  constructor(private languageService: LanguageService, 
              public modalService: CartModalService,
              public productService: ProductService, 
              private cartService: CartService, 
              private currencyService: CurrencyService, 
              private routingService: RoutingService) { }

  ngOnInit(): void {
    this.modalService.setFunction(this.resetData.bind(this));
  }

  public getCurrentPrice(): number {
    return this.cartService.getOrderInfo()["totalPrice"];
  }

  public getCurrentQuantity(): number {
    return this.cartService.getOrderInfo()["numberProducts"];
  }

  public getCurrency(): string {
    return this.currencyService.getCurrencyName();
  }

  public resetData(): void {
    this.productDisplay.reset();
    this.productDisplay.getProducts(this.deliverProducts.bind(this));
  }

  public language(): Object {
    return this.languageService.getMessages();
  }

  public deliverProducts(number): Promise<any> {
    let products: number[] = [];
    if (localStorage.getItem("bag")) {
      let data: Object[] = this.cartService.getBag();
      data.forEach(obj => {
        products.push(obj["id"]);
      });
    }
    return new Promise<any>((resolve, reject) => {
      resolve(this.productService.getProductIds(products));
    });
  }

  public checkout(): void {
    this.modalService.offDisplay();
    this.routingService.routeTo("checkout", {});
  }

  

}
