import { Injectable, ViewEncapsulation } from '@angular/core';
import { CartModalService } from 'src/app/service/configuration/cart/cart-modal/cart-modal-service/cart-modal.service';
import { CurrencyService } from '../currency/currency.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private productsContent: Object = {};
  private orderInfo: Object = {"totalPrice": 0, "numberProducts": 0};
  private products: Object[] = [];

  constructor(private modalService: CartModalService, 
              private currencyService: CurrencyService) {}

  public getOrderInfo(): Object {
    this.setCartData();
    return this.orderInfo;
  }

  public setCartProductsContent(): void {
    let result: Object = {};
    if (this.bagExist()) {
      let data: Object[] = this.getBag();
      data.forEach(obj => {
        result[obj["id"]] = obj;
      });
    }
    this.productsContent = result;
  }

  private setCartOrderInfo(): void {
    let result: Object = {"totalPrice": 0, "numberProducts": 0};
    if (this.products.length > 0) {
      this.products.forEach(product => {
        try {
          result["numberProducts"] += this.productsContent[product["id"]]["quantity"];
          result["totalPrice"] += this.productsContent[product["id"]]["quantity"]*product["price"];
        } catch (error) {}
      });
    }
    result["totalPrice"] = this.currencyService.getConvertedPrice(result["totalPrice"]);
    this.orderInfo = result;
  }

  private setCartData(): void {
    this.setCartProductsContent();
    this.setCartOrderInfo();
  }

  public setProducts(products: Object[]): void {
    this.products = products;
  }

  public cartModal(): void {
    try {
      this.setCartData();
    } catch (error) {
      this.setBag([]);
    }
    this.modalService.onDisplay();
  }

  public getCartProductsContent(): Object {
    this.setCartData();
    return this.productsContent;
  }

  public bagExist(): boolean {
    return !(!localStorage.getItem("bag"));
  }

  public getBag(): Object[] {
    return JSON.parse(localStorage.getItem("bag"));
  }

  private setBag(data: Object): void {
    localStorage.setItem("bag", JSON.stringify(data));
  }

  public addToCart(productObject: Object): void {
    if (!this.bagExist()) {
      localStorage.setItem("bag", JSON.stringify([productObject]));
    } else {
      let data: any[] = this.getBag();
      let tmp: any[] = data.filter(product => {
        return product['id'] != productObject["id"];
      });
      tmp.push(productObject);
      this.setBag(tmp);
    }
    this.setCartData();
  }

  public deleteFromCart(productId: number): void {
    let result: any[] = [];
    let data: any[] = this.getBag();
    data.forEach(obj => {
      if (obj["id"] != productId)
        result.push(obj);
    });
    this.setBag(result);
    this.setCartData();
  }

  public isOnCart(id: number): boolean {
    let result: boolean = false;
    if (this.bagExist()) {
      let data: any[] = this.getBag();
      result = data.filter(product => {
        return product["id"] == id;
      }).length > 0;
    }
    return result;
  }
  
}
