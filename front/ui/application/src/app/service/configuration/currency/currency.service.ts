import { Injectable } from '@angular/core';
import { PetitionService } from '../petition/petition.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocationStrategy } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private loadCurrency: Promise<any>;

  private currency: Object = { "name": "COP", "rate": 1};
  private zone_currency: Object = { "name": null, "rate": null}
  private zone: string = "CO";

  constructor(private petitionService: PetitionService, private httpService: HttpClient) {}

  public getZone(): string {
    return this.zone;
  }

  public load(): void {
    this.getIp().then(ip => {
      this.setZone(ip).then(tmp => {
       this.setCurrency();
      });
    });
  }

  private getIp(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.petitionService.getVanilla(environment.GLOBAL.CURRENCY.IP.URL, {}, [], environment.GLOBAL.CURRENCY.IP.HEADERS).then(result => {
        resolve(result["ip"]);
      });
    });
  }

  private setCurrency(): void {
    this.loadCurrency = new Promise<any>((resolve, reject) => {
      let parameters: Object = environment.GLOBAL.CURRENCY.CONVERT.PARAMETERS(this.currency["name"], this.zone_currency["name"]);
      let headers: HttpHeaders = environment.GLOBAL.CURRENCY.CONVERT.HEADERS;
      this.petitionService.getVanilla(environment.GLOBAL.CURRENCY.CONVERT.URL, parameters, [], headers).then(result => {
        if (result != 0) {
          this.zone_currency["rate"] = result;
        }
        resolve(true);
      });
    });
  }

  private setZone(ip): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      let parameters: Object = environment.GLOBAL.CURRENCY.ZONE.PARAMETERS(ip);
      let headers: HttpHeaders = environment.GLOBAL.CURRENCY.ZONE.HEADERS;
      this.petitionService.getVanilla(environment.GLOBAL.CURRENCY.ZONE.URL, parameters, [], headers).then(result => {
        if (result["code"] == 200) {
          let tmpCode: string = result["result"]["countrycode"];
          this.zone = tmpCode;
          this.zone_currency["name"] = environment.GLOBAL.COUNTRY.CURRENCY[result["result"]["countrycode"]];
        }
        resolve(true);
      });
    });
  }

  public hasCurrencyLoaded(): Promise<any> {
    return this.loadCurrency;
  }

  public getCurrencyName(): string {
    return this.currency["name"];
  }

  public getCOPValue(): number {
    return this.currency["rate"];
  }

  public getConvertedPrice(price: number): number {
    if (!(this.zone_currency["name"] == null || this.zone_currency["rate"] == null)) {
      this.currency = this.zone_currency;
    }
    return parseFloat((this.getCOPValue()*price).toFixed(2));
  }
}
