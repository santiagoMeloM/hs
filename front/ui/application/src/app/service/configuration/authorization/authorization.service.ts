import { Injectable } from '@angular/core';
import { SecurityService } from '../security/security.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private securityService: SecurityService) { }

  public verifyRole(params: Object): Promise<boolean> {
    let roles: string[] = params["roles"];
    return new Promise<boolean>((resolve, reject) => {
      this.securityService.getAuthDecodedToken().then(decoded => {
        if (decoded != null) {
          let result: boolean = true;
          roles.forEach(role => {
            result = result && decoded["roles"].includes(role);
          });
          resolve(result);
        } else
          resolve(false);
      }, (error) => {
        resolve(false);
      });
    });
  }

  public notLogged(params: Object): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.securityService.getAuthDecodedToken().then(decoded => {
        if (decoded != null) {
          resolve(!decoded.hasOwnProperty("roles"));
        } else
          resolve(true);
      }, (error) => {
        resolve(true);
      });
    });
  }
  
}
