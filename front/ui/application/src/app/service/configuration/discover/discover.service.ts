import { Injectable, ɵConsole } from '@angular/core';
import { PetitionService } from 'src/app/service/configuration/petition/petition.service';
import { environment } from 'src/environments/environment';
import { DiscoverModel } from 'src/app/model/configuration/discover.model';

@Injectable({
  providedIn: 'root'
})
export class DiscoverService {

  constructor(private petitionService: PetitionService) { }

  private choose(type: string, url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let result: Promise<any>;
    switch(type) {
      case "GET": {
        result = this.petitionService.get(url, parameters, args);
        break;
      }
      case "POST": {
        result = this.petitionService.post(url, parameters, body, args);
        break;
      }
      case "PUT": {
        result = this.petitionService.put(url, parameters, body, args);
        break;
      }
      case "DELETE": {
        result = this.petitionService.delete(url, parameters, body, args);
        break;
      }
    }
    return result;
  }

  public petitionE(type: string, url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      let discoverHost: string = `http://${environment.DISCOVER.HOST}:${environment.DISCOVER.PORT}/${environment.DISCOVER.INSTANCE}`;
      this.petitionService.getRaw(discoverHost, {}, []).then(data => {
        let instances: Array<any> = data.applications.application;
        let discover: DiscoverModel = new DiscoverModel().deserealize(instances.filter(instance => {
          return instance.instance[0].app == environment.ROUTE.NAME;
        })[0].instance[0]);
        let host: string;
        if (environment.production) {
          host = `http://${discover.getHostName()}:${discover.getPort()}`;
        } else {
          host = `http://0.0.0.0:${discover.getPort()}`;
        }
        let completeUrl: string = `${host}/${environment.ROUTE.TAG}/${url}`;
        this.choose(type, completeUrl, parameters, body, args).then(result => {
          resolve(result);
        }, (error) => {
          reject(error);
        });
      });
    });
    return response;
  }

  public petition(type: string, url: string, parameters: Object, body: any, args: string[]): Promise<any> {
    let response: Promise<any> = new Promise<any>((resolve, reject) => {
      let completeUrl: string = `${environment.HOST}/${url}`;
      this.choose(type, completeUrl, parameters, body, args).then(result => {
        resolve(result);
      }, (error) => {
        reject(error);
      });
    });
    return response;
  }

}
