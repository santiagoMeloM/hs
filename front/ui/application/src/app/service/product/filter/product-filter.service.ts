import { Injectable } from '@angular/core';
import { ProductModel } from 'src/app/model/product/produc.model';
import { environment } from 'src/environments/environment';
import { DiscoverService } from '../../configuration/discover/discover.service';

@Injectable({
  providedIn: 'root'
})
export class ProductFilterService {

  constructor(private discoverService: DiscoverService) { }

  public getProducts(start: number): Promise<ProductModel[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.GET.RANGE.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.PRODUCT.GET.RANGE.URL, {}, null, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }


}
