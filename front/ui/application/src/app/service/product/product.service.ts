import { Injectable } from '@angular/core';
import { DiscoverService } from 'src/app/service/configuration/discover/discover.service';
import { environment } from 'src/environments/environment';
import { ProductModel } from 'src/app/model/product/produc.model';
import { DiscountModel } from 'src/app/model/product/discount.model';
import { CategoryModel } from 'src/app/model/product/category.model';
import { CharacteristicModel } from 'src/app/model/product/characteristic';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private discoverService: DiscoverService) { }

  public addProduct(name: string, description: string, price: number, categoryId: number): Promise<any> {
    let body: Object = { "name": name, "description": description, "price": price, "category_id": categoryId, "content": environment.VALUES.PRODUCT_CONTENT };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCER.ADD_PRODUCT.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public addCategory(name: string, description: string): Promise<any> {
    let body: Object = { "name": name, "description": description };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.CATEGORY.POST.ADD.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public assingCategory(categoryId: number, productId: number): Promise<any> {
    let body: Object = { "category_id": categoryId, "product_id": productId };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.CATEGORY.POST.ASSIGN.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public unassingCategory(categoryId: number, productId: number): Promise<any> {
    let body: Object = { "id": 0, "category_id": categoryId, "product_id": productId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.CATEGORY.POST.UNASSIGN.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public addDiscount(product: number, state: number, deadline: string, porcentage: number): Promise<any> {
    let body: Object = { "product_id": product, "state": state, "deadline": deadline, "porcentage": porcentage };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.DISCOUNT.POST.ADD.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductSize(): Promise<any> {
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.PRODUCT.GET.SIZE.URL, {}, null, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProducts(start: number): Promise<ProductModel[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.GET.RANGE.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.PRODUCT.GET.RANGE.URL, {}, null, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getCharacteristicProducts(productId: number): Promise<CharacteristicModel> {
    let body: Object = { "product_id": productId };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.CHARACTERISTIC.POST.PRODUCT.URL, {}, body, []);
    let result: Promise<CharacteristicModel> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        if (response == environment.RESPONSE.SUCCESS) {
          let content: any = data['content'];
          resolve(new CharacteristicModel().deserealize(content));
        } else {
          resolve(new CharacteristicModel().deserealize({}));
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getDiscounts(start: number): Promise<Object[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.GET.RANGE.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.DISCOUNT.GET.RANGE.URL, {}, null, args);
    let result: Promise<Object[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let info: Object[] = [];
          if (Array.isArray(content)) {
            content.forEach(details => {
              let productInfo: ProductModel = new ProductModel().deserealize(details);
              let discountInfo: DiscountModel = new DiscountModel().deserealize(details);
              info.push({
                "product": productInfo.setId(details["pid"]),
                "discount": discountInfo.setId(details["did"])
              });
            });
          }
          resolve(info);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductsAll(): Promise<ProductModel[]> {
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.PRODUCT.GET.ALL.URL, {}, null, []);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductsDisocunted(start: number): Promise<ProductModel[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.GET.DISCOUNT.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.PRODUCT.GET.DISCOUNT.URL, {}, null, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductsDate(date: Date, start: number): Promise<ProductModel[]> {
    let body: Object = { "date": date.toJSON().slice(0,10) };
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.POST.DATE.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.PRODUCT.POST.DATE.URL, {}, body, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductsDateAdmin(date: Date, start: number): Promise<ProductModel[]> {
    let body: Object = { "date": date.toJSON().slice(0,10) };
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.POST.DATE.ADMIN.QNT.toString()];
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.PRODUCT.POST.DATE.URL, {}, body, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductDiscounts(products: number[]): Promise<DiscountModel[]> {
    let body: Object = { "products": products };
    if (!products.length) return new Promise<DiscountModel[]>((resolve, reject) => {
      resolve([]);
    });
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.DISCOUNT.POST.RANGE.URL, {}, body, []);
    let result: Promise<DiscountModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let discounts: DiscountModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(discount => {
              discounts.push(new DiscountModel().deserealize(discount));
            });
          }
          resolve(discounts);
        }
        resolve([]);
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductIds(products: number[]): Promise<ProductModel[]> {
    let body: Object = { "products": products };
    if (!products.length) return new Promise<ProductModel[]>((resolve, reject) => {
      resolve([]);
    });
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.PRODUCT.POST.MANY.URL, {}, body, []);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductsCategory(start: number, categoryId: number): Promise<ProductModel[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.PRODUCT.GET.CATEGORY.QNT.toString()];
    let body: Object = { "category_id": categoryId };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.PRODUCT.GET.CATEGORY.URL, {}, body, args);
    let result: Promise<ProductModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let products: ProductModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(product => {
              let tmpProduct: ProductModel = new ProductModel().deserealize(product);
              products.push(tmpProduct);
            });
          }
          resolve(products);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getCategoriesByProduct(start: number, productId: number): Promise<CategoryModel[]> {
    let args: string[] = [start.toString(), environment.PRODUCT.CATEGORY.POST.PRODUCT.QNT.toString()];
    let body: Object = { "product_id": productId };
    let petition: Promise<any> = this.discoverService.petition("POST", environment.PRODUCT.CATEGORY.POST.PRODUCT.URL, {}, body, args);
    let result: Promise<CategoryModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let categories: CategoryModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(category => {
              let tmpCategory: CategoryModel = new CategoryModel().deserealize(category);
              categories.push(tmpCategory);
            });
          }
          resolve(categories);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getCategorySize(): Promise<any> {
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.CATEGORY.GET.SIZE.URL, {}, null, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getDiscountSize(): Promise<any> {
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.DISCOUNT.GET.SIZE.URL, {}, null, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getCategories(start: number, end: number = environment.PRODUCT.CATEGORY.GET.RANGE.QNT): Promise<CategoryModel[]> {
    let args: string[] = [start.toString(), end.toString()];
    let petition: Promise<any> = this.discoverService.petition("GET", environment.PRODUCT.CATEGORY.GET.RANGE.URL, {}, null, args);
    let result: Promise<CategoryModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let categories: CategoryModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(category => {
              categories.push(new CategoryModel().deserealize(category));
            });
          }
          resolve(categories);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public updateCharacteristic(charact: CharacteristicModel): Promise<boolean> {
    let body: Object = charact.toMap();
    let petition: Promise<any> = this.discoverService.petition("PUT", environment.PRODUCT.CHARACTERISTIC.PUT.UPDATE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public updateDiscountState(discountId: number): Promise<any> {
    let body: Object = { "discount_id": discountId };
    let petition: Promise<any> = this.discoverService.petition("PUT", environment.PRODUCT.DISCOUNT.PUT.STATE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteProduct(productId: number): Promise<boolean> {
    let body: Object = { "product_id": productId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.PRODUCT.DELETE.PRODUCT.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteCharacteristic(charactId: number): Promise<boolean> {
    let body: Object = { "charact_id": charactId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.CHARACTERISTIC.POST.DELETE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteCharacteristicProduct(id: number, productId: number, charactId: number): Promise<boolean> {
    let body: Object = { "id": id, "product_id": productId, "charact_id": charactId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.CHARACTERISTIC.POST.DELETE_PRODUCT.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteDiscount(discountId: number): Promise<boolean> {
    let body: Object = { "discount_id": discountId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.DISCOUNT.DELETE.SINGLE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteCategory(categoryId: number): Promise<boolean> {
    let body: Object = { "category_id": categoryId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.PRODUCT.CATEGORY.DELETE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

}
