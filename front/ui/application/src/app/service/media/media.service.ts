import { Injectable } from '@angular/core';
import { DiscoverService } from 'src/app/service/configuration/discover/discover.service';
import { environment } from 'src/environments/environment';
import { MediaModel } from 'src/app/model/media/media.model';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private discoverService: DiscoverService) { }

  public addProductMedia(type: number, folderId: number, productId: number, image: any): Promise<string> {
    let body: Object = { "image": image["image"] };
    let params: Object = { "type": type, "folder_id": folderId, "product_id": productId};
    let petition: Promise<any> = this.discoverService.petition("POST", environment.MEDIA.MEDIA.POST.MEDIA.URL, params, body, []);
    let result: Promise<string> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getProductFolder(productId: number): Promise<number> {
    let petition: Promise<any> = this.discoverService.petition("GET", environment.MEDIA.MEDIA.GET.FOLDER.URL, {}, null, [productId.toString()]);
    let result: Promise<number> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(parseInt(content[0]["id"]));
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public getMediaProducts(productIds: number[], unit: boolean = false): Promise<MediaModel[]> {
    let body: Object = { "products": productIds, "unit":unit };
    if (!productIds.length) return new Promise<MediaModel[]>((resolve, reject) => {
      resolve([]);
    });
    let petition: Promise<any> = this.discoverService.petition("POST", environment.MEDIA.MEDIA.POST.PRODUCTS.URL, {}, body, []);
    let result: Promise<MediaModel[]> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          let medias: MediaModel[] = [];
          if (Array.isArray(content)) {
            content.forEach(media => {
              medias.push(new MediaModel().deserealize(media));
            });
          }
          resolve(medias);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteMedia(mediaId: number): Promise<boolean> {
    let body: Object = { "media_id": mediaId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.MEDIA.MEDIA.DELETE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }

  public deleteFolder(folderId: number): Promise<boolean> {
    let body: Object = { "folder_id": folderId };
    let petition: Promise<any> = this.discoverService.petition("DELETE", environment.MEDIA.FOLDER.DELETE.URL, {}, body, []);
    let result: Promise<boolean> = new Promise<any>((resolve, reject) => {
      petition.then(data => {
        let response: number = data['response'];
        let content: any = data['content'];
        if (response == environment.RESPONSE.SUCCESS) {
          resolve(content);
        }
      }, (error) => {
        reject(error);
      });
    });
    return result;
  }
  
}
