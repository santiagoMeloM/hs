
import { Deserealize } from 'src/app/model/configuration/deserealize.model';

export class DiscoverModel implements Deserealize {

    private instanceId: string;
    private applicationName: string;
    private hostName: string;
    private port: string;
    private statusPageUrl: string;
    private healthCheckUrl: string;
    private ipAddress: string;
    private homePageUrl: string;
    private vipAddress: string;
    private status: string;

    constructor() {
        this.instanceId = null;
    }

    public deserealize(data: any): this {
        this.instanceId = data['instanceId'] || "";
        this.applicationName = data['applicationName'] || "";
        this.hostName = data['hostName'] || "";
        this.port = data['port'] || "";
        this.statusPageUrl = data['statusPageUrl'] || "";
        this.healthCheckUrl = data['healthCheckUrl'] || "";
        this.ipAddress = data['ipAddress'] || "";
        this.homePageUrl = data['homePageUrl'] || "";
        this.vipAddress = data['vipAddress'] || "";
        this.status = data['status'] || "";
        return this;
    }

    public getInstanceId(): string {
        return this.instanceId;
    }

    public getApplicationName(): string {
        return this.applicationName;
    }

    public getHostName(): string {
        return this.hostName;
    }

    public getPort(): string {
        return this.port['$'];
    }

    public getStatusPageUrl(): string {
        return this.statusPageUrl;
    }

    public getHealthCheckUrl(): string {
        return this.healthCheckUrl;
    }

    public getIpAddress(): string {
        return this.ipAddress;
    }

    public getHomePageUrl(): string {
        return this.homePageUrl;
    }

    public getVipAddress(): string {
        return this.vipAddress;
    }

    public getStatus(): string {
        return this.status;
    }
    
}