
import { Deserealize } from 'src/app/model/configuration/deserealize.model';

export class Response implements Deserealize {

    private response: any;
    private content: any;

    constructor() {}

    public deserealize(data: any): this {
        this.response = data['response'];
        this.content = data['content'];
        return this;
    }

    public getResponse(): any {
        return this.response;
    }

    public getContent(): any {
        return this.content;
    }
    
}