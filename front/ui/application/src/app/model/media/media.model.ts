
import { Deserealize } from 'src/app/model/configuration/deserealize.model';

export class MediaModel implements Deserealize {

    private content: string;
    private detail: Object;

    constructor() {}

    public deserealize(data: any): this {
        this.content = data['content'];
        this.detail = data['detail'];
        return this;
    }

    public getContent(): string {
        return this.content;
    }

    public getDetail(): Object {
        return this.detail;
    }

    public getBase64(): string {
        let result: string = `data:${this.detail['media']};base64,${this.content}`;
        return result;
    }
    
}