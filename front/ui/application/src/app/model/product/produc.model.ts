
import { Deserealize } from '../configuration/deserealize.model'
import { environment } from 'src/environments/environment';

export class ProductModel implements Deserealize {

    private id: number;
    private name: string;
    private price: number;
    private description: string;
    private created: Date;
    private active: boolean;

    constructor() {}

    public deserealize(data: any): this {
        this.id = data['id'];
        this.name = data['name'];
        this.price = data['price'];
        this.description = data['description'];
        this.created = new Date(data['created']);
        this.active = data['active'];
        return this;
    }

    public setId(id: number): any {
        this.id = id;
        return this;
    }

    public setPrice(price: number): void {
        this.price = price;
    }

    public getId(): number {
        return this.id;
    }
    
    public getName(): string {
        return this.name;
    }

    public getActive(): boolean {
        return this.active;
    }

    public getPrice(): number {
        return this.price;
    }

    public getDescription(): string {
        return this.description;
    }

    public getCreated(): Date {
        return this.created;
    }

    public isNew(): boolean {
        return this.created >= environment.GLOBAL.NEW();
    }

    public getMap(): Object {
        return {
            id: this.id,
            name: this.name,
            active: this.active,
            price: this.price,
            description: this.description,
            created: this.created
        };
    }
    
}