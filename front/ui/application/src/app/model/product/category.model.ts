
import { Deserealize } from '../configuration/deserealize.model'

export class CategoryModel implements Deserealize {

    private id: number;
    private name: string;
    private description: string;

    constructor() {}

    public deserealize(data: any): this {
        this.id = data['id'];
        this.name = data['name'];
        this.description = data['description'];
        return this;
    }

    public getId(): number {
        return this.id;
    }
    
    public getName(): string {
        return this.name;
    }

    public getDescription(): string {
        return this.description;
    }

    public getSubMenu(path): Object {
        return {
            name: this.name,
            path: path,
            params: {
                sub: this.id,
                name: this.name,
                description: this.description
            }
        };
    } 
    
}