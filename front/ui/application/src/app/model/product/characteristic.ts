
import { Deserealize } from '../configuration/deserealize.model'

export class CharacteristicModel implements Deserealize {

    private id: number;
    private name: string;
    private content: Object;
    private updated: boolean;

    constructor() {}

    public deserealize(data: any): this {
        this.id = data['id'];
        this.name = data['name'];
        this.content = JSON.parse(data['content']);
        this.updated = false;
        return this;
    }

    public setContent(cntnt: Object): void {
        this.content = cntnt;
        this.updated = true;
    }

    public getId(): number {
        return this.id;
    }
    
    public getName(): string {
        return this.name;
    }

    public getContent(): Object {
        return this.content;
    }

    public getUpdated(): boolean {
        return this.updated;
    }

    public toMap(): Object {
        return {
            "id": this.id,
            "name": this.name,
            "content": this.content
        }
    }
    
}