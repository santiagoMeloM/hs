
import { Deserealize } from '../configuration/deserealize.model'

export class DiscountModel implements Deserealize {

    private id: number;
    private porcentage: number;
    private product_id: number;
    private state: number;
    private deadline: Date;

    constructor() {}

    public deserealize(data: any): this {
        this.id = data['id'];
        this.porcentage = data['porcentage'];
        this.product_id = data['product_id'];
        this.state = data['state'];
        this.deadline = new Date(data['deadline']);
        return this;
    }

    public setId(id: number): any {
        this.id = id;
        return this;
    }

    public getId(): number {
        return this.id;
    }
    
    public getPorgentage(): number {
        return this.porcentage;
    }

    public getProductId(): number {
        return this.product_id;
    }

    public getState(): number {
        return this.state;
    }

    public getDeadline(): Date {
        return this.deadline;
    }
    
}