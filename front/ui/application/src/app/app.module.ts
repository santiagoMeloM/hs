import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import { HomeComponent } from 'src/app/component/client/home/home.component';
import { MainComponent } from 'src/app/component/client/main/main.component';
import { ReleasesComponent } from 'src/app/component/client/releases/releases.component';
import { CategoriesComponent } from 'src/app/component/client/categories/categories.component';
import { DealsComponent } from 'src/app/component/client/deals/deals.component';
import { ProductComponent } from 'src/app/component/client/product/product-unit/product-unit.component';
import { ProductDisplayComponent } from 'src/app/component/client/product/product-display/product-display.component';
import { StoreComponent } from 'src/app/component/admin/store/store.component';
import { AdminMainComponent } from 'src/app/component/admin/admin-main/admin-main.component';
import { ProductAdminComponent } from 'src/app/component/admin/module/product-admin/product-admin.component';
import { CategoryAdminComponent } from 'src/app/component/admin/module/category-admin/category-admin.component';
import { ProductListAdminComponent } from 'src/app/component/admin/module/product-list-admin/product-list-admin.component';
import { ProductDetailAdminComponent } from 'src/app/component/admin/product-detail-admin/product-detail-admin.component';
import { DiscountAdminComponent } from 'src/app/component/admin/module/discount-admin/discount-admin.component';
import { CategoryListAdminComponent } from 'src/app/component/admin/module/category-list-admin/category-list-admin.component';
import { DiscountListAdminComponent } from 'src/app/component/admin/module/discount-list-admin/discount-list-admin.component';
import { AppGuard } from 'src/app/app.guard';
import { AuthenticationComponent } from 'src/app/component/authentication/authentication.component';
import { RegistrationComponent } from 'src/app/component/registration/registration.component';
import { CartModalComponent } from 'src/app/service/configuration/cart/cart-modal/cart-modal.component';
import { DetailComponent } from 'src/app/component/client/product/detail/detail.component';
import { CheckoutComponent } from 'src/app/component/client/checkout/checkout.component';
import { ProductUnitCartComponent } from 'src/app/component/client/product/product-unit-cart/product-unit-cart.component';
import { OrderFilterModalComponent } from './service/configuration/order-filter/order-filter-modal/order-filter-modal.component';
import { ProductDisplayCartComponent } from './component/client/product/product-display-cart/product-display-cart.component';
import { NoticeModalComponent } from './service/configuration/notice/notice-modal/notice-modal.component';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { InformativeComponent } from './component/information/informative/informative.component';
import { HelpComponent } from './component/information/help/help.component';
import { AboutComponent } from './component/information/about/about.component';
import { FeedbackComponent } from './component/information/feedback/feedback.component';
import { TrackingComponent } from './component/information/tracking/tracking.component';
import { InformationComponent } from './component/information/information.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainComponent,
    ReleasesComponent,
    CategoriesComponent,
    DealsComponent,
    ProductComponent,
    ProductDisplayComponent,
    StoreComponent,
    AdminMainComponent,
    ProductAdminComponent,
    CategoryAdminComponent,
    ProductListAdminComponent,
    ProductDetailAdminComponent,
    DiscountAdminComponent,
    CategoryListAdminComponent,
    DiscountListAdminComponent,
    AuthenticationComponent,
    RegistrationComponent,
    CartModalComponent,
    DetailComponent,
    CheckoutComponent,
    ProductUnitCartComponent,
    OrderFilterModalComponent,
    ProductDisplayCartComponent,
    NoticeModalComponent,
    NotfoundComponent,
    InformativeComponent,
    HelpComponent,
    AboutComponent,
    FeedbackComponent,
    TrackingComponent,
    InformationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    Title,
    AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
